# README

Para executar o projeto no NetBeans no Linux os passos abaixo devem ser seguidos:

1) Ir no synaptic e instalar o pacote "**r-cran-rjava**"

2) A versão do java será alterada durante a instalação e deve ser trocada para a normal com o seguinte comando:

`sudo update-alternatives --config java`

3) Escrever os seguintes comandos no final do arquivo .bashrc que fica no /home:

`export JAVA_HOME=/usr/lib/jvm/jdk1.7.0_45`

`export PATH=$PATH:$JAVA_HOME/bin`

`export R_HOME=/usr/lib/R/`

`export CLASSPATH=.:/usr/lib/R/site-library/rJava/jri/`

`export LD_LIBRARY_PATH=/usr/lib/R/site-library/rJava/jri/`

4) Os arquivos "JRIEngine.jar", "JRI.jar", "REngine.jar", "jdom-2.0.5.jar", "jericho-html-3.3.jar", "mysql-connector-java-5.1.27-bin.jar" e "weka.jar" devem ser copiados para o diretorio do java "/usr/lib/jvm/jdk1.7.0_45/jre/lib/ext/". Esses arquivos podem ser encontrados no link: "https://drive.google.com/folderview?id=0B68uQsGmbj24R1N1TWZsb3NhVU0&usp=sharing".


5) Deve ser adicionado ao final do arquivo netbeans.conf, que fica **<diretorio instalação netbeans>/etc/netbeans.conf**, o seguinte comando:

`export R_HOME=/usr/lib/R/`

e o NetBeans deve ser reiniciado.
   Clicar com o botão direito em cima do projeto ir em Propriedades->Executar e onde diz opções de VM colocar o comando:

`-Djava.library.path="/usr/lib/R/site-library/rJava/jri/"`

6) Instalar o banco de dados do MERLOT, que o Cristian tem, e o banco de dados que o crawler usa no MySQL. O banco de dados que o crawler usa pode ser encontrado no link: "https://drive.google.com/file/d/0B68uQsGmbj24bG54M2g3WEotS00/edit?usp=sharing".

7) No arquivo chamado Configuration.xml, dentro do projeto, deve ser configurada a URL, usuario e a senha para acesso ao banco de dados do MERLOT. No arquivo Crawler.xml tambem devem ser configurados URL, usuario e senha mas para o acesso ao banco de dados utilizado pelo crawler.
