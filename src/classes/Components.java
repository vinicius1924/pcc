package classes;

import gui.MainWindow;
import java.awt.Image;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 * Classe que insere as informações nos components da interface
 *
 * @author Vinicius Santos
 *
 * @version 1.0
 *
 * @since 1.0
 */
public class Components extends JPanel
{
   private final List<String> categories;

   public Components()
   {
      this.categories = new ArrayList<>();
      /*
       * Esta categoria tem no banco de dados mas não tem nenhum objeto de aprendizagem associado a ela. Já no site do MERLOT esta
       * categoria tem vários objetos de aprendizagem de vários Material_Types.
       */
      this.categories.add("Academic Support Services");
      this.categories.add("Arts");
      this.categories.add("Business");
      this.categories.add("Education");
      this.categories.add("Humanities");
      /*
       * não tem esta categoria no banco de dados mas tem no MERLOT, o que tem no banco de dados é 
       * apenas "Mathematics" ou "Mathematics and Statistics/Mathematics"
       */
      this.categories.add("Mathematics and Statistics");
      this.categories.add("Science and Technology");
      this.categories.add("Social Sciences");
      //this.categorias.add("Workforce Development"); //esta categoria não tem no banco de dados mas tem no MERLOT
   }

   /**
    * Carrega as informações iniciais na interface para o usuário como categorias tipos de materiais
    * e números de objetos totais do banco de dados
    * 
    * @since 1.0
    * @param userRatedNumber o componente do tipo JLabel onde será mostrado o número de objetos com notas
    *                        dadas por usuários
    * @param peerReviewNumber o componente do tipo JLabel onde será mostrado o número de objetos com notas
    *                         dadas por especialistas
    * @param personalCollectionNumber o componente do tipo JLabel onde será mostrado o número de objetos que
    *                                 estão em coleções pessoais
    * @param categoriesModel o componente do tipo DefaultListModel onde serão carregadas as 
    *                        informações iniciais do programa (categorias)
    * @param materialsModel o componente do tipo DefaultListModel onde serão carregadas as 
    *                       informações iniciais do programa (tipos de materiais)
    */
   public void loadInicialData(DefaultListModel categoriesModel, DefaultListModel materialsModel, 
                               JLabel userRatedNumber, JLabel peerReviewNumber, JLabel personalCollectionNumber)
   {
      String query;
      ResultSet resultSet;

      showCategories(categoriesModel);

      query = "SELECT DISTINCT `Material_Type` FROM `lodata` ORDER BY `Material_Type` ASC";
      
      resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

      showMaterialTypes(resultSet, materialsModel);
      
      query = "SELECT COUNT(DISTINCT `ID_LO`) AS numeroDeObjetos FROM `lodata` \n"
               + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
               + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
               + "AND `metrics`.`Page_Error` = '0' \n"
               + "AND `lovaloration`.`Stars_Reviews` > '0'";
      
      resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

      showNumberOfObjects(resultSet, peerReviewNumber);
      
      query = "SELECT COUNT(DISTINCT `ID_LO`) AS numeroDeObjetos FROM `lodata` \n"
               + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
               + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
               + "AND `metrics`.`Page_Error` = '0' \n"
               + "AND `lovaloration`.`Stars_Comments` > '0'";      
      
      resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

      showNumberOfObjects(resultSet, userRatedNumber);
      
      query = "SELECT COUNT(DISTINCT `ID_LO`) AS numeroDeObjetos FROM `lodata` \n"
               + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
               + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
               + "AND `metrics`.`Page_Error` = '0' \n"
               + "AND  `Personal_Collections` !=  'none'";
      
      resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

      showNumberOfObjects(resultSet, personalCollectionNumber);
   }
   
   /**
    * Mostra as categorias dos objetos de aprendizagem, contidos no banco de dados, em um
    * DefaultListModel que foi usado no construtor de um JList
    * 
    * @since 1.0
    * @param categoriesModel o componente do tipo DefaultListModel onde serão mostradas as informações
    */
   public void showCategories(DefaultListModel categoriesModel)
   {
      int counter = 1; // conta o número de categorias
      
      for(int i = 0; i < this.categories.size(); i++)
      {
         categoriesModel.addElement(counter + ") " + this.categories.get(i));
         counter++;
      }
   }
   
   /**
    * Mostra os tipos de materiais dos objetos de aprendizagem, contidos no banco de dados, em um
    * DefaultListModel que foi usado no construtor de um JList
    * 
    * @since 1.0
    * @param resultSet ResultSet da consulta retornada do banco de dados
    * @param materialsModel o componente do tipo DefaultListModel onde serão mostradas as informações
    */
   public void showMaterialTypes(ResultSet resultSet, DefaultListModel materialsModel)
   {
      int counter = 1;

      try
      {
         while (resultSet.next())
         {
            String materialType = resultSet.getString("Material_Type");
            if (!materialType.equals(""))
            {
               materialsModel.addElement(counter + ") " + materialType);
               counter++;
            }
         }
      }
      catch (SQLException ex)
      {
         System.err.println(ex);
      }
   }
   
   /**
    * Mostra o número de objetos existentes, conforme o que foi selecionado na interface,
    * que tem notas de usuarios, especialistas e que estão em coleções pessoais em seus respectivos
    * JLabels
    * 
    * @since 1.0
    * @param resultSet ResultSet da consulta retornada do banco de dados
    * @param jlabel o componente do tipo JLabel onde será mostrado o número de objetos
    */
   public void showNumberOfObjects(ResultSet resultSet, JLabel jlabel)
   {
      int numberOfObjects;
      
      try
      {
         resultSet.next();
         numberOfObjects = resultSet.getInt("numeroDeObjetos");
         jlabel.setText(String.valueOf(" " + numberOfObjects));
      }
      catch (SQLException ex)
      {
         System.err.println(ex);
      }

   }
   
   /**
    * Atualiza a lista de tipos de materiais existentes em um DefaultListModel 
    * que foi usado no construtor de um JList conforme a categoria selecionada na interface
    * 
    * @since 1.0
    * @param categoriesList o JList que contém as categorias
    * @param materialTypeList o JList que contém os tipos de materiais
    * @param usersRadio o JRadioButton da perspectiva de qualidade dos usuários
    * @param expertsRadio o JRadioButton da perspectiva de qualidade dos especialistas
    * @param personalCollectionsRadio o JRadioButton da perspectiva de qualidade do número de coleções pessoais
    * @param materialModel o componente do tipo DefaultListModel onde serão mostradas as informações
    */
   public void updateDefaultListModelOfMaterialTypes(DefaultListModel materialModel, JList categoriesList, JList materialTypeList, JRadioButton usersRadio, 
                                                    JRadioButton expertsRadio, JRadioButton personalCollectionsRadio/*, 
                                                    JComboBox expertsNotesComboBox*/)
   {
      String query;
      ResultSet resultSet;
      
      List<String> data;
      String categorie;
      
      data = getComponentsData(categoriesList, materialTypeList, usersRadio, expertsRadio, 
                               personalCollectionsRadio/*, expertsNotesComboBox*/);
      categorie = data.get(0);
      
      cleanDefaultListModel(materialModel);

      /*
       * faz a consulta para pegar os tipos de materiais da categoria selecionada
       */
      query = "SELECT DISTINCT `Material_Type` FROM `lodata` \n"
              + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
              + "INNER JOIN `locategories` ON `locategories`.`ID_Cat` = `locat_dat`.`locategories_ID_Cat` \n"
              + "AND `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
              + "ORDER BY `Material_Type`";

      resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

      showMaterialTypes(resultSet, materialModel);

      updateNumberOfObjects(categoriesList, materialTypeList, usersRadio, expertsRadio, 
                            personalCollectionsRadio/*, expertsNotesComboBox*/);
   }
   
   /**
    * @since 1.0
    * @param listModel o componente do tipo DefaultListModel que será limpo
    */
   public void cleanDefaultListModel(DefaultListModel listModel)
   {
      listModel.clear();
   }
   
   /**
    * Escreve nos JLabels o número de objetos existentes conforme o que foi selecionado na interface
    * 
    * @since 1.0
    * @param categoriesList o JList que contém as categorias
    * @param materialTypeList o JList que contém os tipos de materiais
    * @param usersRadio o JRadioButton da perspectiva de qualidade dos usuários
    * @param expertsRadio o JRadioButton da perspectiva de qualidade dos especialistas
    * @param personalCollectionsRadio o JRadioButton da perspectiva de qualidade do número de coleções pessoais
    */
   public void updateNumberOfObjects(JList categoriesList, JList materialTypeList, JRadioButton usersRadio, 
                                     JRadioButton expertsRadio, JRadioButton personalCollectionsRadio/*, 
                                     JComboBox expertsNotesComboBox*/)
   {
      ResultSet resultSet;
      String query;
      String categorie;
      String materialType;
      String qualityMeasure;
      List<String> data;
      
      data = getComponentsData(categoriesList, materialTypeList, usersRadio, expertsRadio, 
                               personalCollectionsRadio/*, expertsNotesComboBox*/);
      categorie = data.get(0);
      materialType = data.get(1);
      qualityMeasure = data.get(2);
      
      /*
       * verifica o numero de objetos caso o usuario tenha selecionado 
       * categoria e tipo de material
       */
      if (categorie != null && materialType != null)
      {
         query = "SELECT  COUNT(DISTINCT `ID_LO`) AS numeroDeObjetos FROM `lodata` \n"
                 + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                 + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO`  = `lodata`.`ID_LO` \n"
                 + "AND `locat_dat`.`locategories_ID_Cat` =  `locategories`.`ID_Cat` \n"
                 + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                 + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                 + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                 + "AND `metrics`.`Page_Error` = '0' \n"
                 + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%'";

         resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

         showNumberOfObjects(resultSet, MainWindow.peerReviewRatedNumber);
            
         query = "SELECT COUNT(DISTINCT `ID_LO`) AS numeroDeObjetos FROM `lodata` \n"
              + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
              + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO`  = `lodata`.`ID_LO` \n"
              + "AND `locat_dat`.`locategories_ID_Cat` =  `locategories`.`ID_Cat` \n"
              + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
              + "AND `lovaloration`.`Stars_Comments` > '0' \n"
              + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
              + "AND `metrics`.`Page_Error` = '0' \n"
              + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%'";

         resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

         showNumberOfObjects(resultSet, MainWindow.userRatedNumber);

         query = "SELECT COUNT(DISTINCT `ID_LO`) AS numeroDeObjetos FROM `lodata` \n"
              + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
              + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO`  = `lodata`.`ID_LO` \n"
              + "AND `locat_dat`.`locategories_ID_Cat` =  `locategories`.`ID_Cat` \n"
              + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
              + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
              + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
              + "AND `metrics`.`Page_Error` = '0' \n"
              + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "'";

         resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

         showNumberOfObjects(resultSet, MainWindow.personalCollectionsNumber);
      }
      else
      {
         /*
          * verifica o numero de objetos caso o usuario tenha selecionado 
          * apenas categoria
          */
         if (categorie != null)
         {
            // faz a consulta do numero de objetos da categoria selecionada e que tem notas de usuarios
            query = "SELECT COUNT(DISTINCT `ID_LO`) AS numeroDeObjetos FROM `lodata` \n"
                     + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                     + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                     + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                     + "INNER JOIN `locategories` ON `locategories`.`ID_Cat` = `locat_dat`.`locategories_ID_Cat` \n"
                     + "AND `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                     + "AND `metrics`.`Page_Error` = '0' \n"
                     + "AND `lovaloration`.`Stars_Comments` > '0'";

            resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

            showNumberOfObjects(resultSet, MainWindow.userRatedNumber);

            // faz a consulta do numero de objetos da categoria selecionada e que tem notas de especialistas
            query = "SELECT COUNT(DISTINCT `ID_LO`) AS numeroDeObjetos FROM `lodata` \n"
                     + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                     + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                     + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                     + "INNER JOIN `locategories` ON `locategories`.`ID_Cat` = `locat_dat`.`locategories_ID_Cat` \n"
                     + "AND `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                     + "AND `metrics`.`Page_Error` = '0' \n"
                     + "AND `lovaloration`.`Stars_Reviews` > '0'";

            resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

            showNumberOfObjects(resultSet, MainWindow.peerReviewRatedNumber);

            // faz a consulta do numero de objetos da categoria selecionada e que estão em coleções pessoais
            query = "SELECT COUNT(DISTINCT `ID_LO`) AS numeroDeObjetos FROM `lodata` \n"
                     + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                     + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                     + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                     + "INNER JOIN `locategories` ON `locategories`.`ID_Cat` = `locat_dat`.`locategories_ID_Cat` \n"
                     + "AND `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                     + "AND `metrics`.`Page_Error` = '0' \n"
                     + "AND `lovaloration`.`Personal_Collections` != 'none'";

            resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

            showNumberOfObjects(resultSet, MainWindow.personalCollectionsNumber);
         }
         else
         {
            /*
             * verifica o numero de objetos caso o usuario tenha selecionado 
             * apenas tipo de material
             */
            if(materialType != null)
            {
               // faz a consulta do numero de objetos do tipo de material selecionado e que tem notas de usuarios
               query = "SELECT COUNT(DISTINCT `ID_LO`) AS numeroDeObjetos FROM `lodata` \n"
                     + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                     + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                     + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                     + "INNER JOIN `locategories` ON `locategories`.`ID_Cat` = `locat_dat`.`locategories_ID_Cat` \n"
                     + "AND `lodata`.`Material_Type` = " + "'" + materialType + "' \n"
                     + "AND `metrics`.`Page_Error` = '0' \n"
                     + "AND `lovaloration`.`Stars_Comments` > '0'";

               resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

               showNumberOfObjects(resultSet, MainWindow.userRatedNumber);

               // faz a consulta do numero de objetos do tipo de material selecionado e que tem notas de especialistas
               query = "SELECT COUNT(DISTINCT `ID_LO`) AS numeroDeObjetos FROM `lodata` \n"
                        + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                        + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                        + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                        + "INNER JOIN `locategories` ON `locategories`.`ID_Cat` = `locat_dat`.`locategories_ID_Cat` \n"
                        + "AND `lodata`.`Material_Type` = " + "'" + materialType + "' \n"
                        + "AND `metrics`.`Page_Error` = '0' \n"
                        + "AND `lovaloration`.`Stars_Reviews` > '0'";

               resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

               showNumberOfObjects(resultSet, MainWindow.peerReviewRatedNumber);

               // faz a consulta do numero de objetos do tipo de material selecionado e que estão em coleções pessoais
               query = "SELECT COUNT(DISTINCT `ID_LO`) AS numeroDeObjetos FROM `lodata` \n"
                        + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                        + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                        + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                        + "INNER JOIN `locategories` ON `locategories`.`ID_Cat` = `locat_dat`.`locategories_ID_Cat` \n"
                        + "AND `lodata`.`Material_Type` = " + "'" + materialType + "' \n"
                        + "AND `metrics`.`Page_Error` = '0' \n"
                        + "AND `lovaloration`.`Personal_Collections` != 'none'";

               resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);

               showNumberOfObjects(resultSet, MainWindow.personalCollectionsNumber);
            }
         }
      }
   }
   
   /**
    * Verifica os components que tiveram dados selecionados
    * 
    * @since 1.0
    * @param category o componente do tipo JList contendo as categorias que será verificado
    * @param materialType o componente do tipo JList contendo os tipos de materiais que será verificado
    * @param users o componente do tipo JRadioButton contendo a perspectiva de qualidade dos usuários que será verificado
    * @param experts o componente do tipo JRadioButton contendo a perspectiva de qualidade dos especialistas que será verificado
    * @param personalCollections o componente do tipo JRadioButton contendo a perspectiva de qualidade das coleções pessoais 
    *                            que será verificado
    * @return List<String> - uma lista com os dados dos components na seguinte ordem: categoria, tipos de materiais e tipo de
    *                        nota ou null para cada um que não tiver sido selecionado
    */
   public List<String> getComponentsData(JList category, JList materialType, JRadioButton users, JRadioButton experts, 
                                         JRadioButton personalCollections/*, JComboBox expertNote*/)
   {
      List<String> data = new ArrayList<>();
      int closeParenthesisPosition;

      if (category.getSelectedValue() != null)
      {
         closeParenthesisPosition = category.getSelectedValue().toString().indexOf(")");
         data.add(category.getSelectedValue().toString().substring(closeParenthesisPosition + 2));
      }
      else
      {
         data.add(null);
      }

      if (materialType.getSelectedValue() != null)
      {
         closeParenthesisPosition = materialType.getSelectedValue().toString().indexOf(")");
         data.add(materialType.getSelectedValue().toString().substring(closeParenthesisPosition + 2));
      }
      else
      {
         data.add(null);
      }

      if(!users.isSelected() && !experts.isSelected() && !personalCollections.isSelected())
      {
         data.add(null);
      }
      else
      {
         if(users.isSelected())
         {
            data.add(users.getText());
         }
         else
         {
            if(experts.isSelected())
            {
               data.add(experts.getText());
            }
            else
            {
               data.add(personalCollections.getText());
            }
         }
      }
      
      return data;
   }
   
   /**
    * Mostra os valores dos tercis calculados nos seus respectivos jlabels
    * 
    * @since 1.0
    * @param tertile os tercis calculados pelo R
    * @param goodThreshold o componente do tipo JLabel onde serão colocados os valores de tercis 
    *                      que representam objetos bons
    * @param notGoodThreshold o componente do tipo JLabel onde serão colocados os valores de tercis
    *                         que representam objetos não bons
    * @return boolean - true caso o subgrupo que foi selecionado pelo usuário contenha objetos ou false caso contrário
    */
   public boolean showTertileValues(double[] tertile, JLabel goodThreshold, JLabel notGoodThreshold)
   {
      DecimalFormat df = new DecimalFormat("#.00");
      if(String.valueOf(tertile[1]).equals("NaN"))
      {
         return false;
      }
      else
      {
         notGoodThreshold.setText(" 0.01 | " + String.valueOf(Double.valueOf(df.format(tertile[1]).replace(",", ".")).doubleValue() - 0.01));
         goodThreshold.setText(" " + df.format(tertile[1]).replace(",", ".") + " | " + (df.format(tertile[2]).replace(",", ".")));
         return true;
      }
   }
   
   /**
    * Plota o gráfico de densidade feito a partir das escolhas do usuário em um Jpanel
    * 
    * @since 1.0
    * @param graphicJLabel o componente do tipo JLabel onde será plotado o gráfico
    */
   public void plotGraphic(JLabel graphicJLabel)
   {
      ImageIcon icon = new ImageIcon(System.getProperty("user.dir") + "/graphics/rplot.png");
      graphicJLabel.setIcon(new ImageIcon(icon.getImage().getScaledInstance(graphicJLabel.getWidth(), graphicJLabel.getHeight(), Image.SCALE_DEFAULT)));
   }
   
   /**
    * Adiciona o nome das metricas na tabela
    * 
    * @since 1.0
    * @param metricsNames uma lista de string contendo os nomes das metricas que serão adicionadas na tabela
    * @param model o componente do tipo DefaultTableModel onde serão adicionadas as metricas
    */
   public void addNameOfTheMetricsInTheTable(List<String> metricsNames, DefaultTableModel model)
   {
      for(int i = 0; i < metricsNames.size(); i++)
      {
         model.addRow(new String[]{metricsNames.get(i)});
      }
   }
   
   /**
    * Mostra as estatísticas básicas na interface
    * 
    * @since 1.0
    * @param mean a média da metrica selecionada da tabela das metricas do grupo dos bons e não bons
    * @param median a mediana da metrica selecionada da tabela das metricas do grupo dos bons e não bons
    * @param goodAndNotGoodMetricVector o vetor com o valor da metrica selecionada na tabela das metricas do grupo 
    *                                   dos bons e não bons
    * @param minimumValue o componente do tipo JLabel onde será colocado o menor valor da metrica selecionada na 
    *                     tabela das metricas
    * @param maximumValue o componente do tipo JLabel onde será colocado o maior valor da metrica selecionada na 
    *                     tabela das metricas
    * @param meanLabel o componente do tipo JLabel onde será colocado o valor da média da metrica selecionada na 
    *                  tabela das metricas
    * @param medianLabel o componente do tipo JLabel onde será colocado o valor da mediana da metrica selecionada na 
    *                    tabela das metricas
    */
   public void showBasicStatistics(double mean, double median, double[] goodAndNotGoodMetricVector, 
                                    JLabel minimumValue, JLabel maximumValue, JLabel meanLabel, JLabel medianLabel)
   {
      DecimalFormat df = new DecimalFormat("0.00");
      minimumValue.setText(" " + String.valueOf(df.format(goodAndNotGoodMetricVector[0]).replace(",", ".")));
      maximumValue.setText(" " + String.valueOf(df.format(goodAndNotGoodMetricVector[goodAndNotGoodMetricVector.length - 1]).replace(",", ".")));
      meanLabel.setText(" " + String.valueOf(df.format(mean).replace(",", ".")));
      medianLabel.setText(" " + String.valueOf(df.format(median).replace(",", ".")));
   }
   
   /**
    * Limpa os JLabels existentes na interface
    * 
    * @since 1.0
    * @param label1 o componente do tipo JLabel que deverá ser limpo
    * @param label2 o componente do tipo JLabel que deverá ser limpo
    * @param label3 o componente do tipo JLabel que deverá ser limpo
    * @param label4 o componente do tipo JLabel que deverá ser limpo
    * @param label5 o componente do tipo JLabel que deverá ser limpo
    * @param label6 o componente do tipo JLabel que deverá ser limpo
    * @param label7 o componente do tipo JLabel que deverá ser limpo
    * @param label8 o componente do tipo JLabel que deverá ser limpo
    * @param label9 o componente do tipo JLabel que deverá ser limpo
    * @param label10 o componente do tipo JLabel que deverá ser limpo
    */
   public void clearJlabels(JLabel label1, JLabel label2, JLabel label3, JLabel label4, JLabel label5, 
                              JLabel label6, JLabel label7, JLabel label8, JLabel label9, JLabel label10)
   {
      label1.setText(null);
      label2.setText(null);
      label3.setText(null);
      label4.setText(null);
      label5.setText(null);
      label6.setText(null);
      label7.setText(null);
      label8.setText(null);
      label9.setText(null);
      label10.setText(null);
   }
   
   /**
    * Mostra o resultado do calculo do metodo de kolmogorvSmirnov
    * 
    * @since 1.0
    * @param d o componente do tipo JLabel que receberá o valor D calculado pelo método de kolmogovSmirnov
    * @param p o componente do tipo JLabel que receberá o valor p calculado pelo método de kolmogovSmirnov
    * @param dValue uma string com o valor D calculado pelo método de kolmogovSmirnov
    * @param pValue uma string com o valor p calculado pelo método de kolmogovSmirnov
    */
   public void showKolmogorovResult(JLabel d, JLabel p, String dValue, String pValue)
   {  
      DecimalFormat df = new DecimalFormat("0.####");
      
      if(pValue.equals("-1.0"))
      {
         p.setText(" null");
      }
      else
      {
         p.setText(" " + String.valueOf(df.format(Double.valueOf(pValue)).replace(",", ".")));
      }
      
      if(dValue.equals("-1.0"))
      {
         d.setText(" null");
      }
      else
      {
         d.setText(" " + String.valueOf(df.format(Double.valueOf(dValue)).replace(",", ".")));
      }
   }
   
   /**
    * Mostra o resultado do calculo do metodo de mannWhitney
    * 
    * @since 1.0
    * @param w o componente do tipo JLabel que receberá o valor W calculado pelo método de mannWhitney
    * @param p o componente do tipo JLabel que receberá o valor p calculado pelo método de mannWhitney
    * @param wValue uma string com o valor W calculado pelo método de mannWhitney
    * @param pValue uma string com o valor p calculado pelo método de mannWhitney
    */
   public void showMannWhitneyResult(JLabel w, JLabel p, String wValue, String pValue)
   {
      DecimalFormat df = new DecimalFormat("0.####");
      
      if(pValue.equals("-1.0"))
      {
         p.setText(" null");
      }
      else
      {
         p.setText(" " + String.valueOf(df.format(Double.valueOf(pValue)).replace(",", ".")));
      }
      
      if(wValue.equals("-1.0"))
      {
         w.setText(" null");
      }
      else
      {
         w.setText(" " + String.valueOf(df.format(Double.valueOf(wValue)).replace(",", ".")));
      }
   }
   
   /**
    * Limpa a tabela de metricas
    * 
    * @since 1.0
    * @param model o componente do tipo DefaultTableModel para limpar a tabela
    */
   public void clearMetricsFromTable(DefaultTableModel model)
   {  
      if(model.getRowCount() > 0)
      {
         model.setRowCount(0);  
      }
   }
   
   /**
    * Adiciona os indicadores das métricas na coluna Indicators da tabela de métricas
    * 
    * @since 1.0
    * @param model o componente do tipo DefaultTableModel para adicionar os indicadores
    * @param indicators um vetor de inteiros contendo 0 se a metrica não é um indicador de qualidade 1 se é um indicador 
    *                   de qualidade parcial ou 2 se é um indicador de qualidade
    */
   public void addIndicatorInTheMetricsTable(DefaultTableModel model, int[] indicators, List<Integer> indices)
   {
      for(int i = 0; i < indicators.length; i++)
      {
         if(indicators[i] == 0)
         {
            model.setValueAt("N", i, 1);
         }
         else
         {
            if(indicators[i] == 1)
            {
               model.setValueAt("(Y)", i, 1);
               indices.add(i);
            }
            else
            {
               model.setValueAt("Y", i, 1);
               indices.add(i);
            }
         }
      }
   }
   
   /**
    * Adiciona a influência das métricas na coluna Influence da tabela de métricas
    * 
    * @since 1.0
    * @param model o componente do tipo DefaultTableModel para adicionar as influências
    * @param influence um vetor de boolean contendo true se a métrica influencia positivamente ou false se a métrica influencia
    *                  negativamente
    */
   public void addInfluenceInTheMetricsTable(DefaultTableModel model, boolean[] influence)
   {
      for(int i = 0; i < influence.length; i++)
      {
         if(!model.getValueAt(i, 1).toString().equals("N"))
         {
            if(influence[i])
            {
               model.setValueAt("\u2191", i, 2);
               model.setValueAt(true, i, 3);
            }
            else
            {
               model.setValueAt("\u2193", i, 2);
               model.setValueAt(true, i, 3);
            }
         }
         else
         {
            model.setValueAt(false, i, 3);
         }
      }
   }
   
   /**
    * Adiciona a influência das métricas, lidas do modelo, na coluna Influence da tabela de métricas
    * 
    * @since 1.0
    * @param model o componente do tipo DefaultTableModel para adicionar as influências
    * @param influence um vetor de boolean contendo true se a métrica influencia positivamente ou false se a métrica influencia
    *                  negativamente
    * @param modelAttributes os atributos usados no modelo
    */
   public void addInfluenceInTheMetricsTableReadedFromModel(DefaultTableModel model, boolean[] influence, List<String> modelAttributes)
   {
      for(int i = 0; i < influence.length; i++)
      {
         if(!model.getValueAt(i, 1).toString().equals("N"))
         {
            if(influence[i])
            {
               model.setValueAt("\u2191", i, 2);
            }
            else
            {
               model.setValueAt("\u2193", i, 2);
            }
         }
         else
         {
            model.setValueAt(false, i, 3);
         }
      }
      
      for(int i = 0; i < modelAttributes.size(); i++)
      {
         for(int j = 0; j < model.getRowCount(); j++)
         {
            if(model.getValueAt(j, 0).toString().equals(modelAttributes.get(i)))
            {
               model.setValueAt(true, j, 3);
            }
            else
            {
               if(model.getValueAt(j, 3) == null)
               {
                  model.setValueAt(false, j, 3);
               }
               else
               {
                  if(!Boolean.valueOf(model.getValueAt(j, 3).toString()))
                     model.setValueAt(false, j, 3);
               }
            }
         }
      }
   }
   
   /**
    * Mostra o caminho do modelo escolhido pelo usuário em um JTextField
    * 
    * @since 1.0
    * @param choosenModelPath o JTextField onde será mostrado o caminho do modelo
    * @param modelPath o caminho do modelo escolhido
    */
   public void showModelPath(JTextField choosenModelPath, String modelPath)
   {
      choosenModelPath.setText(modelPath);
   }
   
   /**
    * Adiciona as métricas e seus valores, pegos crawler, em uma tabela
    * 
    * @since 1.0
    * @param crawledMetrics o DefaultTableModel onde serão adicionadas as métricas
    * @param values os valores das métricas
    */
   public void addValueCrawledMetricsTable(DefaultTableModel crawledMetrics, List<Double> values)
   {
      for(int i = 0; i < values.size(); i++)
      {
         crawledMetrics.setValueAt(values.get(i), i, 1);
      }
   }
   
   /**
    * Seleciona todas as métricas da tabela de métricas
    * 
    * @since 1.0
    * @param model o DefaultTableModel que contém as métricas
    */
   public void selectAllMetrics(DefaultTableModel model)
   {
      for(int i = 0; i < model.getRowCount(); i++)
      {
         model.setValueAt(true, i, 3);
      }
   }
   
   /**
    * Retira a seleção das métricas selecionadas na tabela de métricas
    * 
    * @since 1.0
    * @param model o DefaultTableModel que contém as métricas
    */
   public void deSelectAllMetrics(DefaultTableModel model)
   {
      for(int i = 0; i < model.getRowCount(); i++)
      {
         model.setValueAt(false, i, 3);
      }
   }
   
   /**
    * Seleciona as métricas relevantes, para a qualidade dos objetos, da tabela das métricas
    * 
    * @since 1.0
    * @param model o DefaultTableModel que contém as métricas
    */
   public void selectRelevantMetrics(DefaultTableModel model)
   {
      for(int i = 0; i < model.getRowCount(); i++)
      {
         if(model.getValueAt(i, 1).equals("Y") || model.getValueAt(i, 1).equals("(Y)"))
         {
            model.setValueAt(true, i, 3);
         }
         else
         {
            model.setValueAt(false, i, 3);
         }
      }
   }
   
   /**
    * Seleciona uma perspectiva de qualidade da tela inicial do software
    * 
    * @since 1.0
    * @param qualityPerspective o nome da perspectiva de qualidade a ser selecionada
    * @param usersRadio o componente do tipo JRadioButton com a perspectiva de qualidade dos usuários
    * @param expertsRadio o componente do tipo JRadioButton com a perspectiva de qualidade dos especialistas
    * @param personalColectionsRadio o componente do tipo JRadioButton com a perspectiva de qualidade das coleções pessoais
    */
   public void selectQualityPerspective(String qualityPerspective, JRadioButton usersRadio, JRadioButton expertsRadio,
                                          JRadioButton personalColectionsRadio)
   {
      if(qualityPerspective.equals("Users"))
      {
         usersRadio.setSelected(true);
      }
      
      if(qualityPerspective.equals("Experts"))
      {
         expertsRadio.setSelected(true);
      }
      
      if(qualityPerspective.equals("Personal Collections"))
      {
         personalColectionsRadio.setSelected(true);
      }
   }
   
   /**
    * Seleciona uma categoria da lista de categorias existentes na tela inicial do software
    * 
    * @since 1.0
    * @param category o nome da categoria a ser selecionada
    * @param categoriesList o componente do tipo JList contendo as categorias
    */
   public void selectCategory(String category, JList categoriesList)
   {
      if(category != null)
         categoriesList.setSelectedValue(category, true);
   }
   
   /**
    * Seleciona um tipo de material da lista de tipos de materiais existentes na tela inicial do software
    * 
    * @since 1.0
    * @param materialType o nome do tipo de material a ser selecionado
    * @param materialTypeList o componente do tipo JList contendo os tipos de materiais
    */
   public void selectMaterialType(String materialType, JList materialTypeList)
   {
      if(materialType != null)
         materialTypeList.setSelectedValue(materialType, true);
   }
}