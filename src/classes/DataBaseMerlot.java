package classes;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 * Classe que faz a conexão e realiza consultas no banco de dados
 * 
 * @author Vinicius Santos
 * 
 * @version 1.0
 * 
 * @since 1.0
 */
public class DataBaseMerlot
{
   private String url;
   private String user;
   private String password;
   
   public DataBaseMerlot()
   {
      
   }

   /**
    * @since 1.0
    * @param url url do banco de dados
    * @param usuario nome do user do banco de dados
    * @param senha password do user do banco de dados
    */
   public DataBaseMerlot(String url, String usuario, String senha)
   {
      this.url = url;
      this.user = usuario;
      this.password = senha;
   }
   
   /**
    * Lê o arquivo XML com as configurações para fazer a conexão com o servidor
    * 
    * @param xmlName o nome do arquivo xml que será lido
    * @return List<String> - uma lista com os parâmetros para a conexão na seguinte ordem: url,
    *                        user e password
    */
   public List<String> leXML(String xmlName)
   {
      List<String> configuracao = new ArrayList<>();
      
      try
      {
         File f = new File(xmlName);

         SAXBuilder builder = new SAXBuilder();

         Document doc = builder.build(f);

         Element root = (Element) doc.getRootElement();
         
         configuracao.add(root.getChildText("Url"));
         configuracao.add(root.getChildText("Usuario"));
         configuracao.add(root.getChildText("Senha"));
      }
      catch(IOException | JDOMException ex)
      {
         System.err.println(ex);
      }
      
      return configuracao;
   }

   /**
    * Retorna uma conexão com o banco de dados ou imprime uma exceção caso a mesma ocorra
    * 
    * @since 1.0
    * @return Connection - conexao com o banco de dados
    */
   public Connection conectaNoBancoDeDados()
   {
      Connection con = null;

      try
      {
         con = (Connection) DriverManager.getConnection(this.url, this.user, this.password);
      }
      catch (SQLException ex)
      {
         JOptionPane.showMessageDialog(null, ex, "Erro", JOptionPane.ERROR_MESSAGE);
         System.exit(1);
      }

      return con;
   }

   /**
    * Retorna um conjunto com o resultado da consulta ao banco de dados ou imprime uma exceção 
    * caso a mesma ocorra
    * 
    * @since 1.0
    * @param conexao a conexão estabelecida com o banco de dados
    * @param consulta a consulta a ser feita no banco de dados
    * @return ResultSet - conjunto com o resultado da consulta ao banco de dados
    */
   public ResultSet consultaBancoDeDados(Connection conexao, String consulta)
   {
      Statement st = null;
      ResultSet rs = null;

      try
      {
         st = (Statement) conexao.createStatement();
         rs = st.executeQuery(consulta);
      }
      catch (SQLException ex)
      {
         JOptionPane.showMessageDialog(null, ex, "Erro", JOptionPane.ERROR_MESSAGE);
      }

      return rs;
   }
}