/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classes;

import crawler.Crawler;
import crawler.DataBaseCrawler;
import gui.MainWindow;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.table.DefaultTableModel;

/**
 * @author Vinicius Santos
 * 
 * @version 1.0
 * 
 * @since 1.0
 */
public class ExtractMetrics extends Thread
{
   private boolean exists;
   private List<String> configReaded;
   private Crawler crawler;
   private final String URL;
   private DataBaseCrawler dataBaseCrawler;
   private final Utils utils;
   private List<String> modelAttributes = new ArrayList<>();
   private final Components components;
   private final DefaultTableModel modelCrawledMetrics;
   private final JButton classifyDataButton;
   private final JProgressBar progressBar;

   public ExtractMetrics(String URL, List<String> modelAttributes, DefaultTableModel modelCrawledMetrics, 
                           JButton classifyDataButton, JProgressBar progressBar)
   {
      this.components = new Components();
      this.dataBaseCrawler = new DataBaseCrawler();
      this.utils = new Utils();
      this.URL = URL;
      this.modelAttributes = modelAttributes;
      this.modelCrawledMetrics = modelCrawledMetrics;
      this.classifyDataButton = classifyDataButton;
      this.progressBar = progressBar;
   }
   
   @Override
   public void run()
   {
      exists = utils.checkURLExists(URL);
      
      if(!exists)
      {
         progressBar.setIndeterminate(false);
         progressBar.setStringPainted(false);
         progressBar.setMinimum(100);
         JOptionPane.showMessageDialog(null, "Unknown Host!", "Erro", JOptionPane.ERROR_MESSAGE);
      }
      else
      {
         configReaded = dataBaseCrawler.leXML("Crawler.xml");

         System.out.println("Obtain Metrics");

         dataBaseCrawler = new DataBaseCrawler(configReaded.get(0), configReaded.get(1), configReaded.get(2));
         MainWindow.conexionCrawler = dataBaseCrawler.connect();
         
         if(!dataBaseCrawler.testIfURLExistsOnDataBase(URL, MainWindow.conexionCrawler))
         {
            crawler = new Crawler();            
            
            /* recebe o valor dos atributos que o crawler coletou e que são usados no modelo carregado */
            MainWindow.attributesValues = crawler.crawlMetrics(URL, MainWindow.conexionCrawler, modelAttributes, false);
            
            if(MainWindow.attributesValues != null)
            {
               components.addNameOfTheMetricsInTheTable(modelAttributes, modelCrawledMetrics);
               components.addValueCrawledMetricsTable(modelCrawledMetrics, MainWindow.attributesValues);
               
               classifyDataButton.setEnabled(true);
               
               progressBar.setIndeterminate(false);
               progressBar.setValue(100);
               progressBar.setString("Processed");
            }
         }
         else
         {
            int choosedOption = JOptionPane.showConfirmDialog(null, "Object has already been analyzed!\nDo you want to reanalyze this object?",
                                                "Message", JOptionPane.WARNING_MESSAGE);
            if(choosedOption == 0)
            {
               crawler = new Crawler();            
            
               /* recebe o valor dos atributos que o crawler coletou e que são usados no modelo carregado */
               MainWindow.attributesValues = crawler.crawlMetrics(URL, MainWindow.conexionCrawler, modelAttributes, true);
               
               if(MainWindow.attributesValues != null)
               {
                  components.addNameOfTheMetricsInTheTable(modelAttributes, modelCrawledMetrics);
                  components.addValueCrawledMetricsTable(modelCrawledMetrics, MainWindow.attributesValues);

                  classifyDataButton.setEnabled(true);

                  progressBar.setIndeterminate(false);
                  progressBar.setValue(100);
                  progressBar.setString("Processed");
               }
            }
            else
            {
               progressBar.setIndeterminate(false);
               progressBar.setStringPainted(false);
               progressBar.setMinimum(100);
               MainWindow.URLAnalized = true;
               classifyDataButton.setEnabled(true);
            }
         }
      }
   }
}
