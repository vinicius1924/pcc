/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classes;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Classe utilizada nas colunas Indicator e Influence da tabela das métricas para mudar
 * a cor de fundo, a cor da fonte e o alinhamento do texto
 * 
 * @author Vinicius Santos
 * 
 * @version 1.0
 *
 * @since 1.0
 */
public class IndicatorAndInfluenceTableRenderer extends DefaultTableCellRenderer
{  
   @Override
   public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
   {
      float[] HSBColorBackgroundSelected = new float[3];
      float[] HSBColorBackgroundNotSelected = new float[3];
      String indicator;
      super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); 
      
      Color.RGBtoHSB(255, 255, 165, HSBColorBackgroundNotSelected);
      Color.RGBtoHSB(158, 86, 9, HSBColorBackgroundSelected);
      
      this.setHorizontalAlignment(JLabel.CENTER);
      indicator = table.getValueAt(row, 1).toString();
      
      /*
       * Teste se o valor na coluna indicator da tabela das métricas é igual a Y ou (Y).
       * Se for igual então testa se a linha está selecionada. Se não estiver selecionada e for uma
       * métrica relevante para determinar a qualidade do objeto então põe a cor de fundo
       * amarelo claro. Se estiver selecionada, e for uma métrica relevante para determinar a 
       * qualidade põe a cor de fundo marrom e a cor da fonte branca.
       */
      if(indicator.equals("Y") || indicator.equals("(Y)"))
      {
         if(isSelected)
         {
            setOpaque(true);
            setBackground(Color.getHSBColor(HSBColorBackgroundSelected[0], HSBColorBackgroundSelected[1], HSBColorBackgroundSelected[2]));
            setForeground(Color.WHITE);
         }
         else
         {    
            setOpaque(true);
            setBackground(Color.getHSBColor(HSBColorBackgroundNotSelected[0], HSBColorBackgroundNotSelected[1], HSBColorBackgroundNotSelected[2]));
            setForeground(Color.BLACK);
         }
      }
      /*
       * Se o valor na coluna indicator da tabela das métricas não é igual a Y ou (Y)
       * e a linha está selecionada então a cor de fundo é posta marrom e a cor da 
       * fonte branca. Caso a linha não esteja selecionada e a coluna indicator da 
       * tabela das métricas não for igual a Y ou (Y) então a cor de fundo é posta branca
       * e a cor da fonte preta.
       */
      else
      {
         if(isSelected)
         {
            setOpaque(true);
            setBackground(Color.getHSBColor(HSBColorBackgroundSelected[0], HSBColorBackgroundSelected[1], HSBColorBackgroundSelected[2]));
            setForeground(Color.WHITE);
         }
         else
         {
            setOpaque(false);
            setBackground(Color.WHITE);
            setForeground(Color.BLACK);
         }
      }
      return this;
   }
}
