/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classes;

import gui.MainWindow;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * Classe utilizada na coluna Teste da tabela das métricas para mudar
 * a cor de fundo, a cor da fonte, o alinhamento do texto e colocar JCheckBox
 * 
 * @author Vinicius Santos
 * 
 * @version 1.0
 *
 * @since 1.0
 */
public class JCheckBoxTableRenderer extends JCheckBox implements TableCellRenderer
{  
   @Override
   public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
   {      
      float[] HSBColorBackgroundSelected = new float[3];
      float[] HSBColorBackgroundNotSelected = new float[3];
      
      Color.RGBtoHSB(158, 86, 9, HSBColorBackgroundSelected);
      Color.RGBtoHSB(255, 255, 165, HSBColorBackgroundNotSelected);
      this.setHorizontalAlignment(JLabel.CENTER);
      
      if((Boolean)value)
      {
         setSelected(true);
      }
      else
      {
         setSelected(false);
      }
      
      
      /*
       * Caso a linha esteja selecionada então a cor de fundo é posta marrom e a cor da 
       * fonte branca.
       */
      if(isSelected)
      {
         setOpaque(true);
         setBackground(Color.getHSBColor(HSBColorBackgroundSelected[0], HSBColorBackgroundSelected[1], HSBColorBackgroundSelected[2]));
         setForeground(Color.WHITE);
      }
      else
      {
         boolean test = false;

         for(int i = 0; i < MainWindow.indices.size(); i++)
         {
            if(MainWindow.indices.get(i).equals(row))
            {
               test = true;
               setOpaque(true);
               setBackground(Color.getHSBColor(HSBColorBackgroundNotSelected[0], HSBColorBackgroundNotSelected[1], HSBColorBackgroundNotSelected[2]));
               setForeground(Color.BLACK);
            }
         }

         if(!test)
            setOpaque(false);
      }

      return this;
   }
}
