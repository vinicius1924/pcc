package classes;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe que descreve um objeto de aprendizagem
 *
 * @author Vinicius Santos
 * 
 * @version 1.0
 *
 * @since 1.0
 */
public class LearningObject
{
   private int ID_LO;
   private String categorie;
   private String material_Type;
   private double[] metrics;
   private List<String> nameOfMetrics;
   /* dataString, dataDate, dataInt são os dados da tabela lodata do banco de dados */
   private String[] dataString;
   private Date[] dataDate;
   private int[] dataInt;
   /* nameOfData é o nome do dado da tabela lodata do banco de dados */
   private List<String> nameOfData;

   /**
    * Construtor da classe LearningObject
    * 
    * @since 1.0
    * @param numberOfMetrics o número de métricas que o objeto contém
    * @param numberOfData o número de dados que o objeto contém
    */
   public LearningObject(int numberOfMetrics, int numberOfData)
   {
      if(numberOfMetrics == -1 && numberOfData != 0)
      {
         this.dataString = new String[numberOfData];
         this.dataDate = new Date[numberOfData];
         this.dataInt = new int[numberOfData];
         this.nameOfData = new ArrayList<>(numberOfData);
      }
      else
      {
         if(numberOfMetrics != 0 && numberOfData == -1)
         {
            this.metrics = new double[numberOfMetrics];
            this.nameOfMetrics = new ArrayList<>(numberOfMetrics);
         }
         else
         {
            this.dataString = new String[numberOfData];
            this.dataDate = new Date[numberOfData];
            this.dataInt = new int[numberOfData];
            this.nameOfData = new ArrayList<>(numberOfData);
            this.metrics = new double[numberOfMetrics];
            this.nameOfMetrics = new ArrayList<>(numberOfMetrics);
         }
      }
   }
   
   /**
    * Adiciona o nome da métrica no objeto
    * 
    * @since 1.0
    * @param name o nome da métrica a ser adicionada no objeto
    */
   public void addNameOfMetric(String name)
   {
      nameOfMetrics.add(name);
   }
   
   /**
    * Adiciona o nome do dado no objeto
    * 
    * @since 1.0
    * @param name o nome do dado a ser adicionado no objeto
    */
   public void addNameOfData(String name)
   {
      nameOfData.add(name);
   }
   
   /**
    * Pega o nome do dado em uma determinada posição da lista
    * 
    * @since 1.0
    * @param position uma posição da lista que contém o nome dos dados
    * @return String - o nome do dado
    */
   public String getNameOfData(int position)
   {
      return nameOfData.get(position);
   }
   
   /**
    * Pega o nome da métrica em uma determinada posição da lista
    * 
    * @since 1.0
    * @param position uma posição da lista que contém o nome das métricas
    * @return String - o nome da métrica
    */
   public String getNameOfMetric(int position)
   {
      return nameOfMetrics.get(position);
   }
   
   /**
    * Pega o tamanho da lista do nome dos dados
    * 
    * @since 1.0
    * @return int - um inteiro com o tamanho
    */
   public int getNameOfDataSize()
   {
      return nameOfData.size();
   }
   
   /**
    * Pega o tamanho da lista do nome das métricas
    * 
    * @since 1.0
    * @return int - um inteiro com o tamanho
    */
   public int getNameOfMetricsSize()
   {
      return nameOfMetrics.size();
   }
   
   /**
    * Adiciona um dado do tipo String em uma posição do vetor de dados do tipo String
    * 
    * @since 1.0
    * @param position a posição do vetor em que o dado deve ser adicionado
    * @param string o dado que deve ser adicionado no vetor
    */
   public void setDataString(int position, String string)
   {
      this.dataString[position] = string;
   }
   
   /**
    * Pega um dado do tipo String em uma posição do vetor de dados do tipo String
    * 
    * @since 1.0
    * @param position a posição do vetor em que o dado deve ser buscado
    * @return String - o dado
    */
   public String getDataString(int position)
   {
      return this.dataString[position];
   }
   
   /**
    * Adiciona um dado do tipo int em uma posição do vetor de dados do tipo int
    * 
    * @since 1.0
    * @param position a posição do vetor em que o dado deve ser adicionado
    * @param number o dado que deve ser adicionado no vetor
    */
   public void setDataInt(int position, int number)
   {
      this.dataInt[position] = number;
   }
   
   /**
    * Pega um dado do tipo int em uma posição do vetor de dados do tipo int
    * 
    * @since 1.0
    * @param position a posição do vetor em que o dado deve ser buscado
    * @return int - o dado
    */
   public int getDataInt(int position)
   {
      return this.dataInt[position];
   }
   
   /**
    * Adiciona uma métrica em uma posição do vetor de métricas
    * 
    * @since 1.0
    * @param position a posição do vetor em que a métrica deve ser adicionada
    * @param number a métrica que deve ser adicionada no vetor
    */
   public void setMetrics(int position, double number)
   {
      this.metrics[position] = number;
   }
   
   /**
    * Adiciona no objeto o ID dele do banco de dados
    * 
    * @since 1.0
    * @param ID_LO o ID do objeto do banco de dados
    */
   public void setID_LO(int ID_LO)
   {
      this.ID_LO = ID_LO;
   }
   
   /**
    * Pega o ID do objeto do banco de dados
    * 
    * @since 1.0
    * @return int - ID do objeto do banco de dados
    */
   public int getID_LO()
   {
      return ID_LO;
   }
   
   /**
    * Retorna o valor da métrica do vetor de metricas da posição especificada
    * 
    * @since 1.0
    * @param position a posição que se deve buscar no vetor de métricas do objeto
    * @return double - o valor da métrica
    */
   public double getMetrics(int position)
   {
      return metrics[position];
   }
   
   /**
    * Adiciona a categoria do objeto
    * 
    * @since 1.0
    * @param category a categoria a ser adicionada no objeto
    */
      public void setCategory(String category)
   {
      this.categorie = category;
   }
   
   /**
    * Adiciona o tipo de material do objeto
    * 
    * @since 1.0
    * @param material_Type o tipo de material a ser adicionada no objeto
    */
   public void setMaterial_Type(String material_Type)
   {
      this.material_Type = material_Type;
   }
   
   /**
    * Retorna a categoria do objeto
    * 
    * @since 1.0
    * @return String - a categoria do objeto
    */
   public String getCategory()
   {
      return categorie;
   }

   /**
    * Retorna o tipo de material do objeto
    * 
    * @since 1.0
    * @return String - o tipo de material do objeto
    */
   public String getMaterial_Type()
   {
      return material_Type;
   }
}
