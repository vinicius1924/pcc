/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classes;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;

/**
 * Classe utilizada na coluna Metrics da tabela das métricas para mudar
 * a cor de fundo, a cor da fonte e o alinhamento do texto
 * 
 * @author Vinicius Santos
 * 
 * @version 1.0
 *
 * @since 1.0
 */
public class MetricsNamesTableRenderer extends IndicatorAndInfluenceTableRenderer
{
   @Override
   public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
   {
      float[] HSBColorBackgroundSelected = new float[3];
      super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); 
      
      Color.RGBtoHSB(158, 86, 9, HSBColorBackgroundSelected);
      this.setHorizontalAlignment(JLabel.LEFT);
      
      /*
       * Caso a linha esteja selecionada então a cor de fundo é posta marrom e a cor da 
       * fonte branca.
       */
      if(isSelected)
      {
         setOpaque(true);
         setBackground(Color.getHSBColor(HSBColorBackgroundSelected[0], HSBColorBackgroundSelected[1], HSBColorBackgroundSelected[2]));
         setForeground(Color.WHITE);
      }

      return this;
   }
}
