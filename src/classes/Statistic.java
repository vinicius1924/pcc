package classes;

import java.io.File;
import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.rosuda.JRI.REXP;
import org.rosuda.JRI.RVector;
import org.rosuda.JRI.Rengine;

/**
 * Classe que faz as contas estatísticas e utiliza os métodos de Mann-Whitney e Kolmogorov–Smirnov
 *
 * @author Vinicius Santos
 *
 * @version 1.0
 *
 * @since 1.0
 */
public class Statistic
{
   private final Rengine r;

   public Statistic()
   {
      this.r = new Rengine(new String[]{"--no-save"}, false, null);
   }

   /**
    * Calcula os tercis de um vetor de double
    * 
    * @since 1.0
    * @param vector um vetor de doubles que será usado para calcular os tercis
    * @return double[] - um vetor de doubles contendo os tercis calculados
    */
   public double[] calculateTertile(double[] vector)
   {
      r.assign("vetor", vector);
      REXP result = r.eval("quantile(vetor, c(1/3, 2/3, 3/3))");
      double[] finalResult = result.asDoubleArray();
      return finalResult;
   }
   
   /**
    * Faz o cálculo do W e do p do método de mannWhitney
    * 
    * @since 1.0
    * @param goodVector um vetor contendo o valor de uma métrica métrica de todos os objetos do grupo dos objetos bons
    * @param notGoodVector um vetor contendo o valor de uma métrica métrica de todos os objetos do grupo dos objetos não bons
    * @return double[] - um vetor de double contendo na posição 0 o valor do W e na posição 1 o valor de p
    */
   /* verifica a diferença entre as medianas */
   public double[] mannWhitney(double[] goodVector, double[] notGoodVector)
   {
      boolean everythingIsZero = true;
      double[] results = new double[2];
      Pattern patternP = Pattern.compile("(\\d+\\.\\d+)");
      Pattern patternPScientific = Pattern.compile("(\\d+\\.\\d+[E]+[\\+|\\-]?\\d+)");
      Pattern patternW = Pattern.compile("([0-9]+.?[0-9]+)");
      Matcher matcherP;
      Matcher matcherPScientific;
      Matcher matcherW;
      REXP result;
      
      for(int i = 0; i < goodVector.length; i++)
      {
         if(goodVector[i] != 0)
            everythingIsZero = false;
         
         if(everythingIsZero == false)
         {
            break;
         }
      }
      
      if(everythingIsZero)
      {
         for(int i = 0; i < notGoodVector.length; i++)
         {
            if(notGoodVector[i] != 0)
               everythingIsZero = false;

            if(everythingIsZero == false)
            {
               break;
            }
         }
      }
      
      if(everythingIsZero)
      {
         results[0] = -1.0;
         results[1] = -1.0;
      }
      else
      {
         r.assign("vector1", goodVector);
         r.assign("vector2", notGoodVector);
         if(goodVector.length > notGoodVector.length)
            result = r.eval("wilcox.test(vector1, vector2)");
         else
            result = r.eval("wilcox.test(vector2, vector1)");
         RVector vector = result.asVector();
         String w = vector.get(0).toString();
         String p = vector.get(2).toString();
         matcherW = patternW.matcher(w);
         matcherPScientific = patternPScientific.matcher(p);
         
         
         if(matcherW.find())
         {
            results[0] = Double.valueOf(matcherW.group());
         }
         
         if(matcherPScientific.find())
         {
            p = new BigDecimal(matcherPScientific.group()).toPlainString();
            results[1] = Double.valueOf(p);
         }
         else
         {
            matcherP = patternP.matcher(p);
            if(matcherP.find())
            {
               results[1] = Double.valueOf(matcherP.group());
            }
         }
      }
      
      return results;
   }
   
   /**
    * Faz o cálculo do D e do p do método de kolmogorovSmirnov
    * 
    * @since 1.0
    * @param goodVector um vetor contendo o valor de uma métrica métrica de todos os objetos do grupo dos objetos bons
    * @param notGoodVector um vetor contendo o valor de uma métrica métrica de todos os objetos do grupo dos objetos não bons
    * @return double[] - um vetor de double contendo na posição 0 o valor do D e na posição 1 o valor de p
    */
   /*realizado para avaliar as diferenças em relação as distribuições*/
   public double[] kolmogorovSmirnov(double[] goodVector, double[] notGoodVector)
   {
      boolean everythingIsZero = true;
      double[] results = new double[2];
      Pattern patternP = Pattern.compile("(\\d+\\.\\d+)");
      //Pattern patternPScientific = Pattern.compile("(\\d+\\.\\d+[E]+[\\+|\\-]?\\d+)");
      Pattern patternD = Pattern.compile("([0-9]+.?[0-9]+)");
      Matcher matcherP;
      //Matcher matcherPScientific;
      Matcher matcherD;
      REXP result;
      
      for(int i = 0; i < goodVector.length; i++)
      {
         if(goodVector[i] != 0)
            everythingIsZero = false;
         
         if(!everythingIsZero)
         {
            break;
         }
      }
      
      if(everythingIsZero)
      {
         for(int i = 0; i < notGoodVector.length; i++)
         {
            if(notGoodVector[i] != 0)
               everythingIsZero = false;

            if(everythingIsZero == false)
            {
               break;
            }
         }
      }
      
      if(everythingIsZero)
      {
         results[0] = -1.0;
         results[1] = -1.0;
      }
      else
      {  
         r.assign("vector1", goodVector);
         r.assign("vector2", notGoodVector);
         result = r.eval("ks.test(vector1, vector2, alternative=\"two.sided\")");
         RVector vector = result.asVector();
         String d = vector.get(0).toString();
         String p = vector.get(1).toString();
         matcherD = patternD.matcher(d);
         
         if(matcherD.find())
         {
            results[0] = Double.valueOf(matcherD.group());
         }
         
         matcherP = patternP.matcher(p);
         
         if(matcherP.find())
         {
            results[1] = Double.valueOf(matcherP.group());
         }
      }
      
      return results;
   }
   
   /** 
    * Cria uma pasta chamada graphics no diretório em que o programa está rodando e salva o gráfico de densidade gerado
    * pelo R dentro desta pasta
    * 
    * @since 1.0
    * @param notes um vetor de double contendo a nota dos especialistas ou dos usuários ou o número de coleçoes pessoais
    * @param tertile um vetor de double contendo o valor dos tercis calculados
    * @param minorNote a menor nota do subgrupo selecionado
    * @param majorNote a maior nota do subgrupo selecionado
    * @param qualityMeasure uma String contendo a perspectiva de qualidade usada para analisar o subgrupo selecionado que pode ser:
    *                       <code>"Personal Collections"</code>, <code>"Users"</code> ou <code>"Experts"</code>
    */
   public void createAndSaveRGraphic(double[] notes, double[] tertile, double minorNote, double majorNote, String qualityMeasure)
   {
      String diretorioLocal = System.getProperty("user.dir");
      File directory = new File(diretorioLocal + "/graphics");
      //System.out.println("minorNote = " + minorNote);
      if(directory.mkdir())
      {
         System.out.println("Diretório criado com sucesso.");
      }
      else
      {
         System.err.println("Diretório já existe ou não pode ser criado");
      }

      r.assign("notes", notes);
      r.eval("png('" + directory + "/rplot.png', width=635, height=385)");      
      r.eval("dens <- density(notes, width = 0.9)");
      
      if(qualityMeasure.equals("Personal Collections"))
      {
         r.eval("plot(dens, col=\"black\", ylim=c(0,1), xlim=c(" + String.valueOf(minorNote) + ", " 
                 + String.valueOf(majorNote) + ")," + "xaxs = \"i\", yaxs = \"i\", ann=FALSE, lwd = 2)");
         
         r.eval("title(xlab=\"Number of personal collections\", col.lab=\"steelblue\", cex.lab=1.2)");
      }
      else
      {
         r.eval("plot(dens, col=\"black\", ylim=c(0,1), xlim=c(" + String.valueOf(minorNote) + ", 5),"
                 + "xaxs = \"i\", yaxs = \"i\", ann=FALSE, lwd = 2)");
         
         if(qualityMeasure.equals("Users"))
         {
            r.eval("title(xlab=\"Users ratings\", col.lab=\"steelblue\", cex.lab=1.2)");
         }
         else
         {
            r.eval("title(xlab=\"Peer-reviewers ratings\", col.lab=\"steelblue\", cex.lab=1.2)");
         }
      }

      r.eval("title(ylab=\"Density\", col.lab=\"steelblue\", cex.lab=1.2)");
      r.eval("x1 <- min(which(dens$x >= " + String.valueOf(tertile[1]) + "))");
      r.eval("x2 <- max(which(dens$x < 5.5))");
      r.eval("x3 <- min(which(dens$x >= " + String.valueOf(minorNote) + "))");
      r.eval("x4 <- max(which(dens$x < " + String.valueOf(tertile[1]) + "))");
      r.eval("with(dens, polygon(x=c(x[c(x1,x1:x2,x2)]), y= c(0, y[x1:x2], 0), col=\"powderblue\", border=NA))");
      r.eval("with(dens, polygon(x=c(x[c(x3,x3:x4,x4)]), y= c(0, y[x3:x4], 0), col=\"indianred1\", border=NA))");
      r.eval("grid(col = \"gray\", lty = 1, lwd = 0.5, equilogs = TRUE)");
      r.eval("dev.off()");
   }
   
   /**
    * Calcula a média de um vetor
    * 
    * @since 1.0
    * @param vector um vetor de double contendo os valores dos quais se deve calcular a média
    * @return double - o valor da média
    */
   public double calculateMean(double[] vector)
   {
      r.assign("vector", vector);
      REXP result = r.eval("mean(vector)");
      return result.asDouble();
   }
   
   /**
    * Calcula a mediana de um vetor
    * 
    * @since 1.0
    * @param vector um vetor de double contendo os valores dos quais se deve calcular a mediana
    * @return double - o valor da mediana
    */
   public double calculateMedian(double[] vector)
   {
      r.assign("vector", vector);
      REXP result = r.eval("median(vector)");
      return result.asDouble();
   }
}
