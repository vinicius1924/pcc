/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classes;

import crawler.DataBaseCrawler;
import gui.MainWindow;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.BayesianLogisticRegression;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Range;
import weka.core.SerializationHelper;
import weka.core.converters.ConverterUtils.DataSource;

/**
 *
 * @author Vinicius Santos
 * 
 * @version 1.0
 *
 * @since 1.0
 */
public class Utils
{
   /**
    * Extrai os dados necessários para o cálculo dos tercis
    * 
    * @since 1.0
    * @param categoriesList o JList que contém as categorias
    * @param materialTypeList o JList que contém os tipos de materiais
    * @param usersRadio o JRadioButton da perspectiva de qualidade dos usuários
    * @param expertsRadio o JRadioButton da perspectiva de qualidade dos especialistas
    * @param personalCollectionsRadio o JRadioButton da perspectiva de qualidade do número de coleções pessoais
    * @return double[] - um vetor de double ordenado contendo as notas dadas por especialistas ou
    * usuários ou o número de coleções pessoais de cada objeto. Este valor é retornado conforme o que o usuário
    * selecionar na interface
    */
   public double[] extractDataForTheTertileCalculus(JList categoriesList, JList materialTypeList, JRadioButton usersRadio, 
                                                    JRadioButton expertsRadio, JRadioButton personalCollectionsRadio)
   {      
      ResultSet resultSet;
      String query;
      String categorie;
      String materialType;
      String qualityMeasure;
      List<Double> userRatings = new ArrayList<>();
      List<Double> expertRatings = new ArrayList<>();
      List<String> personalCollectionsNumber = new ArrayList<>();
      List<String> data;
      Components components = new Components();
      
      data = components.getComponentsData(categoriesList, materialTypeList, usersRadio, expertsRadio, personalCollectionsRadio);
      categorie = data.get(0);
      materialType = data.get(1);
      qualityMeasure = data.get(2);
      
      if (categorie != null && materialType != null)
      {
         /*
          * verifica o numero de objetos caso o usuario tenha selecionado 
          * categoria, tipo de material, e quality measure
          */
         if(qualityMeasure != null)
         {  
            //testa se o quality measure selecionado foi o de especialistas
            if(qualityMeasure.equals("Experts"))
            {
               query = "SELECT DISTINCT(`lodata`.`ID_LO`), `Stars_Reviews` FROM `lovaloration` \n"
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `lodata` ON `lovaloration`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO`  = `lodata`.`ID_LO` \n"
                       + "AND `locat_dat`.`locategories_ID_Cat` =  `locategories`.`ID_Cat` \n"
                       + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                       + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                       + "ORDER BY `Stars_Reviews` ASC";
               
               resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);
               
               try
               {
                  while(resultSet.next())
                  {
                     expertRatings.add(resultSet.getDouble("Stars_Reviews"));
                  }
               }
               catch(SQLException ex)
               {
                  System.err.println(ex);
               }
               
               double[] notasEspecialistas = new double[expertRatings.size()];
               int i = 0;
               
               for(Double e : expertRatings)
               {
                  notasEspecialistas[i] = e;
                  i++;
               }
               
               Arrays.sort(notasEspecialistas);
               
               return notasEspecialistas;
            }
            else
            {
               //testa se o quality measure selecionado foi o de usuários
               if(qualityMeasure.equals("Users"))
               {
                  query = "SELECT DISTINCT(`lodata`.`ID_LO`), `Stars_Comments` FROM `lovaloration` \n"
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `lodata` ON `lovaloration`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO`  = `lodata`.`ID_LO` \n"
                       + "AND `locat_dat`.`locategories_ID_Cat` =  `locategories`.`ID_Cat` \n"
                       + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                       + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                       + "ORDER BY `Stars_Comments` ASC";

                  
                  resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);
               
                  try
                  {
                     while(resultSet.next())
                     {
                        userRatings.add(resultSet.getDouble("Stars_Comments"));
                     }
                  }
                  catch(SQLException ex)
                  {
                     System.err.println(ex);
                  }

                  double[] userNotes = new double[userRatings.size()];
                  int i = 0;

                  for(Double e : userRatings)
                  {
                     userNotes[i] = e;
                     i++;
                  }
                  
                  Arrays.sort(userNotes);
                  
                  return userNotes;
               }
               else
               {
                  // o quality measure selecionado foi o de coleções pessoais
                  query = "SELECT DISTINCT(`lodata`.`ID_LO`), `Personal_Collections` FROM `lovaloration` \n"
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `lodata` ON `lovaloration`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO`  = `lodata`.`ID_LO` \n"
                       + "AND `locat_dat`.`locategories_ID_Cat` =  `locategories`.`ID_Cat` \n"
                       + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                       + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "' \n"
                       + "ORDER BY `Personal_Collections` ASC";
                  
                  resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);
               
                  try
                  {
                     while(resultSet.next())
                     {
                        personalCollectionsNumber.add(resultSet.getString("Personal_Collections"));
                     }
                  }
                  catch(SQLException ex)
                  {
                     System.err.println(ex);
                  }
                  
                  double[] personalCollectionsNum = new double[personalCollectionsNumber.size()];
                  int i = 0;

                  for(String e : personalCollectionsNumber)
                  {
                     personalCollectionsNum[i] = Double.parseDouble(e);
                     i++;
                  }

                  Arrays.sort(personalCollectionsNum);
                  
                  return personalCollectionsNum;
               }
            }
         }
      }
      else
      {
         //testa se foi selecionado categoria e quality measure
         if(categorie != null && qualityMeasure != null)
         {
            //testa se o quality measure selecionado foi o de especialistas
            if(qualityMeasure.equals("Experts"))
            {
               query = "SELECT DISTINCT(`lodata`.`ID_LO`), `Stars_Reviews` FROM `lovaloration` \n" 
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `lodata` ON `lovaloration`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO`  = `lodata`.`ID_LO` \n"
                       + "AND `locat_dat`.`locategories_ID_Cat` =  `locategories`.`ID_Cat` \n"
                       + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                       + "ORDER BY `Stars_Reviews` ASC";
               
               resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);
               
               try
               {
                  while(resultSet.next())
                  {
                     expertRatings.add(resultSet.getDouble("Stars_Reviews"));
                  }
               }
               catch(SQLException ex)
               {
                  System.err.println(ex);
               }
               
               double[] expertNotes = new double[expertRatings.size()];
               int i = 0;
               
               for(Double e : expertRatings)
               {
                  expertNotes[i] = e;
                  i++;
               }
               
               Arrays.sort(expertNotes);
               
               return expertNotes;
            }
            else
            {
               //testa se o quality measure selecionado foi o de usuários
               if(qualityMeasure.equals("Users"))
               {
                  query = "SELECT DISTINCT(`lodata`.`ID_LO`), `Stars_Comments` FROM `lovaloration` \n" 
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `lodata` ON `lovaloration`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO`  = `lodata`.`ID_LO` \n"
                       + "AND `locat_dat`.`locategories_ID_Cat` =  `locategories`.`ID_Cat` \n"
                       + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                       + "ORDER BY `Stars_Comments` ASC";
                  
                  resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);
               
                  try
                  {
                     while(resultSet.next())
                     {
                        userRatings.add(resultSet.getDouble("Stars_Comments"));
                     }
                  }
                  catch(SQLException ex)
                  {
                     System.err.println(ex);
                  }

                  double[] userNotes = new double[userRatings.size()];
                  int i = 0;

                  for(Double e : userRatings)
                  {
                     userNotes[i] = e;
                     i++;
                  }
                  
                  Arrays.sort(userNotes);
                  
                  return userNotes;
               }
               else
               {
                  // o quality measure selecionado foi o de coleções pessoais
                  query = "SELECT DISTINCT(`lodata`.`ID_LO`), `Personal_Collections` FROM `lovaloration` \n" 
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `lodata` ON `lovaloration`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`lodata_ID_LO`  = `lodata`.`ID_LO` \n"
                       + "AND `locat_dat`.`locategories_ID_Cat` =  `locategories`.`ID_Cat` \n"
                       + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                       + "ORDER BY `Personal_Collections` ASC";
                  
                  resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);
               
                  try
                  {
                     while(resultSet.next())
                     {
                        personalCollectionsNumber.add(resultSet.getString("Personal_Collections"));
                     }
                  }
                  catch(SQLException ex)
                  {
                     System.err.println(ex);
                  }
                  
                  double[] personalCollectionsNum = new double[personalCollectionsNumber.size()];
                  int i = 0;

                  for(String e : personalCollectionsNumber)
                  {
                     personalCollectionsNum[i] = Double.parseDouble(e);
                     i++;
                  }

                  Arrays.sort(personalCollectionsNum);
                  
                  return personalCollectionsNum;
               }
            }
         }
         else
         {
            //testa se foi selecionado tipo de material e quality measure
            if(materialType != null && qualityMeasure != null)
            {
               //testa se o quality measure selecionado foi o de especialistas
               if(qualityMeasure.equals("Experts"))
               {
                  query = "SELECT DISTINCT(`lodata`.`ID_LO`), `Stars_Reviews` FROM `lovaloration` \n" 
                          + "INNER JOIN `lodata` ON `lovaloration`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "AND `lodata`.`Material_Type` = " + "'" + materialType + "' \n"
                          + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                          + "ORDER BY `Stars_Reviews` ASC";
                  
                  resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);
               
                  try
                  {
                     while(resultSet.next())
                     {
                        expertRatings.add(resultSet.getDouble("Stars_Reviews"));
                     }
                  }
                  catch(SQLException ex)
                  {
                     System.err.println(ex);
                  }

                  double[] expertNotes = new double[expertRatings.size()];
                  int i = 0;

                  for(Double e : expertRatings)
                  {
                     expertNotes[i] = e;
                     i++;
                  }
                  
                  Arrays.sort(expertNotes);
                  
                  return expertNotes;
               }
               else
               {
                  //testa se o quality measure selecionado foi o de usuários
                  if(qualityMeasure.equals("Users"))
                  {
                     query = "SELECT DISTINCT(`lodata`.`ID_LO`), `Stars_Comments` FROM `lovaloration` \n" 
                          + "INNER JOIN `lodata` ON `lovaloration`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "AND `lodata`.`Material_Type` = " + "'" + materialType + "' \n"
                          + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                          + "ORDER BY `Stars_Comments` ASC";
                     
                     resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);
               
                     try
                     {
                        while(resultSet.next())
                        {
                           userRatings.add(resultSet.getDouble("Stars_Comments"));
                        }
                     }
                     catch(SQLException ex)
                     {
                        System.err.println(ex);
                     }

                     double[] userNotes = new double[userRatings.size()];
                     int i = 0;

                     for(Double e : userRatings)
                     {
                        userNotes[i] = e;
                        i++;
                     }
                     
                     Arrays.sort(userNotes);
                     
                     return userNotes;
                  }
                  else
                  {
                     // o quality measure selecionado foi o de coleções pessoais
                     query = "SELECT DISTINCT(`lodata`.`ID_LO`), `Personal_Collections` FROM `lovaloration` \n" 
                          + "INNER JOIN `lodata` ON `lovaloration`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "AND `lodata`.`Material_Type` = " + "'" + materialType + "' \n"
                          + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                          + "ORDER BY `Personal_Collections` ASC";
                     
                     resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);
               
                     try
                     {
                        while(resultSet.next())
                        {
                           personalCollectionsNumber.add(resultSet.getString("Personal_Collections"));
                        }
                     }
                     catch(SQLException ex)
                     {
                        System.err.println(ex);
                     }

                     double[] personalCollectionsNum = new double[personalCollectionsNumber.size()];
                     int i = 0;

                     for(String e : personalCollectionsNumber)
                     {
                        personalCollectionsNum[i] = Double.parseDouble(e);
                        i++;
                     }

                     Arrays.sort(personalCollectionsNum);
                     
                     return personalCollectionsNum;
                  }
               }
            }
            else
            {
               /* 
                * foi selecionado apenas quality measure 
                */
               // testa se o quality measure selecionado foi o de especialistas
               if(qualityMeasure.equals("Experts"))
               {
                  query = "SELECT DISTINCT(`lodata`.`ID_LO`), `Stars_Reviews` FROM `lovaloration` \n"
                        + "INNER JOIN `lodata` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                        + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                        + "AND `metrics`.`Page_Error` = '0' \n"
                        + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                        + "ORDER BY `Stars_Reviews` ASC";

                  resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);
               
                  try
                  {
                     while(resultSet.next())
                     {
                        expertRatings.add(resultSet.getDouble("Stars_Reviews"));
                     }
                  }
                  catch(SQLException ex)
                  {
                     System.err.println(ex);
                  }

                  double[] expertNotes = new double[expertRatings.size()];
                  int i = 0;

                  for(Double e : expertRatings)
                  {
                     expertNotes[i] = e;
                     i++;
                  }
                  
                  Arrays.sort(expertNotes);
                  
                  return expertNotes;
               }
               else
               {
                  // testa se o quality measure selecionado foi o de usuários
                  if(qualityMeasure.equals("Users"))
                  {
                     query = "SELECT DISTINCT(`lodata`.`ID_LO`), `Stars_Comments` FROM `lovaloration` \n"
                        + "INNER JOIN `lodata` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                        + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                        + "AND `metrics`.`Page_Error` = '0' \n"
                        + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                        + "ORDER BY `Stars_Comments` ASC";

                     resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);
               
                     try
                     {
                        while(resultSet.next())
                        {
                           userRatings.add(resultSet.getDouble("Stars_Comments"));
                        }
                     }
                     catch(SQLException ex)
                     {
                        System.err.println(ex);
                     }

                     double[] userNotes = new double[userRatings.size()];
                     int i = 0;

                     for(Double e : userRatings)
                     {
                        userNotes[i] = e;
                        i++;
                     }
                     
                     Arrays.sort(userNotes);
                     
                     return userNotes;
                  }
                  else
                  {
                     // o quality measure selecionado foi o de personal collections
                     query = "SELECT DISTINCT(`lodata`.`ID_LO`), `Personal_Collections` FROM `lovaloration` \n"
                           + "INNER JOIN `lodata` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                           + "INNER JOIN `metrics` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                           + "AND `metrics`.`Page_Error` = '0' \n"
                           + "AND  `Personal_Collections` !=  'none' \n"
                           + "ORDER BY `Personal_Collections` ASC";

                     resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, query);
               
                     try
                     {
                        while(resultSet.next())
                        {
                           personalCollectionsNumber.add(resultSet.getString("Personal_Collections"));
                        }
                     }
                     catch(SQLException ex)
                     {
                        System.err.println(ex);
                     }

                     double[] personalCollectionsNum = new double[personalCollectionsNumber.size()];
                     int i = 0;

                     for(String e : personalCollectionsNumber)
                     {
                        personalCollectionsNum[i] = Double.parseDouble(e);
                        i++;
                     }

                     Arrays.sort(personalCollectionsNum);
                     
                     return personalCollectionsNum;
                  }
               }
            }
         }
      }
      return null;
   }
   
   /**
    * Pega o ID, categoria, tipo de material, dados e metricas dos objetos
    * 
    * @since 1.0
    * @param queryAllDataFromAllLOs a query que fará a consulta dos dados dos objetos do banco de dados
    * @param queryAllDataAndMetricsFromAllLOs a query que fará a consulta dos dados e das metricas do banco de dados
    * @param categorie
    * @param materialType
    * @param tertil
    * @param goodOrNotGoodObjects se serão consultados os objetos bons ou os não bons. 1 para bons e -1 para não bons
    * @param evaluationType se será usado notas de especilaistas (ou alguma das notas que formam a nota dos mesmos), de usuários 
    *                       ou o número de coleções pessoais.
    *                       <p>
    *                         Os valores que este parâmetro podem assumir são: 1 para nota de especialistas, 2 para content 
    *                         quality rating da nota dos especialistas, 3 para efectiveness rating da nota dos especialistas,
    *                         4 para ease of use rating da nota dos especialistas, 5 para notas de usuários e 6 para coleções
    *                         pessoais.
    *                       </p>
    * @return List<LearningObject> - devolve uma lista contendo os objetos selecionados com suas respectivas metricas
    */
   public List<LearningObject> getLOs(String queryAllDataFromAllLOs, String queryAllDataAndMetricsFromAllLOs, 
                                                   String categorie, String materialType, double tertil, 
                                                   int goodOrNotGoodObjects, int evaluationType)
   {
      ResultSet resultSet;
      ResultSetMetaData rsmd = null;
      int numColumns = 0;
      int numColumnsOfData = 0;
      LearningObject learningObject;
      String intermediateQueryMetrics = null;
      String LOsQueryDataAndMetrics = null;
      List<LearningObject> learningObjectsWithDataAndMetricsList = new ArrayList<>();
            
      resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, queryAllDataFromAllLOs);
      
      // pega o numero de colunas selecionadas na query
      try
      {
         rsmd = resultSet.getMetaData();
         numColumnsOfData = rsmd.getColumnCount();  
      }
      catch (SQLException ex)
      {
         System.err.println(ex);
      }
      
      resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, queryAllDataAndMetricsFromAllLOs);
      // pega o numero de colunas selecionadas na query
      try
      {
         rsmd = resultSet.getMetaData();  
         numColumns = rsmd.getColumnCount();  
      }
      catch (SQLException ex)
      {
         System.err.println(ex);
      }
      
      /* 
       * Faz um for fazendo um cast para double das colunas e cria uma query intermediaria
       * que será adicionada a query que vai pegar todos os objetos que estão classificados como bons conforme
       * os valores tercis.
       * O for começa nesta coluna pois o que foi selecionado pela query e que vem antes são dados e
       * não metricas
       */
      for(int i = numColumnsOfData + 2; i <= numColumns; i++)
      {
         try
         {
            if(i == numColumns)
            {
               if(intermediateQueryMetrics == null)
               {
                  intermediateQueryMetrics = "CAST(`metrics`." + "`" + rsmd.getColumnName(i) + "` AS DECIMAL(20, 2)) AS "
                           + rsmd.getColumnName(i) + " \n";
               }
               else
               {
                  intermediateQueryMetrics = intermediateQueryMetrics + "CAST(`metrics`." + "`" + rsmd.getColumnName(i) + "` AS DECIMAL(20, 2)) AS "
                           + rsmd.getColumnName(i) + " \n";
               }
            }
            else
            {
               if(intermediateQueryMetrics == null)
               {
                  intermediateQueryMetrics = "CAST(`metrics`." + "`" + rsmd.getColumnName(i) + "` AS DECIMAL(20, 2)) AS "
                          + rsmd.getColumnName(i) + ", \n";
               }
               else
               {
                  intermediateQueryMetrics = intermediateQueryMetrics + "CAST(`metrics`." + "`" + rsmd.getColumnName(i) + "` AS DECIMAL(20, 2)) AS "
                          + rsmd.getColumnName(i) + ", \n";
               }
            }
         }
         catch (SQLException ex)
         {
            System.err.println(ex);
         }
      }

      // objetos bons
      if(goodOrNotGoodObjects == 1)
      {
         // notas de especialistas
         if(evaluationType == 1)
         {
            if(materialType == null && categorie == null)
            {
               // Cria a query para pegar todos os objetos que estão classificados como bons conforme os valores tercis
               LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                       + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `lovaloration`.`Stars_Reviews` >= " + "'" + tertil + "' \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "ORDER BY `Stars_Reviews` ASC";
            }
            else
            {
               // caso não seja selecionado um tipo de material
               if(materialType == null)
               {
                  // Cria a query para pegar todos os objetos que estão classificados como bons conforme os valores tercis
                  LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Reviews` >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Reviews` ASC";
               }
               else
               {
                  if(categorie == null)
                  {
                     // Cria a query para pegar todos os objetos que estão classificados como bons conforme os valores tercis
                     LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Stars_Reviews` >= " + "'" + tertil + "' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY `Stars_Reviews` ASC";
                  }
                  /* categoria, tipo de material e tipo de avaliação foram selecionados */
                  else
                  {
                     // Cria a query para pegar todos os objetos que estão classificados como bons conforme os valores tercis
                     LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                             + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                             + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                             + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Stars_Reviews` >= " + "'" + tertil + "' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                             + "ORDER BY `Stars_Reviews` ASC";
                  }
               }
            }
         }
         else
         {
            // notas de usuários
            if(evaluationType == 5)
            {
               if(materialType == null && categorie == null)
               {
                  // Cria a query para pegar todos os objetos que estão classificados como bons conforme os valores tercis
                  LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                          + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Comments` >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Comments` ASC";
               }
               else
               {
                  // caso não seja selecionado um tipo de material
                  if(materialType == null)
                  {
                     // Cria a query para pegar todos os objetos que estão classificados como bons conforme os valores tercis
                     LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                             + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                             + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                             + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Stars_Comments` >= " + "'" + tertil + "' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY `Stars_Comments` ASC";
                  }
                  else
                  {
                     if(categorie == null)
                     {
                        // Cria a query para pegar todos os objetos que estão classificados como bons conforme os valores tercis
                        LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                                + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                                + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                + "AND `lovaloration`.`Stars_Comments` >= " + "'" + tertil + "' \n"
                                + "AND `metrics`.`Page_Error` = '0' \n"
                                + "ORDER BY `Stars_Comments` ASC";
                     }
                     /* categoria, tipo de material e tipo de avaliação foram selecionados */
                     else
                     {
                        // Cria a query para pegar todos os objetos que estão classificados como bons conforme os valores tercis
                        LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                                + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                                + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                                + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                                + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                + "AND `lovaloration`.`Stars_Comments` >= " + "'" + tertil + "' \n"
                                + "AND `metrics`.`Page_Error` = '0' \n"
                                + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                                + "ORDER BY `Stars_Comments` ASC";
                     }
                  }
               }
            }
            else
            {
               // número de coleções pessoais
               if(evaluationType == 6)
               {
                  if(materialType == null && categorie == null)
                  {  
                     // Cria a query para pegar todos os objetos que estão classificados como bons conforme os valores tercis
                     LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                          + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                          + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";
                  }
                  else
                  {
                     // caso não seja selecionado um tipo de material
                     if(materialType == null)
                     {
                        LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                              + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                              + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                              + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                              + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                              + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                              + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                              + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) >= " + "'" + tertil + "' \n"
                              + "AND `metrics`.`Page_Error` = '0' \n"
                              + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";
                     }
                     else
                     {
                        if(categorie == null)
                        {
                           LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                                 + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                                 + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                 + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                 + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                                 + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) >= " + "'" + tertil + "' \n"
                                 + "AND `metrics`.`Page_Error` = '0' \n"
                                 + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";
                        }
                        /* categoria, tipo de material e tipo de avaliação foram selecionados */
                        else
                        {
                           LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                                 + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                                 + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                                 + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                                 + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                 + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                 + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                                 + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) >= " + "'" + tertil + "' \n"
                                 + "AND `metrics`.`Page_Error` = '0' \n"
                                 + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                                 + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";
                        }
                     }
                  }
               }
            }
         }
      }
      else
      {
         // objetos não bons
         if(goodOrNotGoodObjects == -1)
         {
            // notas de especialistas
            if(evaluationType == 1)
            {
               if(materialType == null && categorie == null)
               {
                  // Cria a query para pegar todos os objetos que estão classificados como bons conforme os valores tercis
                  LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                          + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                          + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Reviews` ASC";
               }
               else
               {
                  // caso não seja selecionado um tipo de material
                  if(materialType == null)
                  {
                     // Cria a query para pegar todos os objetos que estão classificados como não bons conforme os valores tercis
                     LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                             + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                             + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                             + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                             + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY `Stars_Reviews` ASC";
                  }
                  else
                  {
                     if(categorie == null)
                     {
                        // Cria a query para pegar todos os objetos que estão classificados como não bons conforme os valores tercis
                        LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                                + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                                + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                                + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                                + "AND `metrics`.`Page_Error` = '0' \n"
                                + "ORDER BY `Stars_Reviews` ASC";
                     }
                     /* categoria, tipo de material e tipo de avaliação foram selecionados */
                     else
                     {
                        // Cria a query para pegar todos os objetos que estão classificados como não bons conforme os valores tercis
                        LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                                + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                                + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                                + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                                + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                                + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                                + "AND `metrics`.`Page_Error` = '0' \n"
                                + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                                + "ORDER BY `Stars_Reviews` ASC";
                     }
                  }
               }
            }
            else
            {
               // notas de usuários
               if(evaluationType == 5)
               {
                  if(materialType == null && categorie == null)
                  {
                     // Cria a query para pegar todos os objetos que estão classificados como bons conforme os valores tercis
                     LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Stars_Comments` < " + "'" + tertil + "' \n"
                             + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY `Stars_Comments` ASC";
                  }
                  else
                  {
                     // caso não seja selecionado um tipo de material
                     if(materialType == null)
                     {
                        // Cria a query para pegar todos os objetos que estão classificados como não bons conforme os valores tercis
                        LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                                + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                                + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                                + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                                + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                + "AND `lovaloration`.`Stars_Comments` < " + "'" + tertil + "' \n"
                                + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                                + "AND `metrics`.`Page_Error` = '0' \n"
                                + "ORDER BY `Stars_Comments` ASC";
                     }
                     else
                     {
                        if(categorie == null)
                        {
                           // Cria a query para pegar todos os objetos que estão classificados como não bons conforme os valores tercis
                           LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                                   + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                                   + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                   + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                   + "AND `lovaloration`.`Stars_Comments` < " + "'" + tertil + "' \n"
                                   + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                                   + "AND `metrics`.`Page_Error` = '0' \n"
                                   + "ORDER BY `Stars_Comments` ASC";
                        }
                        /* categoria, tipo de material e tipo de avaliação foram selecionados */
                        else
                        {
                           // Cria a query para pegar todos os objetos que estão classificados como não bons conforme os valores tercis
                           LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                                   + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                                   + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                                   + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                                   + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                   + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                   + "AND `lovaloration`.`Stars_Comments` < " + "'" + tertil + "' \n"
                                   + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                                   + "AND `metrics`.`Page_Error` = '0' \n"
                                   + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                                   + "ORDER BY `Stars_Comments` ASC";
                        }
                     }
                  }
               }
               else
               {
                  // número de coleções pessoais
                  if(evaluationType == 6)
                  {
                     if(materialType == null && categorie == null)
                     {
                        // query que seleciona tudo da tabela metrics
                        LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) < " + "'" + tertil + "' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) > '0' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";
                     }
                     else
                     {
                        // caso não seja selecionado um tipo de material
                        if(materialType == null)
                        {
                           LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                                 + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                                 + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                                 + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                                 + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                 + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                 + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                                 + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) < " + "'" + tertil + "' \n"
                                 + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) > '0' \n"
                                 + "AND `metrics`.`Page_Error` = '0' \n"                        
                                 + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";
                        }
                        else
                        {
                           if(categorie == null)
                           {
                              LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                                    + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                                    + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                    + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                    + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                                    + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) < " + "'" + tertil + "' \n"
                                    + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) > '0' \n"
                                    + "AND `metrics`.`Page_Error` = '0' \n"
                                    + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";
                           }
                           else
                           {
                              /* categoria, tipo de material e tipo de avaliação foram selecionados */
                              LOsQueryDataAndMetrics = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata` . *, " + intermediateQueryMetrics + "FROM `metrics` \n"
                                    + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                                    + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                                    + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                                    + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                    + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                                    + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                                    + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) < " + "'" + tertil + "' \n"
                                    + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) > '0' \n"
                                    + "AND `metrics`.`Page_Error` = '0' \n"
                                    + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                                    + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      resultSet = MainWindow.dataBaseMerlot.consultaBancoDeDados(MainWindow.conexionMerlot, LOsQueryDataAndMetrics);
      
      try
      {
         rsmd = resultSet.getMetaData();  
         numColumns = rsmd.getColumnCount();  
      }
      catch (SQLException ex)
      {
         System.err.println(ex);
      }

      System.out.println("\n");

      try
      {
         while(resultSet.next())
         {
            learningObject = new LearningObject(numColumns - numColumnsOfData, numColumnsOfData - 2);
            
            // seta a categoria do objeto de aprendizagem caso tenha sido selecionada alguma pelo usuário na interface
            if(categorie != null)
            {
               learningObject.setCategory(categorie);
            }
            else
            {
               learningObject.setCategory(null);
            }
            
            // seta o tipo de material do objeto de aprendizagem caso tenha sido selecionado algum pelo usuário na interface
            if(materialType != null)
            {
               learningObject.setMaterial_Type(materialType);
            }
            else
            {
               learningObject.setMaterial_Type(null);
            }
            
            for(int i = 2; i <= numColumnsOfData; i++)
            {
               if(i == 2)
               {
                  learningObject.setID_LO(resultSet.getInt(i));
               }
               else
               {
                  /* testa o tipo de dado que foi pego do banco de dados e se for VARCHAR ou TEXT adiciona o nome do dado
                   * no arraylist de nome de dados e adiciona, na mesma posicao em que o nome do dado foi adicionado no arraylist
                   * de nome do dado, o dado em si no vetor de dados com tipo String e -1 no vetor de dados com tipo int
                   */
                  if(rsmd.getColumnTypeName(i).equals("VARCHAR") || rsmd.getColumnTypeName(i).equals("TEXT"))
                  {
                     learningObject.addNameOfData(rsmd.getColumnName(i));
                     learningObject.setDataString(learningObject.getNameOfDataSize() - 1, resultSet.getString(i));
                     learningObject.setDataInt(learningObject.getNameOfDataSize() - 1, -1);
                  }
                  else
                  {
                     if(rsmd.getColumnTypeName(i).equals("TINYINT") || rsmd.getColumnTypeName(i).equals("INT") 
                             || rsmd.getColumnTypeName(i).equals("INT UNSIGNED"))
                     {
                        learningObject.addNameOfData(rsmd.getColumnName(i));
                        learningObject.setDataInt(learningObject.getNameOfDataSize() - 1, resultSet.getInt(i));
                        learningObject.setDataString(learningObject.getNameOfDataSize() - 1, null);
                     }
                     /* falta adicionar um else adicionando no objeto os dados no formato de data */
                  }
               }
            }
            
            for(int j = numColumnsOfData + 1; j <= numColumns; j++)
            {
               learningObject.addNameOfMetric(rsmd.getColumnName(j));
               learningObject.setMetrics(learningObject.getNameOfMetricsSize() - 1, resultSet.getDouble(j));
            }
            
            learningObjectsWithDataAndMetricsList.add(learningObject);
         }
      }
      catch (SQLException ex)
      {
         System.err.println(ex);
      }

      return learningObjectsWithDataAndMetricsList;
   }
   
   /**
    * Separa os objetos em duas categorias: bons e não bons
    * 
    * @since 1.0
    * @param categoriesList o JList que contém as categorias
    * @param materialTypeList o JList que contém os tipos de materiais
    * @param usersRadio o JRadioButton da perspectiva de qualidade dos usuários
    * @param expertsRadio o JRadioButton da perspectiva de qualidade dos especialistas
    * @param personalCollectionsRadio o JRadioButton da perspectiva de qualidade do número de coleções pessoais
    * @param tertile o vetor de double contendo os tercis que serão usados para a separação
    * @return List<List<Double>> - devolve uma lista contendo duas listas,
    * a primeira do grupo dos objetos bons e a segunda dos objetos não bons
    */
   public List<List<LearningObject>> separateObjectsIntoGroups(double[] tertile, JList categoriesList, JList materialTypeList, JRadioButton usersRadio, 
                                                    JRadioButton expertsRadio, JRadioButton personalCollectionsRadio/*, 
                                                    JComboBox expertsNotesComboBox*/)
   {
      List<List<LearningObject>> groups = new ArrayList<>();
      List<LearningObject> goodLOs;
      List<LearningObject> notGoodLOs;
      ResultSet resultSet = null;
      double tertil = tertile[1];
      String categorie;
      String materialType;
      String qualityMeasure;
      String queryAllDataAndMetricsFromGoodLOs;
      String queryAllDataAndMetricsFromNotGoodLOs;
      String queryAllDataFromGoodLOs;
      String queryAllDataFromNotGoodLOs;
      List<String> data;
      ResultSetMetaData rsmd = null;
      Components components = new Components();
      
      data = components.getComponentsData(categoriesList, materialTypeList, usersRadio, expertsRadio, 
                                          personalCollectionsRadio/*, expertsNotesComboBox*/);
      categorie = data.get(0);
      materialType = data.get(1);
      qualityMeasure = data.get(2);
      
      if (categorie != null && materialType != null)
      {
         if(qualityMeasure != null)
         {
            /* 
             * Testa se o quality measure selecionado foi o de especialistas. Se foi então é feita uma consulta que pega
             * todos os objetos de ID_LO distintos da tabela lodata e todas as metricas da tabela metrics levando em conta
             * a categoria e o tipo de material selecionado e se a média das notas dos especialistas (a nota dos especialistas 
             * é formada pela média de três notas que eles dão e que são: content_quality, effectiveness e easy_of_use) 
             * é maior ou igual ao tercil calculado para os objetos bons e menor para os objetos não bons
             */
            if(qualityMeasure.equals("Experts"))
            {
               /*------------------------------------------- bons ----------------------------------------------------------*/
               
               /*
                * Pode ser que não tenha o mesmo número de objetos contados quando as opções
                * são selecionadas na interface pois a alguns objetos da tabela lodata 
                * podem não estar relacionados com a tabela metrics
                */
               
               // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
               queryAllDataAndMetricsFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                       + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `lovaloration`.`Stars_Reviews` >= " + "'" + tertil + "' \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                       + "ORDER BY `Stars_Reviews` ASC";
   
               // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
               queryAllDataFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                       + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `lovaloration`.`Stars_Reviews` >= " + "'" + tertil + "' \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                       + "ORDER BY `Stars_Reviews` ASC";
      
               goodLOs = getLOs(queryAllDataFromGoodLOs, queryAllDataAndMetricsFromGoodLOs, 
                                                  categorie, materialType, tertil, 1, 1);
               
               // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
               groups.add(0, goodLOs);
                 
               /*------------------------------------------------------------------------------------------------------*/
               
               /*--------------------------------------- não bons -----------------------------------------------------*/
               
               // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
               queryAllDataAndMetricsFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                       + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                       + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                       + "ORDER BY `Stars_Reviews` ASC";
               
               // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
               queryAllDataFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                       + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                       + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                       + "ORDER BY `Stars_Reviews` ASC";
               
               notGoodLOs = getLOs(queryAllDataFromNotGoodLOs, queryAllDataAndMetricsFromNotGoodLOs, 
                                    categorie, materialType, tertil, -1, 1);
               
               // adiciona o grupo dos objetos não bons com seus respectivos dados e metricas no List que será retornado
               groups.add(1, notGoodLOs);
                  
               /*------------------------------------------------------------------------------------------------------*/
            }
            else
            {
               /* 
                * Testa se o quality measure selecionado foi o de especialistas. Se foi então é feita uma consulta que pega
                * todos os objetos de ID_LO distintos da tabela lodata e todas as metricas da tabela metrics levando em conta
                * a categoria e o tipo de material selecionado e se a média das notas dos especialistas (a nota dos especialistas 
                * é formada pela média de três notas que eles dão e que são: content_quality, effectiveness e easy_of_use) 
                * é maior ou igual ao tercil calculado para os objetos bons e menor para os objetos não bons
                */
               if(qualityMeasure.equals("Users"))
               {
                  /*------------------------------------------- bons ----------------------------------------------------------*/
                  
                  /*
                   * Pode ser que não tenha o mesmo número de objetos contados quando as opções
                   * são selecionadas na interface pois a alguns objetos da tabela lodata 
                   * podem não estar relacionados com a tabela metrics
                   */
                  
                  // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                  queryAllDataAndMetricsFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat` = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Comments` >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                          + "ORDER BY `Stars_Comments` ASC";

                  // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                  queryAllDataFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Comments` >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                          + "ORDER BY `Stars_Comments` ASC";
                  
                  goodLOs = getLOs(queryAllDataFromGoodLOs, queryAllDataAndMetricsFromGoodLOs, 
                                                                                        categorie, materialType, tertil, 1, 5);
                  
                  // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                  groups.add(0, goodLOs);

                  /*------------------------------------------------------------------------------------------------------*/

                  /*--------------------------------------- não bons -----------------------------------------------------*/
                  
                  // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                  queryAllDataAndMetricsFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Comments` < " + "'" + tertil + "' \n"
                          + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                          + "ORDER BY `Stars_Comments` ASC";

                  // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                  queryAllDataFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                          + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                          + "ORDER BY `Stars_Reviews` ASC";
                  
                  notGoodLOs = getLOs(queryAllDataFromNotGoodLOs, 
                                                                  queryAllDataAndMetricsFromNotGoodLOs, categorie, materialType, 
                                                                  tertil, -1, 5);
               
                  // adiciona o grupo dos objetos não bons com seus respectivos dados e metricas no List que será retornado
                  groups.add(1, notGoodLOs);
                                    
                  /*------------------------------------------------------------------------------------------------------*/
               }
               else
               {
                  // o quality measure selecionado foi o de coleções pessoais
                  
                  /*------------------------------------------- bons ----------------------------------------------------------*/
                  
                  /*
                   * pode ser que não tenha o mesmo número de objetos contados quando as opções
                   * são selecionadas na interface pois a alguns objetos da tabela lodata 
                   * podem não estar relacionados com a tabela metrics
                   */
                  
                  // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                  queryAllDataAndMetricsFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat` = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                          + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                          + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";

                  // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                  queryAllDataFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                          + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                          + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";
               
                  goodLOs = getLOs(queryAllDataFromGoodLOs, queryAllDataAndMetricsFromGoodLOs, 
                                                                                        categorie, materialType, tertil, 1, 6);
                  
                  // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                  groups.add(0, goodLOs);

                  /*------------------------------------------------------------------------------------------------------*/
               
                  /*--------------------------------------- não bons -----------------------------------------------------*/

                  // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                  queryAllDataAndMetricsFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat` = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                          + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) < " + "'" + tertil + "' \n"
                          + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) > '0' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                          + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";

                  // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                  queryAllDataFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                          + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) < " + "'" + tertil + "' \n"
                          + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) > '0' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "AND `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                          + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";
               
                  notGoodLOs = getLOs(queryAllDataFromNotGoodLOs, queryAllDataAndMetricsFromNotGoodLOs, 
                                                                                        categorie, materialType, tertil, -1, 6);
                  
                  // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                  groups.add(1, notGoodLOs);
               
                  /*------------------------------------------------------------------------------------------------------*/
               }
            }
         }
      }
      else
      {
         if(categorie != null && qualityMeasure != null)
         {
            /*------------------------------------------- bons ----------------------------------------------------------*/
            
            /*
             * pode ser que não tenha o mesmo número de objetos contados quando as opções
             * são selecionadas na interface pois a alguns objetos da tabela lodata 
             * podem não estar relacionados com a tabela metrics
             */
            if(qualityMeasure.equals("Experts"))
            {
               // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
               queryAllDataAndMetricsFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                       + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `lovaloration`.`Stars_Reviews` >= " + "'" + tertil + "' \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "ORDER BY `Stars_Reviews` ASC";
   
               // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
               queryAllDataFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                       + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `lovaloration`.`Stars_Reviews` >= " + "'" + tertil + "' \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "ORDER BY `Stars_Reviews` ASC";
      
               goodLOs = getLOs(queryAllDataFromGoodLOs, queryAllDataAndMetricsFromGoodLOs, 
                                                                                        categorie, null, tertil, 1, 1);
               
               // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
               groups.add(0, goodLOs);
               
               /*------------------------------------------------------------------------------------------------------*/
               
               /*--------------------------------------- não bons -----------------------------------------------------*/
               
               // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
               queryAllDataAndMetricsFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                       + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                       + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "ORDER BY `Stars_Reviews` ASC";
               
               // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
               queryAllDataFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                       + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                       + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                       + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                       + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                       + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                       + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                       + "AND `metrics`.`Page_Error` = '0' \n"
                       + "ORDER BY `Stars_Reviews` ASC";
               
               notGoodLOs = getLOs(queryAllDataFromNotGoodLOs, 
                                                                  queryAllDataAndMetricsFromNotGoodLOs, categorie, null, 
                                                                  tertil, -1, 1);
               
               // adiciona o grupo dos objetos não bons com seus respectivos dados e metricas no List que será retornado
               groups.add(1, notGoodLOs);
               
               /*------------------------------------------------------------------------------------------------------*/
            }
            else
            {
               if(qualityMeasure.equals("Users"))
               {
                  /*------------------------------------------- bons ----------------------------------------------------------*/
                  /*
                   * pode ser que não tenha o mesmo número de objetos contados quando as opções
                   * são selecionadas na interface pois a alguns objetos da tabela lodata 
                   * podem não estar relacionados com a tabela metrics
                   */
                  
                  // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                  queryAllDataAndMetricsFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat` = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Comments` >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Comments` ASC";

                  // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                  queryAllDataFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Comments` >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Comments` ASC";
                  
                  goodLOs = getLOs(queryAllDataFromGoodLOs, queryAllDataAndMetricsFromGoodLOs, 
                                                                                        categorie, null, tertil, 1, 5);
                  
                  // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                  groups.add(0, goodLOs);

                  /*------------------------------------------------------------------------------------------------------*/

                  /*--------------------------------------- não bons -----------------------------------------------------*/
                  
                  // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                  queryAllDataAndMetricsFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Comments` < " + "'" + tertil + "' \n"
                          + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Comments` ASC";

                  // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                  queryAllDataFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                          + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Reviews` ASC";
                  
                  notGoodLOs = getLOs(queryAllDataFromNotGoodLOs, 
                                                                  queryAllDataAndMetricsFromNotGoodLOs, categorie, null, 
                                                                  tertil, -1, 5);
               
                  // adiciona o grupo dos objetos não bons com seus respectivos dados e metricas no List que será retornado
                  groups.add(1, notGoodLOs);
                  
                  /*------------------------------------------------------------------------------------------------------*/
               }
               else
               {
                  // o quality measure selecionado foi o de coleções pessoais
                  
                  /*------------------------------------------- bons ----------------------------------------------------------*/
                  
                  /*
                   * pode ser que não tenha o mesmo número de objetos contados quando as opções
                   * são selecionadas na interface pois a alguns objetos da tabela lodata 
                   * podem não estar relacionados com a tabela metrics
                   */
                  
                  // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                  queryAllDataAndMetricsFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat` = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                          + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";

                  // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                  queryAllDataFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                          + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";
               
                  goodLOs = getLOs(queryAllDataFromGoodLOs, queryAllDataAndMetricsFromGoodLOs, 
                                                                                        categorie, null, tertil, 1, 6);
                  
                  // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                  groups.add(0, goodLOs);

                  /*------------------------------------------------------------------------------------------------------*/
               
                  /*--------------------------------------- não bons -----------------------------------------------------*/

                  // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                  queryAllDataAndMetricsFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat` = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                          + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) < " + "'" + tertil + "' \n"
                          + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) > '0' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"                          
                          + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";

                  // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                  queryAllDataFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                          + "INNER JOIN `locategories` ON `locategories`.`Categorie` LIKE " + "'" + categorie + "%' \n"
                          + "INNER JOIN `locat_dat` ON `locat_dat`.`locategories_ID_Cat`  = `locategories`.`ID_Cat` \n"
                          + "INNER JOIN `lodata` ON `locat_dat`.`lodata_ID_LO` =  `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                          + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) < " + "'" + tertil + "' \n"
                          + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) > '0' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";
               
                  notGoodLOs = getLOs(queryAllDataFromNotGoodLOs, queryAllDataAndMetricsFromNotGoodLOs, 
                                                                                        categorie, null, tertil, -1, 6);
                  
                  // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                  groups.add(1, notGoodLOs);
               
                  /*------------------------------------------------------------------------------------------------------*/
               }
            }
         }
         else
         {
            if(materialType != null && qualityMeasure != null)
            {
               if(qualityMeasure.equals("Experts"))
               {
                  /*------------------------------------------- bons ----------------------------------------------------------*/
               
                  /*
                   * pode ser que não tenha o mesmo número de objetos contados quando as opções
                   * são selecionadas na interface pois a alguns objetos da tabela lodata 
                   * podem não estar relacionados com a tabela metrics
                   */
                  
                  // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                  queryAllDataAndMetricsFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                          + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Reviews` >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Reviews` ASC";

                  // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                  queryAllDataFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                          + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Reviews` >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Reviews` ASC";

                  goodLOs = getLOs(queryAllDataFromGoodLOs, queryAllDataAndMetricsFromGoodLOs, 
                                                                                           null, materialType, tertil, 1, 1);

                  // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                  groups.add(0, goodLOs);

                  /*------------------------------------------------------------------------------------------------------*/

                  /*--------------------------------------- não bons -----------------------------------------------------*/
               
                  // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                  queryAllDataAndMetricsFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                          + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                          + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Reviews` ASC";

                  // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                  queryAllDataFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                          + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                          + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Reviews` ASC";

                  notGoodLOs = getLOs(queryAllDataFromNotGoodLOs, 
                                                                     queryAllDataAndMetricsFromNotGoodLOs, null, materialType, 
                                                                     tertil, -1, 1);

                  // adiciona o grupo dos objetos não bons com seus respectivos dados e metricas no List que será retornado
                  groups.add(1, notGoodLOs);

                  /*------------------------------------------------------------------------------------------------------*/
               }
               else
               {
                  if(qualityMeasure.equals("Users"))
                  {
                     /*------------------------------------------- bons ----------------------------------------------------------*/
               
                     /*
                      * pode ser que não tenha o mesmo número de objetos contados quando as opções
                      * são selecionadas na interface pois a alguns objetos da tabela lodata 
                      * podem não estar relacionados com a tabela metrics
                      */
                  
                     // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                     queryAllDataAndMetricsFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Stars_Comments` >= " + "'" + tertil + "' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY `Stars_Comments` ASC";

                     // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                     queryAllDataFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Stars_Comments` >= " + "'" + tertil + "' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY `Stars_Comments` ASC";

                     goodLOs = getLOs(queryAllDataFromGoodLOs, queryAllDataAndMetricsFromGoodLOs, 
                                                                                              null, materialType, tertil, 1, 5);

                     // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                     groups.add(0, goodLOs);

                     /*------------------------------------------------------------------------------------------------------*/

                     /*--------------------------------------- não bons -----------------------------------------------------*/

                     // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                     queryAllDataAndMetricsFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Stars_Comments` < " + "'" + tertil + "' \n"
                             + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY `Stars_Comments` ASC";

                     // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                     queryAllDataFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Stars_Comments` < " + "'" + tertil + "' \n"
                             + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY `Stars_Comments` ASC";

                     notGoodLOs = getLOs(queryAllDataFromNotGoodLOs, 
                                                                        queryAllDataAndMetricsFromNotGoodLOs, null, materialType, 
                                                                        tertil, -1, 5);

                     // adiciona o grupo dos objetos não bons com seus respectivos dados e metricas no List que será retornado
                     groups.add(1, notGoodLOs);

                     /*------------------------------------------------------------------------------------------------------*/
                  }
                  else
                  {
                     // o quality measure selecionado foi o de coleções pessoais                  

                     /*------------------------------------------- bons ----------------------------------------------------------*/

                     /*
                      * pode ser que não tenha o mesmo número de objetos contados quando as opções
                      * são selecionadas na interface pois a alguns objetos da tabela lodata 
                      * podem não estar relacionados com a tabela metrics
                      */

                     // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                     queryAllDataAndMetricsFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) >= " + "'" + tertil + "' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";

                     // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                     queryAllDataFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) >= " + "'" + tertil + "' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";

                     goodLOs = getLOs(queryAllDataFromGoodLOs, queryAllDataAndMetricsFromGoodLOs, 
                                                                                           null, materialType, tertil, 1, 6);

                     // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                     groups.add(0, goodLOs);

                     /*------------------------------------------------------------------------------------------------------*/

                     /*--------------------------------------- não bons -----------------------------------------------------*/

                     // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                     queryAllDataAndMetricsFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) < " + "'" + tertil + "' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) > '0' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"                          
                             + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";

                     // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                     queryAllDataFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `lodata`.`Material_Type` LIKE " + "'" + materialType + "%' \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) < " + "'" + tertil + "' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) > '0' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";

                     notGoodLOs = getLOs(queryAllDataFromNotGoodLOs, queryAllDataAndMetricsFromNotGoodLOs, 
                                                                                           null, materialType, tertil, -1, 6);

                     // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                     groups.add(1, notGoodLOs);

                     /*------------------------------------------------------------------------------------------------------*/
                  }
               }
            }
            /* se apenas o quality measure for diferente de null */
            else
            {
               if(qualityMeasure.equals("Experts"))
               {  
                  /*------------------------------------------- bons ----------------------------------------------------------*/

                  /*
                   * pode ser que não tenha o mesmo número de objetos contados quando as opções
                   * são selecionadas na interface pois a alguns objetos da tabela lodata 
                   * podem não estar relacionados com a tabela metrics
                   */

                  // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                  queryAllDataAndMetricsFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                          + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Reviews` >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Reviews` ASC";

                  // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                  queryAllDataFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                          + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Reviews` >= " + "'" + tertil + "' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Reviews` ASC";

                  goodLOs = getLOs(queryAllDataFromGoodLOs, queryAllDataAndMetricsFromGoodLOs, 
                                                                                           null, null, tertil, 1, 1);

                  // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                  groups.add(0, goodLOs);

                  /*------------------------------------------------------------------------------------------------------*/

                  /*--------------------------------------- não bons -----------------------------------------------------*/

                  // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                  queryAllDataAndMetricsFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                          + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                          + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Reviews` ASC";

                  // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                  queryAllDataFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                          + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                          + "AND `lovaloration`.`Stars_Reviews` < " + "'" + tertil + "' \n"
                          + "AND `lovaloration`.`Stars_Reviews` > '0' \n"
                          + "AND `metrics`.`Page_Error` = '0' \n"
                          + "ORDER BY `Stars_Reviews` ASC";

                  notGoodLOs = getLOs(queryAllDataFromNotGoodLOs, 
                                                                     queryAllDataAndMetricsFromNotGoodLOs, null, null, 
                                                                     tertil, -1, 1);

                  // adiciona o grupo dos objetos não bons com seus respectivos dados e metricas no List que será retornado
                  groups.add(1, notGoodLOs);

                  /*------------------------------------------------------------------------------------------------------*/
               }
               else
               {
                  if(qualityMeasure.equals("Users"))
                  {
                     /*------------------------------------------- bons ----------------------------------------------------------*/

                     /*
                      * pode ser que não tenha o mesmo número de objetos contados quando as opções
                      * são selecionadas na interface pois a alguns objetos da tabela lodata 
                      * podem não estar relacionados com a tabela metrics
                      */

                     // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                     queryAllDataAndMetricsFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Stars_Comments` >= " + "'" + tertil + "' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY `Stars_Comments` ASC";

                     // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                     queryAllDataFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Stars_Comments` >= " + "'" + tertil + "' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY `Stars_Comments` ASC";

                     goodLOs = getLOs(queryAllDataFromGoodLOs, queryAllDataAndMetricsFromGoodLOs, 
                                                                                              null, null, tertil, 1, 5);

                     // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                     groups.add(0, goodLOs);

                     /*------------------------------------------------------------------------------------------------------*/

                     /*--------------------------------------- não bons -----------------------------------------------------*/

                     // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                     queryAllDataAndMetricsFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Stars_Comments` < " + "'" + tertil + "' \n"
                             + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY `Stars_Comments` ASC";

                     // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                     queryAllDataFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Stars_Comments` < " + "'" + tertil + "' \n"
                             + "AND `lovaloration`.`Stars_Comments` > '0' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY `Stars_Comments` ASC";

                     notGoodLOs = getLOs(queryAllDataFromNotGoodLOs, 
                                                                        queryAllDataAndMetricsFromNotGoodLOs, null, null, 
                                                                        tertil, -1, 5);

                     // adiciona o grupo dos objetos não bons com seus respectivos dados e metricas no List que será retornado
                     groups.add(1, notGoodLOs);

                     /*------------------------------------------------------------------------------------------------------*/
                  }
                  else
                  {
                     // o quality measure selecionado foi o de coleções pessoais

                     /*------------------------------------------- bons ----------------------------------------------------------*/
                     
                     /*
                      * pode ser que não tenha o mesmo número de objetos contados quando as opções
                      * são selecionadas na interface pois a alguns objetos da tabela lodata 
                      * podem não estar relacionados com a tabela metrics
                      */
                     
                     // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                     queryAllDataAndMetricsFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) >= " + "'" + tertil + "' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";
                     
                     // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                     queryAllDataFromGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) >= " + "'" + tertil + "' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";

                     goodLOs = getLOs(queryAllDataFromGoodLOs, queryAllDataAndMetricsFromGoodLOs, 
                                                                                           null, materialType, tertil, 1, 6);

                     // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                     groups.add(0, goodLOs);

                     /*------------------------------------------------------------------------------------------------------*/

                     /*--------------------------------------- não bons -----------------------------------------------------*/

                     // Query que seleciona tudo da tabela lodata e tudo da tabela metrics
                     queryAllDataAndMetricsFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.*, `metrics`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) < " + "'" + tertil + "' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) > '0' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"                          
                             + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";

                     // Query que seleciona o ID_LO da tabela lodata e tudo da tabela lodata
                     queryAllDataFromNotGoodLOs = "SELECT DISTINCT(`lodata`.`ID_LO`), `lodata`.* FROM `metrics` \n"
                             + "INNER JOIN `lodata` ON `metrics`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "INNER JOIN `lovaloration` ON `lovaloration`.`lodata_ID_LO` = `lodata`.`ID_LO` \n"
                             + "AND `lovaloration`.`Personal_Collections` != 'none' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) < " + "'" + tertil + "' \n"
                             + "AND CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) > '0' \n"
                             + "AND `metrics`.`Page_Error` = '0' \n"
                             + "ORDER BY CAST(`lovaloration`.`Personal_Collections` AS DECIMAL(20, 2)) ASC";

                     notGoodLOs = getLOs(queryAllDataFromNotGoodLOs, queryAllDataAndMetricsFromNotGoodLOs, 
                                                                                           null, materialType, tertil, -1, 6);

                     // adiciona o grupo dos objetos bons com seus respectivos dados e metricas no List que será retornado
                     groups.add(1, notGoodLOs);

                     /*------------------------------------------------------------------------------------------------------*/
                  }
               }
            }
         }
      }
      return groups;
   }
   
   /**
    * Retorna o grupo dos bons e não bons ordenado pelos valores da métrica selecionada na tabela 
    * de métricas
    * 
    * @since 1.0
    * @param row a linha selecionada da tabela de métricas
    * @param grupos uma lista que contém duas listas de objetos de aprendizagem. Sendo que a primeira
    *               lista contém os objetos bons e a segunda lista os objetos não bons
    * @return double[] - um vetor ordenado pelos valores da métrica selecionada na tabela 
    *                    de métricas
    */
   public double[] getGoodAndNotGoodMetricVector(int row, List<List<LearningObject>> grupos)
   {
      double[] vector = new double[grupos.get(0).size() + grupos.get(1).size()];
      int n = 0;
      
      for(int i = 0; i < grupos.get(0).size() - 1; i++)
      {
         vector[n] = grupos.get(0).get(i).getMetrics(row);
         n++;
      }
      
      for(int i = 0; i < grupos.get(1).size() - 1; i++)
      {
         vector[n] = grupos.get(1).get(i).getMetrics(row);
         n++;
      }
      
      Arrays.sort(vector);
      
      return vector;
   }
   
   /**
    * Calcula se a métrica é um indicador de qualidade
    * 
    * @since 1.0
    * @param pMannWhitney o valor do p calculado pelo método de MannWhitney do R
    * @param pKolmogorov o valor do p calculado pelo método de Kolmogorov do R
    * @return int - um valor de 0 a 2 sendo que 0 significa que não é um indicador de qualidade,
    *               1 é um indicador de qualidade parcial e 2 é um indicador de qualidade
    */
   public int calculateIndicator(double pMannWhitney, double pKolmogorov)
   {
      if(pMannWhitney == -1.0 && pKolmogorov == -1.0)
      {
         return 0;
      }
      else
      {
         if(pMannWhitney <= 0.10 && pKolmogorov <= 0.10)
         {
            if(pMannWhitney < 0.05 && pKolmogorov < 0.05)
            {
               return 2;
            }
            else
            {
               if(pMannWhitney < 0.05 && pKolmogorov < 0.10)
               {
                  return 1;
               }
               else
               {
                  if(pMannWhitney < 0.10 && pKolmogorov < 0.05)
                  {
                     return 1;
                  }
                  else
                  {
                     return 1;
                  }
               }
            }
         }
         
         return 0;
      }
   }
   
   /**
    * Calcula a influência de uma métrica
    * 
    * @since 1.0
    * @param medianGood um double representando a mediana da métrica dos objetos bons
    * @param medianNotGood um double representando a mediana da métrica dos objetos não bons
    * @return boolean - <code>true</code> se a a influência da métrica for positiva ou
    *                   <code>false</code> se a a influência da métrica for negativa
    */
   public boolean calculateInfluence(double medianGood, double medianNotGood)
   {
      return medianGood > medianNotGood;
   }

   /**
    * Cria um arquivo arff que será usado lido pelo weka para executar o algoritmo J48
    * 
    * @since 1.0
    * @param objects uma lista que contém duas listas onde a lista 0 tem objetos bons e a lista 1 objetos não bons
    * @param model o componente do tipo DefaultTableModel onde será usada a coluna Indicator para verificar as métricas
    *              indicadoras de qualidade
    * @return String - uma string contendo o caminho do arquivo arff criado ou a string <code>null</code> indicando 
    *                  que não existe nenhuma métrica que indique qualidade no subgrupo selecionado
    */
   public String createArffFile(List<List<LearningObject>> objects, DefaultTableModel model/*, String category,
                              String materialType, String qualityPerspective*/)
   {
      String archivePath = null;
      /* pega o diretorio de onde o programa está executando */
      String localdirectory = System.getProperty("user.dir");
      File directory = new File(localdirectory + "/arfffiles");
      /* receberá os índices da tabela que contém metricas indicadoras qualidade*/
      List<Integer>indices = new ArrayList<>();
      FileWriter fw;
      BufferedWriter bw;
      
      /* cria um diretório chamado file caso o mesmo não exista */
      if(directory.mkdir())
      {
         System.out.println("Diretório criado com sucesso.");
      }
      else
      {
         System.err.println("Diretório já existe ou não pode ser criado");
      }
      
      File file = new File(localdirectory + "/arfffiles/MerlotRelation.arff");
      
      archivePath = localdirectory + "/arfffiles/MerlotRelation.arff";
      
      try
      {
         FastVector attributes;
         FastVector attributeGoodNotGood;
         Instances data;
         double[] vals;

         attributes = new FastVector();
         attributeGoodNotGood = new FastVector();
         attributeGoodNotGood.addElement("Good");
         attributeGoodNotGood.addElement("Not Good");
         attributes.addElement(new Attribute("Group", attributeGoodNotGood));
         
         /* adiciona um atributo numerico */
         
         for(int i = 0; i < model.getRowCount(); i++)
         {
            if((Boolean)model.getValueAt(i, 3))
            {
               indices.add(i);
               /* testa  se o grupo dos bons tem algum objeto */
               if(objects.get(0).size() > 0)
                  attributes.addElement(new Attribute(objects.get(0).get(0).getNameOfMetric(i)));
               else
                  attributes.addElement(new Attribute(objects.get(1).get(0).getNameOfMetric(i)));
            }
         }
         
         /*  se não existir nenhuma métrica que indique qualidade então retorna null */
         if(indices.isEmpty())
            return null;
         
         /* 
          * Cria um instancia da classe FileWriter para escrever diretamente no arquivo.
          * Este construtor recebe o objeto do tipo arquivo
          */
         fw = new FileWriter(file);
         /* construtor recebe como argumento o objeto do tipo FileWriter */
         bw = new BufferedWriter(fw);

         /* cria um objeto de instâncias */
         data = new Instances("MerlotRelation", attributes, 0);
         
         /* adiciona os objetos bons no arquivo */
         for(int i = 0; i < objects.get(0).size(); i++)
         {
            vals = new double[data.numAttributes()];

            vals[0] = attributeGoodNotGood.indexOf("Good");

            for(int j = 0; j < indices.size(); j++)
               vals[j+1] = objects.get(0).get(i).getMetrics(indices.get(j));

            data.add(new Instance(1.0, vals));
         }

         /* adiciona os objetos não bons no arquivo */
         for(int i = 0; i < objects.get(1).size(); i++)
         {
            vals = new double[data.numAttributes()];

            vals[0] = attributeGoodNotGood.indexOf("Not Good");

            for(int j = 0; j < indices.size(); j++)
               vals[j+1] = objects.get(1).get(i).getMetrics(indices.get(j));

            data.add(new Instance(1.0, vals));
         }

         indices.clear();
         
         bw.write(data.toString());
         bw.flush();
         //fecha os recursos
         bw.close();
         fw.close();
      }
      catch (IOException ex)
      {
         System.out.println(ex);
      }
      
      return archivePath;
   }
   
   /**
    * Lê os dados contidos no arquivo .model
    * 
    * @since 1.0
    * @param path uma string contendo o caminho do arquivo que será lido
    * @return Instances - um objeto da classe Instances com todos os dados lidos do arquivo arff
    */
   public Instances readDataFromArchive(String path)
   {
      DataSource source = null;
      Instances data = null;
      
      try
      {
         source = new DataSource(path);
         data = source.getDataSet();
      }
      catch (Exception ex)
      {
         System.err.println(ex);
      }
      
      return data;
   }
   
   /**
    * Constrói o classificador do algoritmo J48
    * 
    * @since 1.0
    * @param data um objeto da classe Instances com os dados lidos do arquivo arff
    * @return J48 - um objeto com o classificador da classe J48 construído
    */
   public J48 j48BuildClassifier(Instances data, boolean split, int percentageSplit)
   {
      J48 j48 = new J48();
      float confidenceFactor = (float) 0.25;
      
      /* usa o primeiro atributo do arquivo que nesse caso é o grupo do objeto */
      data.setClassIndex(0);
      
      /* o arquivo tem que ter no mínimo dois objetos para ser analisado */
      j48.setMinNumObj(2);
      
      j48.setConfidenceFactor(confidenceFactor);

      try
      {
         if(split)
         {
            Random rand = new Random(1);
            MainWindow.randData = new Instances(data);
            MainWindow.randData.randomize(rand);
            MainWindow.trainSize = (int) Math.round(MainWindow.randData.numInstances() * percentageSplit/100);
            MainWindow.testSize = data.numInstances() - MainWindow.trainSize;
            MainWindow.train = new Instances(MainWindow.randData, 0, MainWindow.trainSize);
            MainWindow.train.setClassIndex(0);
            j48.buildClassifier(MainWindow.train);
         }
         else
         {
            // Constrói classificador
            j48.buildClassifier(data);
         }
      }
      catch (Exception ex)
      {
         System.err.println(ex);
      }

      return j48;
   }
   
   public MultilayerPerceptron multiLayerPerceptronBuildClassifier(Instances data, boolean split, int percentageSplit)
   {
      MultilayerPerceptron perceptron = new MultilayerPerceptron();
      //float confidenceFactor = (float) 0.25;
      
      /* usa o primeiro atributo do arquivo que nesse caso é o grupo do objeto */
      data.setClassIndex(0);

      try
      {
         if(split)
         {
            Random rand = new Random(1);
            MainWindow.randData = new Instances(data);
            MainWindow.randData.randomize(rand);
            MainWindow.trainSize = (int) Math.round(MainWindow.randData.numInstances() * percentageSplit/100);
            MainWindow.testSize = data.numInstances() - MainWindow.trainSize;
            MainWindow.train = new Instances(MainWindow.randData, 0, MainWindow.trainSize);
            MainWindow.train.setClassIndex(0);
            perceptron.buildClassifier(MainWindow.train);
         }
         else
         {
            // Constrói classificador
            perceptron.buildClassifier(data);
         }
      }
      catch (Exception ex)
      {
         System.err.println(ex);
      }

      return perceptron;
   }
   
   public BayesianLogisticRegression bayesNetworkBuildClassifier(Instances data, boolean split, int percentageSplit)
   {
      BayesianLogisticRegression bayes = new BayesianLogisticRegression();
      //float confidenceFactor = (float) 0.25;
      
      /* usa o primeiro atributo do arquivo que nesse caso é o grupo do objeto */
      data.setClassIndex(0);

      try
      {
         if(split)
         {
            Random rand = new Random(1);
            MainWindow.randData = new Instances(data);
            MainWindow.randData.randomize(rand);
            MainWindow.trainSize = (int) Math.round(MainWindow.randData.numInstances() * percentageSplit/100);
            MainWindow.testSize = data.numInstances() - MainWindow.trainSize;
            MainWindow.train = new Instances(MainWindow.randData, 0, MainWindow.trainSize);
            MainWindow.train.setClassIndex(0);
            bayes.buildClassifier(MainWindow.train);
         }
         else
         {
            // Constrói classificador
            bayes.buildClassifier(data);
         }
      }
      catch (Exception ex)
      {
         System.err.println(ex);
      }

      return bayes;
   }
   
   /**
    * Faz a avaliação do algoritmo J48 nos dados lidos do arquivo
    * 
    * @since 1.0
    * @param j48 um objeto da classe J48 com o classificador construído
    * @param data um objeto da classe Instances com os dados lidos do arquivo arff
    * @return Evaluation - a avaliação do algoritmo j48 nos dados lidos do arquivo
    */
   public Evaluation j48Evaluation(J48 j48, Instances data, int evaluationMethod, int numberOfFolds)
   {
      Evaluation evaluation = null;
      
      try
      {  
         if(evaluationMethod == 1)
         {
            evaluation = new Evaluation(data);
            evaluation.evaluateModel(j48, data);
         }
         else
         {
            if(evaluationMethod == 2)
            {
               evaluation = new Evaluation(data);
               evaluation.crossValidateModel(j48, data, numberOfFolds, new Random(1));
            }
            else
            {
               evaluation = new Evaluation(MainWindow.train);
               Instances test = new Instances(MainWindow.randData, MainWindow.trainSize, MainWindow.testSize);
               test.setClassIndex(0);
               evaluation.evaluateModel(j48, test);
            }
         }
      }
      catch (Exception ex)
      {
         System.err.println(ex);
      }
      
      return evaluation;
   }
   
   public Evaluation multiLayerPerceptronEvaluation(MultilayerPerceptron perceptron, Instances data, int evaluationMethod, int numberOfFolds)
   {
      Evaluation evaluation = null;
      
      try
      {
         perceptron.buildClassifier(data);
         
         if(evaluationMethod == 1)
         {
            evaluation = new Evaluation(data);
            evaluation.evaluateModel(perceptron, data);
         }
         else
         {
            if(evaluationMethod == 2)
            {
               evaluation = new Evaluation(data);
               evaluation.crossValidateModel(perceptron, data, numberOfFolds, new Random(0));
            }
            else
            {
               evaluation = new Evaluation(MainWindow.train);
               Instances test = new Instances(MainWindow.randData, MainWindow.trainSize, MainWindow.testSize);
               test.setClassIndex(0);
               evaluation.evaluateModel(perceptron, test);
            }
         }
      }
      catch (Exception ex)
      {
         System.err.println(ex);
      }
      
      return evaluation;
   }
   
   public Evaluation bayesNetworkEvaluation(BayesianLogisticRegression bayes, Instances data, int evaluationMethod, int numberOfFolds)
   {
      Evaluation evaluation = null;
      
      try
      {
         bayes.buildClassifier(data);
         
         if(evaluationMethod == 1)
         {
            evaluation = new Evaluation(data);
            evaluation.evaluateModel(bayes, data);
         }
         else
         {
            if(evaluationMethod == 2)
            {
               evaluation = new Evaluation(data);
               evaluation.crossValidateModel(bayes, data, numberOfFolds, new Random(0));
            }
            else
            {
               evaluation = new Evaluation(MainWindow.train);
               Instances test = new Instances(MainWindow.randData, MainWindow.trainSize, MainWindow.testSize);
               test.setClassIndex(0);
               evaluation.evaluateModel(bayes, test);
            }
         }
      }
      catch (Exception ex)
      {
         System.err.println(ex);
      }
      
      return evaluation;
   }
   
   /**
    * Salva o modelo criado pelo weka
    * 
    * @since 1.0
    * @param evaluation a avaliação feita pelo modelo
    * @param qualityPerspective a perspectiva de qualidade selecionada para a criação do modelo
    * @param category a categoria selecionada para a criação do modelo
    * @param metricsNames o nome das métricas usadas na criação do modelo
    * @param materialType o tipo de material utilizado na criação do modelo
    * @param metricsIndicators os indicadores de qualidade das métricas utilizadas para a criação do modelo
    * @param metricsInfluences a influência das métricas utilizadas na criação do modelo
    * @param j48 uma instancia da classe J48 do weka
    * @param perceptron uma instancia da classe MultilayerPerceptron do weka
    * @param bayes uma instancia da classe BayesianLogisticRegression do weka
    * @param usedMetricsOnModel uma lista contendo o nome das métricas usadas para criar o modelo
    * @param modelType o tipo de modelo a ser salvo que pode ser j48, perceptron ou bayes
    * @return String - o caminho onde o modelo foi salvo
    */
   public String saveModel(J48 j48, MultilayerPerceptron perceptron, BayesianLogisticRegression bayes, Evaluation evaluation, List<String> usedMetricsOnModel, String qualityPerspective, 
                           String category, String materialType, List<String> metricsNames,
                           List<String> metricsIndicators, List<String> metricsInfluences, String modelType)
   {
      /* pega o diretorio de onde o programa está executando */
      String localdirectory = System.getProperty("user.dir");
      File modelPath = null;      
      
      JFileChooser window = new JFileChooser(localdirectory);
      window.setDialogTitle("Save File");
      window.setFileFilter(new FileNameExtensionFilter(".model files", "model"));
      window.setAcceptAllFileFilterUsed(false);
      
      int actionDialog = window.showSaveDialog(null);
      
      if (actionDialog == JFileChooser.APPROVE_OPTION)
      {
         modelPath = window.getSelectedFile( );

         if(modelPath == null)
         {
            return null;
         }

         if(modelPath.exists())
         {
            actionDialog = JOptionPane.showConfirmDialog(null, "Replace existing file?");

            if(actionDialog == JOptionPane.NO_OPTION || actionDialog == JOptionPane.CANCEL_OPTION)
            {
               return null;
            }
         }
         else
         {
            modelPath = new File(window.getSelectedFile( ) + ".model");
         }
      }
      
      if(modelPath == null)
      {
         return null;
      }
      else
      {
         try
         {
            if(modelType.equals("j48"))
            {
               SerializationHelper.write(modelPath.getAbsolutePath(), j48);
            }
            else
            {
               if(modelType.equals("perceptron"))
               {
                  SerializationHelper.write(modelPath.getAbsolutePath(), perceptron);
               }
               else
               {
                  if(modelType.equals("bayes"))
                  {
                     SerializationHelper.write(modelPath.getAbsolutePath(), bayes);
                  }
               }
            }
            
            String txtPath = modelPath.getAbsolutePath().substring(0, modelPath.getAbsolutePath().lastIndexOf("/"));
            String txtTreePath = modelPath.getAbsolutePath().substring(0, modelPath.getAbsolutePath().lastIndexOf("/"));
            String txtEvaluationPath = modelPath.getAbsolutePath().substring(0, modelPath.getAbsolutePath().lastIndexOf("/"));
            String fileName = modelPath.getAbsolutePath().substring(modelPath.getAbsolutePath().lastIndexOf("/"), 
                                                                    modelPath.getAbsolutePath().lastIndexOf("."));
            String fileTreeName = modelPath.getAbsolutePath().substring(modelPath.getAbsolutePath().lastIndexOf("/"), 
                                                                    modelPath.getAbsolutePath().lastIndexOf("."));
            String fileEvaluationName = modelPath.getAbsolutePath().substring(modelPath.getAbsolutePath().lastIndexOf("/"), 
                                                                    modelPath.getAbsolutePath().lastIndexOf("."));
            
            
            File file = new File(txtPath + fileName + ".txt");
            File fileTree = new File(txtTreePath + fileTreeName + "_Tree" + ".txt");
            File fileEvaluation = new File(txtEvaluationPath + fileEvaluationName + "_Evaluation" + ".csv");
            
            if(!file.exists())
               file.createNewFile();
            
            if(!fileTree.exists())
               fileTree.createNewFile();
            
            if(!fileEvaluation.exists())
               fileEvaluation.createNewFile();
            
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            FileWriter fwTree = new FileWriter(fileTree.getAbsoluteFile());
            FileWriter fwEvaluation = new FileWriter(fileEvaluation.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            BufferedWriter bwTree = new BufferedWriter(fwTree);
            BufferedWriter bwEvaluation = new BufferedWriter(fwEvaluation);
            
            for(int i = 0; i < usedMetricsOnModel.size(); i++)
            {
               bw.write(usedMetricsOnModel.get(i));
               bw.newLine();
            }
            
            bw.write("@modelType");
            bw.newLine();
            bw.write(modelType);
            bw.newLine();
            
            bw.write("@selection");
            bw.newLine();
            bw.write(qualityPerspective);
            bw.newLine();
            
            if(category != null)
            {
               bw.write(category);
               bw.newLine();
            }
            else
            {
               bw.write("null");
               bw.newLine();
            }
            
            if(materialType != null)
            {
               bw.write(materialType);
               bw.newLine();
            }
            else
            {
               bw.write("null");
               bw.newLine();
            }
            
            bw.write("@metricsnames");
            bw.newLine();
            
            for(int i = 0; i < metricsNames.size(); i++)
            {
               bw.write(metricsNames.get(i));
               bw.newLine();
            }
            
            bw.write("@metricsindicators");
            bw.newLine();
            
            for(int i = 0; i < metricsIndicators.size(); i++)
            {
               bw.write(metricsIndicators.get(i));
               bw.newLine();
            }
            
            bw.write("@metricsinfluences");
            bw.newLine();
            
            for(int i = 0; i < metricsInfluences.size(); i++)
            {
               bw.write(metricsInfluences.get(i));
               bw.newLine();
            }
            
            bw.write("@modelevaluation");
            bw.newLine();
            
            if(modelType.equals("j48"))
            {
               bw.write(String.valueOf(j48.getConfidenceFactor()));
               bw.newLine();
               bw.write(String.valueOf(evaluation.correct()));
               bw.newLine();
               bw.write(String.valueOf(evaluation.pctCorrect()));
               bw.newLine();
               bw.write(String.valueOf(evaluation.incorrect()));
               bw.newLine();
               bw.write(String.valueOf(evaluation.pctIncorrect()));
               bw.newLine();
               bw.write(String.valueOf(evaluation.kappa()));
               bw.newLine();
               bw.write(String.valueOf(evaluation.meanAbsoluteError()));
               bw.newLine();
               bw.write(String.valueOf(evaluation.fMeasure(0)));
               bw.newLine();
               bw.write(String.valueOf(evaluation.fMeasure(1)));
               bw.newLine();
               bw.write(String.valueOf(evaluation.precision(0)));
               bw.newLine();
               bw.write(String.valueOf(evaluation.precision(1)));
               bw.newLine();
               bw.write(String.valueOf(evaluation.truePositiveRate(0)));
               bw.newLine();
               bw.write(String.valueOf(evaluation.truePositiveRate(1)));
               bw.newLine();
               bw.write(String.valueOf(j48.measureNumRules()));
               bw.newLine();
               bw.write(evaluation.toMatrixString("===== Evaluation Numbers =====\n"));
               bw.newLine();

               bwTree.write(j48.toString());

               bwEvaluation.write("\"Quality Perspective\",");

               if(category != null)
               {
                  bwEvaluation.write("\"Category\",");
               }

               if(materialType != null)
               {
                  bwEvaluation.write("\"Material Type\",");
               }

               bwEvaluation.write("\"Correct Instances\",");
               bwEvaluation.write("\"Correct Instances Percent\",");
               bwEvaluation.write("\"Incorrect Instances\",");
               bwEvaluation.write("\"Incorrect Instances Percent\",");
               bwEvaluation.write("\"Kappa\",");
               bwEvaluation.write("\"Mean Absolute Error\",");
               bwEvaluation.write("\"F Measure Good\",");
               bwEvaluation.write("\"F Measure Not Good\",");
               bwEvaluation.write("\"Precision Good\",");
               bwEvaluation.write("\"Precision Not Good\",");
               bwEvaluation.write("\"TruePositive Good\",");
               bwEvaluation.write("\"TruePositive Not Good\",");
               bwEvaluation.newLine();
               bwEvaluation.write("\"" + qualityPerspective + "\"" +  ",");

               if(category != null)
               {
                  int closeParenthesisPosition = category.indexOf(")");
                  bwEvaluation.write("\"" + category.substring(closeParenthesisPosition + 2) + "\"" + ",");
               }

               if(materialType != null)
               {
                  int closeParenthesisPosition = materialType.indexOf(")");
                  bwEvaluation.write("\"" + materialType.substring(closeParenthesisPosition + 2) + "\"" + ",");
               }

               bwEvaluation.write(String.valueOf(evaluation.correct()) + ",");
               bwEvaluation.write(String.valueOf(evaluation.pctCorrect()) + ",");
               bwEvaluation.write(String.valueOf(evaluation.incorrect()) + ",");
               bwEvaluation.write(String.valueOf(evaluation.pctIncorrect()) + ",");
               bwEvaluation.write(String.valueOf(evaluation.kappa()) + ",");
               bwEvaluation.write(String.valueOf(evaluation.meanAbsoluteError()) + ",");
               bwEvaluation.write(String.valueOf(evaluation.fMeasure(0)) + ",");
               bwEvaluation.write(String.valueOf(evaluation.fMeasure(1)) + ",");
               bwEvaluation.write(String.valueOf(evaluation.precision(0)) + ",");
               bwEvaluation.write(String.valueOf(evaluation.precision(1)) + ",");
               bwEvaluation.write(String.valueOf(evaluation.truePositiveRate(0)) + ",");
               bwEvaluation.write(String.valueOf(evaluation.truePositiveRate(1)));

               bwTree.flush();
               bwTree.close();
               bwEvaluation.flush();
               bwEvaluation.close();
               bw.flush();
               bw.close();
            }
            else
            {
               if(modelType.equals("perceptron"))
               {
                  bw.write(String.valueOf(evaluation.correct()));
                  bw.newLine();
                  bw.write(String.valueOf(evaluation.pctCorrect()));
                  bw.newLine();
                  bw.write(String.valueOf(evaluation.incorrect()));
                  bw.newLine();
                  bw.write(String.valueOf(evaluation.pctIncorrect()));
                  bw.newLine();
                  bw.write(String.valueOf(evaluation.kappa()));
                  bw.newLine();
                  bw.write(String.valueOf(evaluation.meanAbsoluteError()));
                  bw.newLine();
                  bw.write(String.valueOf(evaluation.rootMeanSquaredError()));
                  bw.newLine();
                  bw.write(String.valueOf(evaluation.relativeAbsoluteError()));
                  bw.newLine();
                  bw.write(String.valueOf(evaluation.rootRelativeSquaredError()));
                  bw.newLine();
                  bw.write(evaluation.toMatrixString("===== Evaluation Numbers =====\n"));
                  bw.newLine();

                  bwTree.write(perceptron.toString());
                  
                  bwEvaluation.write("\"Quality Perspective\",");

                  if(category != null)
                  {
                     bwEvaluation.write("\"Category\",");
                  }

                  if(materialType != null)
                  {
                     bwEvaluation.write("\"Material Type\",");
                  }

                  bwEvaluation.write("\"Correct Instances\",");
                  bwEvaluation.write("\"Correct Instances Percent\",");
                  bwEvaluation.write("\"Incorrect Instances\",");
                  bwEvaluation.write("\"Incorrect Instances Percent\",");
                  bwEvaluation.write("\"Kappa\",");
                  bwEvaluation.write("\"Mean Absolute Error\",");
                  bwEvaluation.write("\"Root Mean Squared Error\",");
                  bwEvaluation.write("\"Relative Absolute Error\",");
                  bwEvaluation.write("\"Root Relative Squared Error\",");
                  bwEvaluation.newLine();
                  bwEvaluation.write("\"" + qualityPerspective + "\"" +  ",");
                  
                  if(category != null)
                  {
                     int closeParenthesisPosition = category.indexOf(")");
                     bwEvaluation.write("\"" + category.substring(closeParenthesisPosition + 2) + "\"" + ",");
                  }

                  if(materialType != null)
                  {
                     int closeParenthesisPosition = materialType.indexOf(")");
                     bwEvaluation.write("\"" + materialType.substring(closeParenthesisPosition + 2) + "\"" + ",");
                  }

                  bwEvaluation.write(String.valueOf(evaluation.correct()) + ",");
                  bwEvaluation.write(String.valueOf(evaluation.pctCorrect()) + ",");
                  bwEvaluation.write(String.valueOf(evaluation.incorrect()) + ",");
                  bwEvaluation.write(String.valueOf(evaluation.pctIncorrect()) + ",");
                  bwEvaluation.write(String.valueOf(evaluation.kappa()) + ",");
                  bwEvaluation.write(String.valueOf(evaluation.meanAbsoluteError()) + ",");
                  bwEvaluation.write(String.valueOf(evaluation.rootMeanSquaredError()) + ",");
                  bwEvaluation.write(String.valueOf(evaluation.relativeAbsoluteError()) + ",");
                  bwEvaluation.write(String.valueOf(evaluation.rootRelativeSquaredError()) + ",");
                  
                  bwTree.flush();
                  bwTree.close();
                  bwEvaluation.flush();
                  bwEvaluation.close();
                  bw.flush();
                  bw.close();
               }
               else
               {
                  if(modelType.equals("bayes"))
                  {
                     bw.write(String.valueOf(evaluation.correct()));
                     bw.newLine();
                     bw.write(String.valueOf(evaluation.pctCorrect()));
                     bw.newLine();
                     bw.write(String.valueOf(evaluation.incorrect()));
                     bw.newLine();
                     bw.write(String.valueOf(evaluation.pctIncorrect()));
                     bw.newLine();
                     bw.write(String.valueOf(evaluation.kappa()));
                     bw.newLine();
                     bw.write(String.valueOf(evaluation.meanAbsoluteError()));
                     bw.newLine();
                     bw.write(String.valueOf(evaluation.rootMeanSquaredError()));
                     bw.newLine();
                     bw.write(String.valueOf(evaluation.relativeAbsoluteError()));
                     bw.newLine();
                     bw.write(String.valueOf(evaluation.rootRelativeSquaredError()));
                     bw.newLine();
                     bw.write(evaluation.toMatrixString("===== Evaluation Numbers =====\n"));
                     bw.newLine();

                     bwTree.write(bayes.toString());

                     bwEvaluation.write("\"Quality Perspective\",");

                     if(category != null)
                     {
                        bwEvaluation.write("\"Category\",");
                     }

                     if(materialType != null)
                     {
                        bwEvaluation.write("\"Material Type\",");
                     }

                     bwEvaluation.write("\"Correct Instances\",");
                     bwEvaluation.write("\"Correct Instances Percent\",");
                     bwEvaluation.write("\"Incorrect Instances\",");
                     bwEvaluation.write("\"Incorrect Instances Percent\",");
                     bwEvaluation.write("\"Kappa\",");
                     bwEvaluation.write("\"Mean Absolute Error\",");
                     bwEvaluation.write("\"Root Mean Squared Error\",");
                     bwEvaluation.write("\"Relative Absolute Error\",");
                     bwEvaluation.write("\"Root Relative Squared Error\",");
                     bwEvaluation.newLine();
                     bwEvaluation.write("\"" + qualityPerspective + "\"" +  ",");

                     if(category != null)
                     {
                        int closeParenthesisPosition = category.indexOf(")");
                        bwEvaluation.write("\"" + category.substring(closeParenthesisPosition + 2) + "\"" + ",");
                     }

                     if(materialType != null)
                     {
                        int closeParenthesisPosition = materialType.indexOf(")");
                        bwEvaluation.write("\"" + materialType.substring(closeParenthesisPosition + 2) + "\"" + ",");
                     }

                     bwEvaluation.write(String.valueOf(evaluation.correct()) + ",");
                     bwEvaluation.write(String.valueOf(evaluation.pctCorrect()) + ",");
                     bwEvaluation.write(String.valueOf(evaluation.incorrect()) + ",");
                     bwEvaluation.write(String.valueOf(evaluation.pctIncorrect()) + ",");
                     bwEvaluation.write(String.valueOf(evaluation.kappa()) + ",");
                     bwEvaluation.write(String.valueOf(evaluation.meanAbsoluteError()) + ",");
                     bwEvaluation.write(String.valueOf(evaluation.rootMeanSquaredError()) + ",");
                     bwEvaluation.write(String.valueOf(evaluation.relativeAbsoluteError()) + ",");
                     bwEvaluation.write(String.valueOf(evaluation.rootRelativeSquaredError()) + ",");

                     bwTree.flush();
                     bwTree.close();
                     bwEvaluation.flush();
                     bwEvaluation.close();
                     bw.flush();
                     bw.close();
                  }
               }
            }
            
            return modelPath.getAbsolutePath();
         }
         catch (Exception ex)
         {
            System.err.println(ex);
            return null;
         }
      }
   }
   
   /**
    * Retorna o caminho do modelo escolhido pelo usuário
    * 
    * @since 1.0
    * @return File - o caminho absoluto do arquivo
    */
   public File getModelPath()
   {  
      String localdirectory = System.getProperty("user.dir");
      File modelPath = null;      
      
      JFileChooser window = new JFileChooser(localdirectory);
      window.setDialogTitle("Open File");
      window.setFileFilter(new FileNameExtensionFilter(".model files", "model"));
      window.setAcceptAllFileFilterUsed(false);
      
      int actionDialog = window.showOpenDialog(null);
      
      if (actionDialog == JFileChooser.APPROVE_OPTION)
      {
         modelPath = window.getSelectedFile( );
          
         if(modelPath == null)
         {
            return null;
         }
         else
         {
            return modelPath.getAbsoluteFile();
         }
      }
      else
      {
         return null;
      }
   }
   
   /**
    * Faz a leitura do modelo escolhido pelo usuário
    * 
    * @since 1.0
    * @param modelPath o caminho do modelo
    * @return J48 - uma instância da classe J48 do weka
    */
   public J48 readJ48Model(File modelPath)
   {
      J48 j48 = null;
      
      try
      {
         j48 = (J48) weka.core.SerializationHelper.read(modelPath.getAbsolutePath());
         return j48;
      }
      catch (Exception ex)
      {
         System.err.println(ex);
         return null;
      }
   }
   
   /**
    * Faz a leitura do modelo escolhido pelo usuário
    * 
    * @since 1.0
    * @param modelPath o caminho do modelo
    * @return J48 - uma instância da classe J48 do weka
    */
   public MultilayerPerceptron readPerceptronModel(File modelPath)
   {
      MultilayerPerceptron perceptron = null;
      
      try
      {
         perceptron = (MultilayerPerceptron) weka.core.SerializationHelper.read(modelPath.getAbsolutePath());
         return perceptron;
      }
      catch (Exception ex)
      {
         System.err.println(ex);
         return null;
      }
   }
   
   /**
    * Faz a leitura do modelo escolhido pelo usuário
    * 
    * @since 1.0
    * @param modelPath o caminho do modelo
    * @return J48 - uma instância da classe J48 do weka
    */
   public BayesianLogisticRegression readBayesModel(File modelPath)
   {
      BayesianLogisticRegression bayes = null;
      
      try
      {
         bayes = (BayesianLogisticRegression) weka.core.SerializationHelper.read(modelPath.getAbsolutePath());
         return bayes;
      }
      catch (Exception ex)
      {
         System.err.println(ex);
         return null;
      }
   }
   
   public String readModelType(File modelPath)
   {
      String modelType = null;
      boolean modelTypeReaded = false;
      
      String txtPath = modelPath.getAbsolutePath().substring(0, modelPath.getAbsolutePath().lastIndexOf("/"));
      String fileName = modelPath.getAbsolutePath().substring(modelPath.getAbsolutePath().lastIndexOf("/"), 
                                                              modelPath.getAbsolutePath().lastIndexOf("."));
      File file = new File(txtPath + fileName + ".txt");
      
      try
      {
         FileReader fr = new FileReader(file);
         BufferedReader br = new BufferedReader(fr);
         
         try
         {
            while(br.ready())
            {
               String readed = br.readLine();
               
               if(readed.equals("@selection"))
               {
                  modelTypeReaded = false;
                  fr.close();
                  br.close();
               }
               
               if(readed.equals("@modelType"))
               {
                  modelTypeReaded = true;
               }
               
               if(modelTypeReaded && !readed.equals("@modelType"))
               {
                  modelType = readed;
               }
            }
         }
         catch (IOException ex)
         {
            System.err.println(ex);
         }
      }
      catch (FileNotFoundException ex)
      {
         System.err.println(ex);
      }
      
      return modelType;
   }
   
   /**
    * Faz a leitura dos atributos do modelo escolhido pelo usuário
    * 
    * @since 1.0
    * @param modelPath o caminho do modelo
    * @return List<String> - uma lista contendo o nome dos atributos usados pelo modelo
    */
   public List<String> readModelAttributes(File modelPath)
   {
      List<String> modelAttributes = new ArrayList<>();
      
      String txtPath = modelPath.getAbsolutePath().substring(0, modelPath.getAbsolutePath().lastIndexOf("/"));
      String fileName = modelPath.getAbsolutePath().substring(modelPath.getAbsolutePath().lastIndexOf("/"), 
                                                              modelPath.getAbsolutePath().lastIndexOf("."));
      File file = new File(txtPath + fileName + ".txt");
      
      try
      {
         FileReader fr = new FileReader(file);
         BufferedReader br = new BufferedReader(fr);
         
         try
         {
            while(br.ready())
            {
               String readed = br.readLine();
               
               if(!readed.equals("@modelType"))
               {
                  modelAttributes.add(readed);
               }
               else
               {
                  fr.close();
                  br.close();
               }
            }
         }
         catch (IOException ex)
         {
            System.err.println(ex);
         }
      }
      catch (FileNotFoundException ex)
      {
         System.err.println(ex);
      }
      
      return modelAttributes;
   }
   
   /**
    * Lê a seleção que foi feita pelo usuário em uma avaliação na qual o modelo está salvo
    * 
    * @since 1.0
    * @param modelPath o caminho do arquivo que contém a seleção feita pelo usuário
    * @return List<String> - uma lista contendo a seleção feita pelo usuário
    */
   public List<String> readUserSelection(File modelPath)
   {
      List<String> userSelection = new ArrayList<>();
      
      String txtPath = modelPath.getAbsolutePath().substring(0, modelPath.getAbsolutePath().lastIndexOf("/"));
      String fileName = modelPath.getAbsolutePath().substring(modelPath.getAbsolutePath().lastIndexOf("/"), 
                                                              modelPath.getAbsolutePath().lastIndexOf("."));
      File file = new File(txtPath + fileName + ".txt");
      boolean selection = false;
      
      try
      {
         FileReader fr = new FileReader(file);
         BufferedReader br = new BufferedReader(fr);
         
         try
         {
            while(br.ready())
            {
               String readed = br.readLine();
               
               if(readed.equals("@metricsnames"))
               {
                  selection = false;
                  fr.close();
                  br.close();
               }
               
               if(readed.equals("@selection"))
               {
                  selection = true;
               }
               
               if(selection && !readed.equals("@selection"))
               {
                  userSelection.add(readed);
               }
            }
         }
         catch (IOException ex)
         {
            System.err.println(ex);
         }
      }
      catch (FileNotFoundException ex)
      {
         System.err.println(ex);
      }
      
      return userSelection;
   }
   
   /**
    * Lê o nome das métricas usadas anteriormente por um modelo em uma avaliação e que estão em um arquivo
    * 
    * @since 1.0
    * @param modelPath o caminho do arquivo que contém o nome das métricas
    * @return List<String> - uma lista contendo o nome das métricas
    */
   public List<String> readMetricsNames(File modelPath)
   {
      List<String> metricsNames = new ArrayList<>();
      
      String txtPath = modelPath.getAbsolutePath().substring(0, modelPath.getAbsolutePath().lastIndexOf("/"));
      String fileName = modelPath.getAbsolutePath().substring(modelPath.getAbsolutePath().lastIndexOf("/"), 
                                                              modelPath.getAbsolutePath().lastIndexOf("."));
      File file = new File(txtPath + fileName + ".txt");
      boolean selection = false;
      
      try
      {
         FileReader fr = new FileReader(file);
         BufferedReader br = new BufferedReader(fr);
         
         try
         {
            while(br.ready())
            {
               String readed = br.readLine();
               
               if(readed.equals("@metricsindicators"))
               {
                  selection = false;
                  fr.close();
                  br.close();
               }
               
               if(readed.equals("@metricsnames"))
               {
                  selection = true;
               }
               
               if(selection && !readed.equals("@metricsnames"))
               {
                  metricsNames.add(readed);
               }
            }
         }
         catch (IOException ex)
         {
            System.err.println(ex);
         }
      }
      catch (FileNotFoundException ex)
      {
         System.err.println(ex);
      }
      
      return metricsNames;
   }
   
   /**
    * Lê o resultado de uma avaliação, feita pelo j48, que está dentro de um arquivo
    * 
    * @since 1.0
    * @param modelPath o caminho do arquivo que contém o resultado das avaliações
    * @return List<String> - uma lista contendo o resultado das avaliações.
    */
   public List<String> readEvaluationResults(File modelPath)
   {
      List<String> evaluationResults = new ArrayList<>();
      
      String txtPath = modelPath.getAbsolutePath().substring(0, modelPath.getAbsolutePath().lastIndexOf("/"));
      String fileName = modelPath.getAbsolutePath().substring(modelPath.getAbsolutePath().lastIndexOf("/"), 
                                                              modelPath.getAbsolutePath().lastIndexOf("."));
      File file = new File(txtPath + fileName + ".txt");
      boolean selection = false;
      
      try
      {
         FileReader fr = new FileReader(file);
         BufferedReader br = new BufferedReader(fr);
         
         try
         {
            while(br.ready())
            {
               String readed = br.readLine();
               
               if(readed.equals("@modelevaluation"))
               {
                  selection = true;
               }
               
               if(selection && !readed.equals("@modelevaluation"))
               {
                  evaluationResults.add(readed);
               }
            }
         }
         catch (IOException ex)
         {
            System.err.println(ex);
         }
      }
      catch (FileNotFoundException ex)
      {
         System.err.println(ex);
      }
      
      return evaluationResults;
   }
   
   /**
    * Lê as métricas, usadas anteriormente por um modelo em uma avaliação, que contribuem para a qualidade
    * dos OAs e que estão em um arquivo
    * 
    * @since 1.0
    * @param modelPath o caminho do arquivo que contém as métricas que contribuem para a qualidade dos OAs
    * @return int[] - um vetor de int indicando se a métrica não tem influência, tem influência parcial ou tem influência
    */
   public int[] readMetricsIndicators(File modelPath)
   {
      List<Integer> metricsIndicators = new ArrayList<>();
      
      String txtPath = modelPath.getAbsolutePath().substring(0, modelPath.getAbsolutePath().lastIndexOf("/"));
      String fileName = modelPath.getAbsolutePath().substring(modelPath.getAbsolutePath().lastIndexOf("/"), 
                                                              modelPath.getAbsolutePath().lastIndexOf("."));
      File file = new File(txtPath + fileName + ".txt");
      boolean selection = false;
      
      try
      {
         FileReader fr = new FileReader(file);
         BufferedReader br = new BufferedReader(fr);
         
         try
         {
            while(br.ready())
            {
               String readed = br.readLine();
               
               if(readed.equals("@metricsinfluences"))
               {
                  selection = false;
                  fr.close();
                  br.close();
               }
               
               if(readed.equals("@metricsindicators"))
               {
                  selection = true;
               }
               
               if(selection && !readed.equals("@metricsindicators"))
               {
                  if(readed.equals("N"))
                  {
                     metricsIndicators.add(0);
                  }
                  
                  if(readed.equals("(Y)"))
                  {
                     metricsIndicators.add(1);
                  }
                  
                  if(readed.equals("Y"))
                  {
                     metricsIndicators.add(2);
                  }
               }
            }
         }
         catch (IOException ex)
         {
            System.err.println(ex);
         }
      }
      catch (FileNotFoundException ex)
      {
         System.err.println(ex);
      }
      
      int[] indicators = new int[metricsIndicators.size()];
      
      for(int i = 0; i < metricsIndicators.size(); i++)
      {
         indicators[i] = metricsIndicators.get(i);
      }
      
      return indicators;
   }
   
   /**
    * Lê a influência das métricas usadas anteriormente por um modelo em uma avaliação e que estão em um arquivo
    * 
    * @since 1.0
    * @param modelPath o caminho do arquivo que contém a influência das métricas
    * @return boolean[] - um vetor de boolean onde true significa influência positiva e false influência negativa
    */
   public boolean[] readMetricsInFluence(File modelPath)
   {
      List<Boolean> metricsInfluence = new ArrayList<>();
      
      String txtPath = modelPath.getAbsolutePath().substring(0, modelPath.getAbsolutePath().lastIndexOf("/"));
      String fileName = modelPath.getAbsolutePath().substring(modelPath.getAbsolutePath().lastIndexOf("/"), 
                                                              modelPath.getAbsolutePath().lastIndexOf("."));
      File file = new File(txtPath + fileName + ".txt");
      boolean selection = false;
      
      try
      {
         FileReader fr = new FileReader(file);
         BufferedReader br = new BufferedReader(fr);
         
         try
         {
            while(br.ready())
            {
               String readed = br.readLine();
               
               if(readed.equals("@modelevaluation"))
               {
                  selection = false;
                  fr.close();
                  br.close();
               }
               
               if(readed.equals("@metricsinfluences"))
               {
                  selection = true;
               }
               
               if(selection && !readed.equals("@metricsinfluences"))
               {
                  if(readed.equals("false"))
                  {
                     metricsInfluence.add(false);
                  }
                  
                  if(readed.equals("-"))
                  {
                     metricsInfluence.add(false);
                  }
                  
                  if(readed.equals("+"))
                  {
                     metricsInfluence.add(true);
                  }
               }
            }
         }
         catch (IOException ex)
         {
            System.err.println(ex);
         }
      }
      catch (FileNotFoundException ex)
      {
         System.err.println(ex);
      }
      
      boolean[] influence = new boolean[metricsInfluence.size()];
      
      for(int i = 0; i < metricsInfluence.size(); i++)
      {
         influence[i] = metricsInfluence.get(i);
      }
      
      return influence;
   }
   
   /**
    * Testa se a URL informada pelo usuário existe
    * 
    * @since 1.0
    * @param URLName o nome da URL
    * @return boolean - true se a URL existe, false caso o contrário
    */
   public boolean checkURLExists(String URLName)
   {
      boolean exists = false;
      URLConnection urlConnection;
      URL url;
      HttpURLConnection httpURLConnection = null;
      
      if(URLName.equals(""))
      {
         return exists;
      }
      else
      {   
         try
         {
            url = new URL(URLName);
            urlConnection = url.openConnection();

            HttpURLConnection.setFollowRedirects(false);
            httpURLConnection = (HttpURLConnection) urlConnection;
            httpURLConnection.setRequestMethod("HEAD");

            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                exists = true;
            }
            else
            {
                exists = false;
            }
         } 
         catch(UnknownHostException e)
         {
            return false;
         }
         catch (IOException e)
         {
            return false;
         }

         httpURLConnection.disconnect();
      
         return exists;
      }
   }
   
   /**
    * Classifica se um objeto é bom ou não bom
    * 
    * @since 1.0
    * @param j48 uma instância da classe J48 do weka que foi lida de um modelo salvo
    * @param perceptron uma instância da classe MultilayerPerceptron do weka que foi lida de um modelo salvo
    * @param bayes uma instância da classe BayesianLogisticRegression do weka que foi lida de um modelo salvo
    * @param nameAttributes o nome dos atributos usados nesse modelo
    * @param attributesValues o valor dos atributos que o crawler obteve da URL informada caso ela ainda
    *                         não conste no banco de dados
    * @param URL a URL informada pelo usuário caso ela já conste no banco de dados
    * @param URLAnalized true se a URL já consta no banco de dados, false caso contrário
    * @return String - uma string contendo "Good" ou "Not Good"
    */
   public String classifyData(J48 j48, MultilayerPerceptron perceptron, BayesianLogisticRegression bayes, List<String> nameAttributes, List<Double> attributesValues, 
                            String URL, boolean URLAnalized)
   {
      Evaluation evaluation;
      Range attsToOutput;
      StringBuffer forPredictionsPrinting;
      String archivePath = null;
      /* pega o diretorio de onde o programa está executando */
      String localdirectory = System.getProperty("user.dir");
      File directory = new File(localdirectory + "/arfffiles");
      FileWriter fw = null;
      BufferedWriter bw;
      FastVector attributes;
      FastVector attributeGoodNotGood;
      Instances data;
      double[] vals;

      /* cria um diretório chamado file caso o mesmo não exista */
      if(directory.mkdir())
      {
         System.out.println("Diretório criado com sucesso.");
      }
      else
      {
         System.err.println("Diretório já existe ou não pode ser criado");
      }

      File file = new File(localdirectory + "/arfffiles/MerlotRelation.arff");

      archivePath = localdirectory + "/arfffiles/MerlotRelation.arff";

      attributes = new FastVector();
      attributeGoodNotGood = new FastVector();

      attributeGoodNotGood.addElement("Good");
      attributeGoodNotGood.addElement("Not Good");
      attributes.addElement(new Attribute("Group", attributeGoodNotGood));

      /* adiciona um atributo numerico */
      for(int i = 0; i < nameAttributes.size(); i++)
      {   
         attributes.addElement(new Attribute(nameAttributes.get(i)));
      }

      vals = new double[nameAttributes.size() + 1];

      /* põe um ponto de interrogação no primeiro atributo que deveria ser Good ou Not Good pois é o que vai ser avaliado */
      vals[0] = Instance.missingValue();

      if(!URLAnalized)
      {
      
         /* faz um for percorrendo o valor dos atributos coletados pelo crawler e adicionando os mesmos como valores dos atributos 
          * no arquivo que será usado para avaliação pelo weka
          */
         for(int j = 0; j < attributesValues.size(); j++)
         {
            vals[j+1] = attributesValues.get(j);
         }
      }
      else
      {
         /* 
          * pega o valor dos atributos, que o modelo usa, do banco de dados 
          * e faz a analise se o objeto é bom ou não bom
          */
         String query;
         DataBaseCrawler dbCrawler = new DataBaseCrawler();
         ResultSet resultSet = null;
         
         query = "SELECT ";
         
         for(int i = 0; i < nameAttributes.size(); i++)
         {
            if(i == nameAttributes.size() - 1)
            {
               query = query + "`" + nameAttributes.get(i) + "` ";
            }
            else
            {
               query = query + "`" + nameAttributes.get(i) + "`, ";
            }
         }
         
         query = query + "FROM `metrics` \n" 
                 + "INNER JOIN `lodata` ON `lodata`.`ID_LO` = `metrics`.`lodata_ID_LO` \n"
                 + "AND `lodata`.`Location` = " + "'" + URL + "'";
         
         resultSet = dbCrawler.consultaBancoDeDados(MainWindow.conexionCrawler, query);
         
         try
         {
            while(resultSet.next())
            {
               for(int i = 1; i <= nameAttributes.size(); i++)
               {
                  vals[i] = resultSet.getDouble(i);
               }
            }
         }
         catch (SQLException ex)
         {
            System.err.println(ex);
         }
      }
      
      try
      {
         /* 
          * Cria um instancia da classe FileWriter para escrever diretamente no arquivo.
          * Este construtor recebe o objeto do tipo arquivo
          */
         fw = new FileWriter(file);

         /* construtor recebe como argumento o objeto do tipo FileWriter */
         bw = new BufferedWriter(fw);

         /* cria um objeto de instâncias */
         data = new Instances("MerlotRelation", attributes, 0);

         data.add(new Instance(1.0, vals));

         bw.write(data.toString());
         bw.flush();
         fw.flush();
         //fecha os recursos
         bw.close();
         fw.close();
      }
      catch (IOException ex)
      {
         System.err.println(ex);
      }
      
      try
      {
         Instances unlabeled = new Instances(new BufferedReader(new FileReader(System.getProperty("user.dir") + "/arfffiles/MerlotRelation.arff")));
         
         unlabeled.setClassIndex(0);
         
         forPredictionsPrinting = new StringBuffer();
         attsToOutput = null;
         
         try
         {
            evaluation = new Evaluation(unlabeled);
            
            if(perceptron == null && j48 == null)
            {
               evaluation.evaluateModel(bayes, unlabeled, forPredictionsPrinting, attsToOutput, false);
            }
            else
            {
               if(j48 == null && bayes == null)
               {
                  evaluation.evaluateModel(perceptron, unlabeled, forPredictionsPrinting, attsToOutput, false);
               }
               else
               {
                  if(perceptron == null && bayes == null)
                  {
                     evaluation.evaluateModel(j48, unlabeled, forPredictionsPrinting, attsToOutput, false);
                  }
               }
            }
            
            String intermed = forPredictionsPrinting.substring(0, forPredictionsPrinting.length());
            Pattern regexGood = Pattern.compile("Good");
            Pattern regexNotGood = Pattern.compile("Not Good");
            Matcher matchGood = regexGood.matcher(intermed);
            Matcher matchNotGood = regexNotGood.matcher(intermed);
            
            if(matchNotGood.find())
            {
                System.out.println(matchNotGood.group());
                return matchNotGood.group();
            }
            else
            {
               if(matchGood.find())
               {
                   System.out.println(matchGood.group());
                   return matchGood.group();
               }
            }
            
            System.out.println("\n");
         }
         catch (Exception ex)
         {
            System.err.println(ex);
            return null;
         }
      }
      catch (IOException ex)
      {
         System.err.println(ex);
         return null;
      }
      
      return null;
   }
}
