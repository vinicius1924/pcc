/*
=============
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package crawler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import javax.swing.JOptionPane;
import net.htmlparser.jericho.Config;
import net.htmlparser.jericho.LoggerProvider;
import net.htmlparser.jericho.Source;

/**
 *
 * @author Victor Apellaniz
 */
public class Crawler
{
   public Crawler()
   {

   }

   /**
    * Navega pela URL informada pelo usuário para obeter as métricas e inserí-las no banco de dados
    * 
    * @since 1.0
    * @param connection a conexão com o banco de dados
    * @param modelAttributes os atributos usados no modelo que foi carregado
    * @param URL URL que deve ser explorada
    * @return List<Double> - uma lista das metricas coletadas pelo crawler da URL informada e que são usadas no modelo carregado
    */
   public List<Double> crawlMetrics(String URL, Connection connection, List<String> modelAttributes, boolean reevaluate)
   {
      DataBaseCrawler dataBaseCrawler = new DataBaseCrawler();
      List<Double> attributesValues = new ArrayList<>();
      Date fecha_hora_inicio;
      //int top;
      MetricsParser parser;
      int idLo = 0;
      String location;
      TreeSet treeSite;
      TreeSet pagesTree = null;
      TreeSet locationUniqueLinks = null;
      List locationLinks = null;
      TreeSet imagesTree = null;
      Source source;
      Date fecha_hora_final;
      //boolean URLExist;
      ObjectData object = null;
      //Connection connection;
      ObjectMetrics objectMetrics = null;
      int nLinks = 0;
      int nUniqueLinks = 0;
      int nInternalLinks = 0;
      int nUniqueInternalLinks = 0;
      int nExternalLinks = 0;
      int nUniqueExternalLinks = 0;
      int nImgs = 0;
      int htmlSize = 0;
      int imagesSize = 0;
      int nScripts = 0;
      int nApplets = 0;
      int displayWordsCount = 0;
      int linkWordsCount = 0;
      int numberOfPages = 0;
      int downloadFiles = 0;
      int audioFiles = 0;
      int videoFiles = 0;
      int multimediaFiles = 0;
      float averageNumberOfUniqueInternalLinks = 0;
      float averageNumberOfInternalLinks = 0;
      float averageNumberOfUniqueExternalLinks = 0;
      float averageNumberOfExternalLinks = 0;
      float averageNumberOfUniqueLinks = 0;
      float averageNumberOfLinks = 0;
      float averageWords = 0;
      float averageDownloadFiles = 0;
      float averageAudioFiles = 0;
      float averageVideoFiles = 0;
      float averageMultimediaFiles = 0;
      float averageApplets = 0;
      float averageImages = 0;
      float averageHtmlBytes = 0;
      float averageImagesSize = 0;
      float averageScripts = 0;

      Config.LoggerProvider = LoggerProvider.DISABLED;
      fecha_hora_inicio = new Date(); //Show time
      System.out.println("Time : " + fecha_hora_inicio); //Show time

      parser = new MetricsParser();
      
      if(reevaluate)
      {
         idLo = dataBaseCrawler.getLOID(connection, URL);
      }
      else
      {
         idLo = dataBaseCrawler.getMaxLOID(connection) + 1;
      }
      
      location = URL;
         
      try
      {
         treeSite = parser.getTreeSite(location);
         pagesTree = parser.getLocationPages(location, treeSite);
         locationUniqueLinks = parser.getUniqueLinksTree(pagesTree);
         locationLinks = parser.getLinksTree(pagesTree);
         imagesTree = parser.getImagesTree(pagesTree);
      }
      catch (SQLException ex)
      {
         System.err.println(ex);
      }
         
      source = parser.getSource(location);
         
      if (!location.endsWith(".mhtml") && (source != null))
      {
         try
         {
            System.out.println("\nLocation: " + location);
            nLinks = locationLinks.size();
            System.out.println("nLinks ok = " + nLinks);
            nInternalLinks = parser.getIntenalLinksNumber(location, locationLinks);
            System.out.println("nInternalLinks ok = " + nInternalLinks);
            nUniqueLinks = locationUniqueLinks.size();
            System.out.println("nUniqueLinks ok = " + nUniqueLinks);
            nUniqueInternalLinks = parser.getUniqueIntenalLinksNumber(location, locationUniqueLinks);
            System.out.println("nUniqueInternalLinks ok = " + nUniqueInternalLinks);
            nExternalLinks = parser.getExternalLinksNumber(location, locationLinks);
            System.out.println("nExternalLinks ok");
            nUniqueExternalLinks = parser.getUniqueExternalLinksNumber(location, locationUniqueLinks);
            System.out.println("nExternalUniqueLinks ok");
            nImgs = parser.getTreeSize(imagesTree);
            System.out.println("nImgs ok");
            htmlSize = parser.getIHtmlBytes(pagesTree);
            System.out.println("htmlSize ok");
            imagesSize = parser.getImagesSizeN(location, imagesTree);
            System.out.println("imagesSize ok");
            nScripts = parser.getScriptNumber(pagesTree);
            System.out.println("nScripts ok");
            nApplets = parser.getAppletNumber(pagesTree);
            System.out.println("nApplets ok");
            displayWordsCount = parser.getDisplayedWords(pagesTree);
            System.out.println("displayWords ok");
            linkWordsCount = parser.getLinkWords(pagesTree);
            System.out.println("linkWords ok");
            numberOfPages = pagesTree.size();
            System.out.println("nPages ok");
            downloadFiles = parser.getFilesForDownload(locationUniqueLinks);
            System.out.println("download files ok");
            audioFiles = parser.getAudioFiles(locationUniqueLinks);
            System.out.println("audio files ok");
            videoFiles = parser.getVideoFiles(locationUniqueLinks);
            System.out.println("video files ok");
            multimediaFiles = audioFiles + videoFiles;
            System.out.println("multimedia files ok");
            averageNumberOfUniqueInternalLinks = parser.reducirDecimales((float) nUniqueInternalLinks / numberOfPages);
            System.out.println("averageNumberOfUniqueInternalLinks ok");
            averageNumberOfInternalLinks = parser.reducirDecimales((float) nInternalLinks / numberOfPages);
            System.out.println("averageNumberOfInternalLinks ok");
            averageNumberOfUniqueExternalLinks = parser.reducirDecimales((float) nUniqueExternalLinks / numberOfPages);
            System.out.println("averageNumberOfUniqueExternalLinks ok");
            averageNumberOfExternalLinks = parser.reducirDecimales((float) nExternalLinks / numberOfPages);
            System.out.println("averageNumberOfExternalLinks ok");
            averageNumberOfUniqueLinks = parser.reducirDecimales((float) nUniqueLinks / numberOfPages);
            System.out.println("averageNumberOfUniqueLinks ok");
            averageNumberOfLinks = parser.reducirDecimales((float) nLinks / numberOfPages);
            System.out.println("averageNumberOfLinks ok");
            averageWords = parser.reducirDecimales((float) displayWordsCount / numberOfPages);
            System.out.println("averageWords ok");
            averageDownloadFiles = parser.reducirDecimales((float) downloadFiles / numberOfPages);
            System.out.println("averageDownloadFiles ok");
            averageAudioFiles = parser.reducirDecimales((float) audioFiles / numberOfPages);
            System.out.println("averageAudioFiles ok");
            averageVideoFiles = parser.reducirDecimales((float) videoFiles / numberOfPages);
            System.out.println("averageVideoFiles ok");
            averageMultimediaFiles = parser.reducirDecimales((float) multimediaFiles / numberOfPages);
            System.out.println("averageMultimediaFiles ok");
            averageApplets = parser.reducirDecimales((float) nApplets / numberOfPages);
            System.out.println("averageApplets ok");
            averageImages = parser.reducirDecimales((float) nImgs / numberOfPages);
            System.out.println("averageImages ok");
            averageHtmlBytes = parser.reducirDecimales((float) htmlSize / numberOfPages);
            System.out.println("averageHtmlBytes ok");
            averageImagesSize = parser.reducirDecimales((float) imagesSize / numberOfPages);
            System.out.println("averageImagesSize ok");
            averageScripts = parser.reducirDecimales((float) nScripts / numberOfPages);
            System.out.println("averageScripts ok");

            for(int i = 0; i < modelAttributes.size(); i++)
            {
               if(modelAttributes.get(i).equals("Links_number"))
               {
                  attributesValues.add(Integer.valueOf(nLinks).doubleValue());
               }

               if(modelAttributes.get(i).equals("Links_unique_number"))
               {
                  attributesValues.add(Integer.valueOf(nUniqueLinks).doubleValue());
               }

               if(modelAttributes.get(i).equals("Internal_links"))
               {
                  attributesValues.add(Integer.valueOf(nInternalLinks).doubleValue());
               }

               if(modelAttributes.get(i).equals("Unique_internal_links"))
               {
                  attributesValues.add(Integer.valueOf(nUniqueInternalLinks).doubleValue());
               }

               if(modelAttributes.get(i).equals("External_links"))
               {
                  attributesValues.add(Integer.valueOf(nExternalLinks).doubleValue());
               }

               if(modelAttributes.get(i).equals("Unique_external_links"))
               {
                  attributesValues.add(Integer.valueOf(nUniqueExternalLinks).doubleValue());
               }

               if(modelAttributes.get(i).equals("Image_Number"))
               {
                  attributesValues.add(Integer.valueOf(nImgs).doubleValue());
               }

               if(modelAttributes.get(i).equals("Html_bytes"))
               {
                  attributesValues.add(Integer.valueOf(htmlSize).doubleValue());
               }

               if(modelAttributes.get(i).equals("Images_Size"))
               {
                  attributesValues.add(Integer.valueOf(imagesSize).doubleValue());
               }

               if(modelAttributes.get(i).equals("Script_number"))
               {
                  attributesValues.add(Integer.valueOf(nScripts).doubleValue());
               }

               if(modelAttributes.get(i).equals("Applets_Number"))
               {
                  attributesValues.add(Integer.valueOf(nApplets).doubleValue());
               }

               if(modelAttributes.get(i).equals("Display_word_count"))
               {
                  attributesValues.add(Integer.valueOf(displayWordsCount).doubleValue());
               }

               if(modelAttributes.get(i).equals("Link_word_count"))
               {
                  attributesValues.add(Integer.valueOf(linkWordsCount).doubleValue());
               }

               if(modelAttributes.get(i).equals("Number_of_pages"))
               {
                  attributesValues.add(Integer.valueOf(numberOfPages).doubleValue());
               }

               if(modelAttributes.get(i).equals("download_files"))
               {
                  attributesValues.add(Integer.valueOf(downloadFiles).doubleValue());
               }

               if(modelAttributes.get(i).equals("audio_files"))
               {
                  attributesValues.add(Integer.valueOf(audioFiles).doubleValue());
               }

               if(modelAttributes.get(i).equals("video_files"))
               {
                  attributesValues.add(Integer.valueOf(videoFiles).doubleValue());
               }

               if(modelAttributes.get(i).equals("multimedia_files"))
               {
                  attributesValues.add(Integer.valueOf(multimediaFiles).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_number_unique_internal_links"))
               {
                  attributesValues.add(Float.valueOf(averageNumberOfUniqueInternalLinks).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_number_internal_links"))
               {
                  attributesValues.add(Float.valueOf(averageNumberOfInternalLinks).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_number_unique_external_links"))
               {
                  attributesValues.add(Float.valueOf(averageNumberOfUniqueExternalLinks).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_number_external_links"))
               {
                  attributesValues.add(Float.valueOf(averageNumberOfExternalLinks).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_number_unique_links"))
               {
                  attributesValues.add(Float.valueOf(averageNumberOfUniqueLinks).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_number_links"))
               {
                  attributesValues.add(Float.valueOf(averageNumberOfLinks).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_number_words"))
               {
                  attributesValues.add(Float.valueOf(averageWords).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_files_download"))
               {
                  attributesValues.add(Float.valueOf(averageDownloadFiles).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_audio_files"))
               {
                  attributesValues.add(Float.valueOf(averageAudioFiles).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_video_files"))
               {
                  attributesValues.add(Float.valueOf(averageVideoFiles).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_multimedia_files"))
               {
                  attributesValues.add(Float.valueOf(averageMultimediaFiles).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_applets_number"))
               {
                  attributesValues.add(Float.valueOf(averageApplets).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_image_number"))
               {
                  attributesValues.add(Float.valueOf(averageImages).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_html_bytes"))
               {
                  attributesValues.add(Float.valueOf(averageHtmlBytes).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_images_size"))
               {
                  attributesValues.add(Float.valueOf(averageImagesSize).doubleValue());
               }

               if(modelAttributes.get(i).equals("average_scripts_number"))
               {
                  attributesValues.add(Float.valueOf(averageScripts).doubleValue());
               }
            }
         }
         catch (SQLException ex)
         {
            System.err.println(ex);
         }
         
         object = new ObjectData(idLo, location);

         object.insertObjectdata(connection);

         objectMetrics = new ObjectMetrics(nLinks, nUniqueLinks, nInternalLinks, nUniqueInternalLinks, nExternalLinks,
                 nUniqueExternalLinks, nImgs, htmlSize, imagesSize, nScripts, nApplets,
                 displayWordsCount, linkWordsCount, numberOfPages, downloadFiles, audioFiles,
                 videoFiles, multimediaFiles, averageNumberOfUniqueInternalLinks,
                 averageNumberOfInternalLinks, averageNumberOfUniqueExternalLinks,
                 averageNumberOfExternalLinks, averageNumberOfUniqueLinks, averageNumberOfLinks,
                 averageWords, averageDownloadFiles, averageAudioFiles, averageVideoFiles,
                 averageMultimediaFiles, averageApplets, averageImages, averageHtmlBytes,
                 averageImagesSize, averageScripts);

         if(reevaluate)
         {
            dataBaseCrawler.setDataMerlot(object, objectMetrics, connection, true);
         }
         else
         {
            dataBaseCrawler.setDataMerlot(object, objectMetrics, connection, false);
         }

         if (htmlSize == 0)
         {
            dataBaseCrawler.updateErrorFieldMerlot(idLo, 1, connection);
            JOptionPane.showMessageDialog(null, "An error occurred while extracting metrics!", "Erro", JOptionPane.ERROR_MESSAGE);
            
            return null;
         }

         fecha_hora_final = new Date(); //Show time
         System.out.println("Time : " + fecha_hora_final); //Show time
         dataBaseCrawler.closeConnection(connection);

         return attributesValues;
      }
      else
      {
         System.out.println("Location: " + location);
         nLinks = 0;
         nUniqueLinks = 0;
         nInternalLinks = 0;
         nUniqueInternalLinks = 0;
         nExternalLinks = 0;
         nUniqueExternalLinks = 0;
         nImgs = 0;
         htmlSize = 0;
         imagesSize = 0;
         nScripts = 0;
         nApplets = 0;
         displayWordsCount = 0;
         linkWordsCount = 0;
         numberOfPages = 0;
         downloadFiles = 0;
         audioFiles = 0;
         videoFiles = 0;
         multimediaFiles = 0;
         averageNumberOfUniqueInternalLinks = 0;
         averageNumberOfInternalLinks = 0;
         averageNumberOfUniqueExternalLinks = 0;
         averageNumberOfExternalLinks = 0;
         averageNumberOfUniqueLinks = 0;
         averageNumberOfLinks = 0;
         averageWords = 0;
         averageDownloadFiles = 0;
         averageAudioFiles = 0;
         averageVideoFiles = 0;
         averageMultimediaFiles = 0;
         averageApplets = 0;
         averageImages = 0;
         averageHtmlBytes = 0;
         averageImagesSize = 0;
         averageScripts = 0;

         object = new ObjectData(idLo, location);

         object.insertObjectdata(connection);

         objectMetrics = new ObjectMetrics(nLinks, nUniqueLinks, nInternalLinks, nUniqueInternalLinks, nExternalLinks,
                 nUniqueExternalLinks, nImgs, htmlSize, imagesSize, nScripts, nApplets,
                 displayWordsCount, linkWordsCount, numberOfPages, downloadFiles, audioFiles,
                 videoFiles, multimediaFiles, averageNumberOfUniqueInternalLinks,
                 averageNumberOfInternalLinks, averageNumberOfUniqueExternalLinks,
                 averageNumberOfExternalLinks, averageNumberOfUniqueLinks, averageNumberOfLinks,
                 averageWords, averageDownloadFiles, averageAudioFiles, averageVideoFiles,
                 averageMultimediaFiles, averageApplets, averageImages, averageHtmlBytes,
                 averageImagesSize, averageScripts);

         if(reevaluate)
         {
            dataBaseCrawler.setDataMerlot(object, objectMetrics, connection, true);
         }
         else
         {
            dataBaseCrawler.setDataMerlot(object, objectMetrics, connection, false);
         }

         dataBaseCrawler.updateErrorFieldMerlot(idLo, 2, connection);

         fecha_hora_final = new Date(); //Show time
         System.out.println("Time : " + fecha_hora_final); //Show time
         dataBaseCrawler.closeConnection(connection);

         JOptionPane.showMessageDialog(null, "An error occurred while extracting metrics!", "Erro", JOptionPane.ERROR_MESSAGE);

         return null;
      }
   }
}

