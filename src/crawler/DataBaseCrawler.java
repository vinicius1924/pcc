/*
=============
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package crawler;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.sql.*;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 *
 * @author Victor Apellaniz
 */

/**
* Class: DataBaseCrawler
 Used for connecting to the merlot database and inserting data
*/
public class DataBaseCrawler
{
   //private Statement statement = null;
   //private Connection connection;
   
   private String url;
   private String user;
   private String password;

   public DataBaseCrawler()
   {
      
   }
   
   /**
    * Conecta no banco de dados
    * 
    * @since 1.0
    * @param database o nome do banco de dados em que será feita a conexão
    * @param user o nome do usário para acessar o banco de dados
    * @param password a senha do usário para acessar o banco de dados
    */
   public DataBaseCrawler(String database, String user, String password)
   {
      this.url = database;
      this.user = user;
      this.password = password;
   }
   
   /**
    * Retorna uma conexão com o banco de dados ou imprime uma exceção caso a mesma ocorra
    * 
    * @since 1.0
    * @return Connection - conexao com o banco de dados
    */
   public Connection connect()
   {
      Connection con = null;

      try
      {
         con = (Connection) DriverManager.getConnection(this.url, this.user, this.password);
      }
      catch (SQLException ex)
      {
         JOptionPane.showMessageDialog(null, ex, "Erro", JOptionPane.ERROR_MESSAGE);
         System.exit(1);
      }

      return con;
   }

  /**
    * Retorna um conjunto com o resultado da consulta ao banco de dados ou imprime uma exceção 
    * caso a mesma ocorra
    * 
    * @since 1.0
    * @param conexao a conexão estabelecida com o banco de dados
    * @param consulta a consulta a ser feita no banco de dados
    * @return ResultSet - conjunto com o resultado da consulta ao banco de dados
    */
   public ResultSet consultaBancoDeDados(Connection conexao, String consulta)
   {
      Statement st = null;
      ResultSet rs = null;

      try
      {
         st = (Statement) conexao.createStatement();
         rs = st.executeQuery(consulta);
      }
      catch (SQLException ex)
      {
         JOptionPane.showMessageDialog(null, ex, "Erro", JOptionPane.ERROR_MESSAGE);
         //System.exit(1);
      }

      return rs;
   }

   /**
    * Insere na tabela metrics as métricas coletadas pelo crawler
    *
    * @since 1.0
    * @param object um objeto contendo o seu ID e a sua localização
    * @param objectMetrics um objeto contendo as métricas coletadas pelo crawler
    * @param connection a conexao com o banco de dados
    * @param update true se o objeto já existe e deve ser atualizado ou false se o objeto não existe
    */
   public void setDataMerlot(ObjectData object, ObjectMetrics objectMetrics, Connection connection, boolean update)
   {
      Statement statement;
      String insertScript = null;
      PreparedStatement addData = null;
       
      try
      {
         int pageError = 0;
         
         if(objectMetrics.getHtmlBytes() == 0)
         {
            pageError = 1;
         }
         
         objectMetrics.setPageError(pageError);
         
         statement = connection.createStatement();

         if(update)
         {
            insertScript = ("UPDATE `Crawler`.`metrics` SET "
                           + "`Links_number` = ?, `Links_unique_number` = ?, `Internal_links` = ?, "
                           + "`Unique_internal_links` = ?, `External_links` = ?, `Unique_external_links` = ?, "
                           + "`Image_Number` = ?, `Html_bytes` = ?, `Images_Size` = ?, `Script_number` = ?, `Applets_Number` = ?, "
                           + "`Display_word_count` = ?, `Link_word_count` = ?, `Number_of_pages` = ?, `download_files` = ?, "
                           + "`audio_files` = ?, `video_files` = ?, `multimedia_files` = ?, "
                           + "`average_number_unique_internal_links` = ?, `average_number_internal_links` = ?, "
                           + "`average_number_unique_external_links` = ?, `average_number_external_links` = ?, "
                           + "`average_number_unique_links` = ?, `average_number_links` = ?, `average_number_words` = ?, "
                           + "`average_files_download` = ?, `average_audio_files` = ?, `average_video_files` = ?, "
                           + "`average_multimedia_files` = ?, `average_applets_number` = ?, `average_image_number` = ?, " 
                           + "`average_html_bytes` = ?, `average_images_size` = ?, `average_scripts_number` = ?, "
                           + "`Page_Error` = ?\n"
                           + "WHERE `lodata_ID_LO` = '" + object.getID() + "'");
            
            addData = connection.prepareStatement(insertScript);
            
            addData.setInt(1, objectMetrics.getLinksNumber());
            addData.setInt(2, objectMetrics.getLinksUniqueNumber());
            addData.setInt(3, objectMetrics.getInternalLinks());
            addData.setInt(4, objectMetrics.getUniqueInternalLinks());
            addData.setInt(5, objectMetrics.getExternalLinks());
            addData.setInt(6, objectMetrics.getUniqueExternalLinks());
            addData.setInt(7, objectMetrics.getImageNumber());
            addData.setInt(8, objectMetrics.getHtmlBytes());
            addData.setInt(9, objectMetrics.getImagesSize());
            addData.setInt(10, objectMetrics.getScriptsNumber());
            addData.setInt(11, objectMetrics.getAppletsNumber());
            addData.setInt(12, objectMetrics.getDisplayWordCount());
            addData.setInt(13, objectMetrics.getLinkWordCount());
            addData.setInt(14, objectMetrics.getNumberOfPages());
            addData.setInt(15, objectMetrics.getDownloadFiles());
            addData.setInt(16, objectMetrics.getAudioFiles());
            addData.setInt(17, objectMetrics.getVideoFiles());
            addData.setInt(18, objectMetrics.getMultimediaFiles());
            addData.setFloat(19, objectMetrics.getAverageNumberUniqueInternalLinks());
            addData.setFloat(20, objectMetrics.getAverageNumberInternalLinks());
            addData.setFloat(21, objectMetrics.getAverageNumberUniqueExternalLinks());
            addData.setFloat(22, objectMetrics.getAverageNumberExternalLinks());
            addData.setFloat(23, objectMetrics.getAverageNumberUniqueLinks());
            addData.setFloat(24, objectMetrics.getAverageNumberLinks());
            addData.setFloat(25, objectMetrics.getAverageNumberWords());
            addData.setFloat(26, objectMetrics.getAverageFilesDownload());
            addData.setFloat(27, objectMetrics.getAverageAudioFiles());
            addData.setFloat(28, objectMetrics.getAverageVideoFiles());
            addData.setFloat(29, objectMetrics.getAverageMultimediaFiles());
            addData.setFloat(30, objectMetrics.getAverageAppletsNumber());
            addData.setFloat(31, objectMetrics.getAverageImageNumber());
            addData.setFloat(32, objectMetrics.getAverageHtmlBytes());
            addData.setFloat(33, objectMetrics.getAverageImagesSize());
            addData.setFloat(34, objectMetrics.getAverageScriptsNumber());
            addData.setInt(35, objectMetrics.getPageError());
         }
         else
         {
            insertScript = ("INSERT INTO Crawler.metrics VALUES " +
                           "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
            addData = connection.prepareStatement(insertScript);
            
            addData.setInt(1, object.getID());
            addData.setInt(2, objectMetrics.getLinksNumber());
            addData.setInt(3, objectMetrics.getLinksUniqueNumber());
            addData.setInt(4, objectMetrics.getInternalLinks());
            addData.setInt(5, objectMetrics.getUniqueInternalLinks());
            addData.setInt(6, objectMetrics.getExternalLinks());
            addData.setInt(7, objectMetrics.getUniqueExternalLinks());
            addData.setInt(8, objectMetrics.getImageNumber());
            addData.setInt(9, objectMetrics.getHtmlBytes());
            addData.setInt(10, objectMetrics.getImagesSize());
            addData.setInt(11, objectMetrics.getScriptsNumber());
            addData.setInt(12, objectMetrics.getAppletsNumber());
            addData.setInt(13, objectMetrics.getDisplayWordCount());
            addData.setInt(14, objectMetrics.getLinkWordCount());
            addData.setInt(15, objectMetrics.getNumberOfPages());
            addData.setInt(16, objectMetrics.getDownloadFiles());
            addData.setInt(17, objectMetrics.getAudioFiles());
            addData.setInt(18, objectMetrics.getVideoFiles());
            addData.setInt(19, objectMetrics.getMultimediaFiles());
            addData.setFloat(20, objectMetrics.getAverageNumberUniqueInternalLinks());
            addData.setFloat(21, objectMetrics.getAverageNumberInternalLinks());
            addData.setFloat(22, objectMetrics.getAverageNumberUniqueExternalLinks());
            addData.setFloat(23, objectMetrics.getAverageNumberExternalLinks());
            addData.setFloat(24, objectMetrics.getAverageNumberUniqueLinks());
            addData.setFloat(25, objectMetrics.getAverageNumberLinks());
            addData.setFloat(26, objectMetrics.getAverageNumberWords());
            addData.setFloat(27, objectMetrics.getAverageFilesDownload());
            addData.setFloat(28, objectMetrics.getAverageAudioFiles());
            addData.setFloat(29, objectMetrics.getAverageVideoFiles());
            addData.setFloat(30, objectMetrics.getAverageMultimediaFiles());
            addData.setFloat(31, objectMetrics.getAverageAppletsNumber());
            addData.setFloat(32, objectMetrics.getAverageImageNumber());
            addData.setFloat(33, objectMetrics.getAverageHtmlBytes());
            addData.setFloat(34, objectMetrics.getAverageImagesSize());
            addData.setFloat(35, objectMetrics.getAverageScriptsNumber());
            addData.setInt(36, objectMetrics.getPageError());
         }
                
         

         addData.executeUpdate();
         addData.close();
         statement.close();
      }
      catch(SQLException error)
      {
         System.err.println(error);
      }
   }

   /*
    * Function: updateErrorFieldMerlot
    * Description: Sets the error type
    * In: id_Lo to set the error and typeError to set the concrete error.
    * Out: void
    *
    * **********ERROR DEFINITION:****************
    * 1-> Page not accesible (Not found).       *
    * 2-> Page not recognized (like .mhtml).    *
    * 3-> Unknown error                         *
    * *******************************************
    */
   /**
    * Atualiza o campo page error na tabela metrics caso tenha ocorrido algum erro ao pegar os
    * dados do site
    *
    * @since 1.0
    * @param idLo o ID do objeto
    * @param typeError o tipo de erro que ocorreu
    * @param connection a conexao com o banco de dados
    */
   public void updateErrorFieldMerlot(int idLo, int typeError, Connection connection)// throws SQLException
   {
      Statement statement;
      String PageNotFoundScript = ("UPDATE Crawler.metrics SET Page_Error = " + typeError + " WHERE lodata_ID_LO=" + idLo);
      
      try
      {
         statement = connection.createStatement();
         statement.executeUpdate(PageNotFoundScript);
         statement.close();
      }
      catch (SQLException ex)
      {
         System.err.println(ex);
      }
   }

   /**
    * Fecha a conexão com o banco de dados
    * 
    * @since 1.0
    * @param connection a conexão com o banco de dados
    */
   public void closeConnection(Connection connection)
   {
      try
      {
         System.out.println("Connection with DB closed");
         connection.close();
      }
      catch(SQLException e)
      {
         System.out.println("SQLException " + e.getMessage());
      }
   }

   /**
    * Retorna o maior ID entre os LOs existentes no banco de dados ou 0 caso não exista nenhum
    * 
    * @since 1.0
    * @return int - o maior ID ou 0 caso não exista nenhum objeto
    */
   public int getMaxLOID(Connection connection)
   {
      Statement statement = null;
      ResultSet res;
      int maxID = 0;
      
      try
      {
         statement = connection.createStatement();
         res = statement.executeQuery("SELECT MAX(`ID_LO`) AS maxID FROM `Crawler`.`lodata`");
         res.next();
         maxID = res.getInt("maxID");
      }
      catch (SQLException ex)
      {
         System.err.println(ex);
      }

      return maxID;
   }
   
   /**
    * Pega o ID de um objeto do banco de dados utilizado pelo crawler
    * 
    * @param connection a conexão com o banco de dados
    * @param URL a URL da qual se deseja obter o ID
    * @return int - um inteiro que indica o ID do objeto
    */
   public int getLOID(Connection connection, String URL)
   {
      Statement statement = null;
      ResultSet res;
      int id = 0;
      
      try
      {
         statement = connection.createStatement();
         res = statement.executeQuery("SELECT `ID_LO` FROM `Crawler`.`lodata` WHERE `Location` = '" + URL + "'");
         res.next();
         id = res.getInt("ID_LO");
      }
      catch (SQLException ex)
      {
         System.err.println(ex);
      }

      return id;
   }
   
   /**
    * Lê o arquivo XML com as configurações para fazer a conexão com o servidor
    * 
    * @return List<String> - uma lista com os parâmetros para a conexão na seguinte ordem: url,
    * usuario e senha
    */
   public List<String> leXML(String xmlName)
   {
      List<String> configuracao = new ArrayList<>();
      
      try
      {
         File f = new File(xmlName);

         SAXBuilder builder = new SAXBuilder();

         Document doc = builder.build(f);

         Element root = (Element) doc.getRootElement();
         
         configuracao.add(root.getChildText("Url"));
         configuracao.add(root.getChildText("Usuario"));
         configuracao.add(root.getChildText("Senha"));
      }
      catch(IOException | JDOMException ex)
      {
         System.err.println(ex);
      }
      
      return configuracao;
   }
   
   /**
    * Testa se o crawler já analisou a URL informada pelo usuário
    * 
    * @since 1.0
    * @param URL a URL informada pelo usuário
    * @param connection a conexão com o banco de dados
    * @return boolean - true se a URL já foi analisada ou false caso contrário
    */
   public boolean testIfURLExistsOnDataBase(String URL, Connection connection)
   {
      Statement statement;
      ResultSet res;
      int number = 0;
      
      try
      {
         statement = connection.createStatement();
         res = statement.executeQuery("SELECT COUNT(`Location`) AS number FROM `Crawler`.`lodata` WHERE Location = " 
                 + "'" + URL + "'");
         
         res.next();
         number = res.getInt("number");
      }
      catch (SQLException ex)
      {
         System.err.println(ex);
      }
      
      if(number > 0)
         return true;
      else
         return false;
   }
}
