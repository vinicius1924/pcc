/*
 =============
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package crawler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeSet;
import net.htmlparser.jericho.Config;
import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.LoggerProvider;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.TextExtractor;

/**
 * Gets all the metric's data
 *
 * @author Víctor Apellániz
 */
public class MetricsParser
{

   /**
    * Constructor
    */
   public MetricsParser()
   {
   }

   /**
    * Pega o código fonte da página
    *
    * @since 1.0
    * @param URL o endereço da página a ser analisada
    * @return Source - o código html da página
    */
   public Source getSource(String URL)
   {
      Source source = null;
      
      try
      {
         URL url = new URL(URL);
         URLConnection urlConnection = url.openConnection();
         urlConnection.setConnectTimeout(10000); //20 secs timeout
         urlConnection.setReadTimeout(10000); //20 secs timeout
         urlConnection.connect();
         source = new Source(urlConnection);
      }
      catch (ConnectException ce)
      {
         System.err.println(ce);
      }
      catch (FileNotFoundException | MalformedURLException | IllegalArgumentException fe)
      {
         System.err.println(fe);
      }
      catch (UnknownHostException he)
      {
         System.err.println(he);
         source = null;
      }
      catch (IOException e)
      {
         System.err.println(e);
         source = null;
      }
      
      return source;
   }

   /**
    * Pega o tamanho de uma arvore
    *
    * @since 1.0
    * @param tree uma arvore
    * @return int - o tamanho da arvore
    * @throws SQLException
    */
   public int getTreeSize(TreeSet tree) throws SQLException
   {
      int nLinks = 0;
      
      try
      {
         nLinks = tree.size();
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return nLinks;
   }

   /**
    * Pega todos os links da URL
    *
    * @since 1.0
    * @param URL o endereço a ser analiasado
    * @return LinkedList<String> - uma lista com todos os links
    * @throws SQLException
    */
   public LinkedList<String> getLinkList(String URL) throws SQLException
   {
      Config.LoggerProvider = LoggerProvider.DISABLED;
      LinkedList<String> links = new LinkedList<>();
      
      try
      {
         Source source = this.getSource(URL);
         if (source != null)
         {
            List<Element> linkList = source.getAllElements("a");

            for (Element link : linkList)
            {
               String path = link.toString().substring(link.toString().indexOf("\"") + 1, link.toString().length());
               path = path.substring(0, path.indexOf("\""));
               String fixedLink = this.fixLink(URL, path);
               links.add(fixedLink);
            }
         }
      }
      catch (SQLException e)
      {
         System.err.println(e);
      }
      
      return links;
   }

   /**
    * Monta uma arvore do site
    *
    * @param URL o endereço a ser analisado
    * @return TreeSet<String> - uma arvore do site
    * @throws SQLException
    */
   public TreeSet<String> getTreeSite(String URL) throws SQLException
   {
      Config.LoggerProvider = LoggerProvider.DISABLED;
      TreeSet<String> linksTree = new TreeSet();
      String path = null;
      
      try
      {
         Source source = this.getSource(URL);
         
         if(source != null)
         {
            List<Element> linkList = source.getAllElements("a");
            linksTree.add(URL);
            
            if(!linkList.isEmpty())
            {
               for(Element link : linkList)
               {
                  if(link.toString().contains("\""))
                  {
                     path = link.toString().substring(link.toString().indexOf("\"") + 1, link.toString().length());
                     path = path.substring(0, path.indexOf("\""));
                  }
                  else
                  {
                     path = link.toString().substring(link.toString().indexOf("<a href=") + 8, link.toString().length());
                     path = path.substring(0, path.indexOf(">"));
                  }
                  String fixedLink = fixLink(URL, path);
                  //If depends on the parent web, we get all the links inside
                  if (fixedLink.startsWith(URL.substring(0, URL.lastIndexOf("/"))))
                  {
                     linksTree.addAll(getLinkList(fixedLink));
                  }
                  
                  linksTree.add(fixedLink);
               }
            }
         }
      }
      catch (SQLException e)
      {
         System.err.println(e);
      }
      
      return linksTree;
   }

   /**
    * Monta uma arvore com todos os links unicos
    *
    * @since 1.0
    * @param pagesTree uma arvore com as paginas a serem avaliadas
    * @return TreeSet<String> - uma arvore com os links unicos
    * @throws SQLException
    */
   public TreeSet<String> getUniqueLinksTree(TreeSet pagesTree) throws SQLException
   {
      TreeSet<String> linksTree = new TreeSet();
      
      try
      {
         for (Iterator iter = pagesTree.iterator(); iter.hasNext();)
         {
            String linked = (String) iter.next();
            linksTree.addAll(this.getLinkList(linked));
            System.out.print(">");
         }
      }
      catch (SQLException e)
      {
         System.err.println(e);
      }
      
      return linksTree;
   }

   /**
    * Pega a arvore de links
    *
    * @since 1.0
    * @param pagesTree uma arvore com as paginas a serem avaliadas
    * @return LinkedList<String> - uma arvore com os links
    * @throws SQLException
    */
   public LinkedList<String> getLinksTree(TreeSet pagesTree) throws SQLException
   {
      LinkedList<String> linksList = new LinkedList();
      
      try
      {
         for (Iterator iter = pagesTree.iterator(); iter.hasNext();)
         {
            String linked = (String) iter.next();
            linksList.addAll(getLinkList(linked));
            System.out.print(">");
         }
      }
      catch (SQLException e)
      {
         System.err.println(e);
      }
      
      return linksList;
   }

   /**
    * Testa se o link é um link interno
    *
    * @since 1.0
    * @param URL uma URL
    * @param link um link existente nesta URL
    * @return boolean - true se o link é interno ou falso caso contrário
    */
   public boolean isInternalLink(String URL, String link)
   {
      boolean isInternal = false;
      
      try
      {
         String locationTemp = URL.substring(URL.indexOf("/") + 2); //Delete http://
         String locationRoot = "";
         
         if (locationTemp.contains("/"))
         {
            locationRoot = locationTemp.substring(0, locationTemp.indexOf("/"));
         }
         else
         {
            locationRoot = locationTemp;
         }
         locationRoot = "http://" + locationRoot;
         
         if (link.startsWith(locationRoot) || !link.startsWith("http://"))
         { //Consider mail address and ftp like internal
            isInternal = true;
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return isInternal;
   }

   /**
    * Conta o número de links internos únicos
    *
    * @since 1.0
    * @param URL a URL a ser analisada
    * @param uniqueLinksList uma lista contendo os links unicos existentes
    * @return int - o número de links internos únicos
    * @throws SQLException
    */
   public int getUniqueIntenalLinksNumber(String URL, TreeSet<String> uniqueLinksList) throws SQLException
   {
      int nLinks = 0;
      Config.LoggerProvider = LoggerProvider.DISABLED;
      
      try
      {
         for (String link : uniqueLinksList)
         {
            if (this.isInternalLink(URL, link))
            {
               nLinks++;
            }
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return nLinks;
   }

   /**
    * Conta o número de links externos únicos
    *
    * @since 1.0
    * @param URL a URL a ser analisada
    * @param uniqueLinksList uma lista contendo os links unicos existentes
    * @return int - o número de links externos únicos
    * @throws SQLException
    */
   public int getUniqueExternalLinksNumber(String URL, TreeSet<String> uniqueLinksList) throws SQLException
   {
      Config.LoggerProvider = LoggerProvider.DISABLED;
      int nLinks = 0;
      
      try
      {
         for (String link : uniqueLinksList)
         {
            if (!this.isInternalLink(URL, link))
            {
               nLinks++;
            }
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return nLinks;
   }

   /**
    * Conta o número de links internos
    *
    * @since 1.0
    * @param URL a URL a ser analisada
    * @param linksList uma lista contendo os links existentes
    * @return int - o número de links internos
    * @throws SQLException
    */
   public int getIntenalLinksNumber(String URL, List<String> linksList) throws SQLException
   {
      Config.LoggerProvider = LoggerProvider.DISABLED;
      int nLinks = 0;
      
      try
      {
         for (String link : linksList)
         {
            if (this.isInternalLink(URL, link))
            {
               nLinks++;
            }
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return nLinks;
   }

   /**
    * Conta o número de links externos
    *
    * @since 1.0
    * @param URL a URL a ser analisada
    * @param linksList uma lista contendo os links existentes
    * @return int - o número de links externos
    * @throws SQLException
    */
   public int getExternalLinksNumber(String URL, List<String> linksList) throws SQLException
   {
      Config.LoggerProvider = LoggerProvider.DISABLED;
      int nLinks = 0;
      
      try
      {
         for (String link : linksList)
         {
            if (!this.isInternalLink(URL, link))
            {
               nLinks++;
            }
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return nLinks;
   }

   /**
    * Monta uma árvore com a localização dos links existentes na URL
    *
    * @since 1.0
    * @param URL a URL a ser analisada
    * @param locations uma arvore com todos os links existentes da URL
    * @return TreeSet - uma árvore com a localização dos links existentes na URL
    * @throws SQLException
    */
   public TreeSet getLocationPages(String URL, TreeSet locations) throws SQLException
   {
      TreeSet pages = new TreeSet();
      
      if (!URL.equals(""))
      {
         String locationPath = URL.substring(0, URL.lastIndexOf("/")); //Gets only the pàth of the locationPages, not the file

         if (locationPath.equals("http:/") || locationPath.equals("http://"))
         { //If location is a first domain site
            locationPath = URL;
         }
         
         String locationTemp = URL.substring(URL.indexOf("/") + 2); //Delete http://
         String locationRoot = "";
         
         try
         {
            if (locationTemp.contains("/"))
            {
               locationRoot = locationTemp.substring(0, locationTemp.indexOf("/"));
            }
            else
            {
               locationRoot = locationTemp;
            }
            
            locationRoot = "http://" + locationRoot;
         }
         catch (Exception e)
         {
            System.err.println(e);
         }
         
         try
         {
            pages.add(URL);
            
            for (Iterator iter = locations.iterator(); iter.hasNext();)
            {
               String linked = (String) iter.next();
               if (linked.startsWith(locationPath) && (!linked.contains("#")) && (!linked.contains("id=")) && (!linked.contains("../")) && (!linked.contains("./")))
               {
                  if (linked.endsWith(".html") || (linked.endsWith(".htm")) || (linked.endsWith(".asp")) || (linked.contains(".php")) || (linked.endsWith(".aspx")) || (linked.endsWith(".anim")) || (linked.endsWith(".pl")))
                  {
                     pages.add(linked);
                     System.out.println("Pages of " + URL + " : " + linked);
                  }
               }
            }
         }
         catch (Exception e)
         {
            System.err.println(e);
         }
      }
      
      return pages;
   }

   /**
    * Conserta links em um formato específico
    *
    * @since 1.0
    * @param URL a URL a ser analisada
    * @param path o caminho do link no html
    * @return String - o link consertado
    * @throws SQLException
    */
   public String fixLink(String URL, String path) throws SQLException
   {
      String link = null;
      
      try
      {
         if (path.startsWith("http://") || path.startsWith("ftp://") || path.startsWith("mailto:"))
         {
            link = path;
         }
         else
         {
            URL = URL.substring(0, URL.lastIndexOf("/"));
            if (path.startsWith("../../"))
            {
               path = path.substring(6);
            }
            if (path.startsWith("/"))
            {
               path = path.substring(1);
            }
            if (path.startsWith("../"))
            {
               path = path.substring(3);
            }
            if (path.startsWith("./"))
            {
               path = path.substring(2);
            }
            link = URL + "/" + path;
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return link;
   }

   /**
    * Conta o número de imagens existentes no código fonte da página
    *
    * @since 1.0
    * @param source o código fonte da página
    * @return int - o número de imagens
    */
   public int getImgNumber(Source source)
   {
      int nImgs = 0;
      
      try
      {
         List imgList = source.getAllElements("img");
         nImgs = imgList.size();
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return nImgs;
   }

   /**
    * Conta o número de imagens existentes em uma árvore com links de páginas
    *
    * @since 1.0
    * @param locationTree uma árvore contendo links de páginas
    * @return int - o número de imagens existentes
    * @throws SQLException
    */
   public int getImageNumber(TreeSet locationTree) throws SQLException
   {
      int totalImages = 0;
      int elementImages = 0;
      
      try
      {
         for (Iterator iter = locationTree.iterator(); iter.hasNext();)
         {
            String linked = (String) iter.next();
            elementImages = getImgNumber(getSource(linked));
            totalImages += elementImages;
            System.out.print(">");
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return totalImages;
   }

   /**
    * Pega o tamanho dos objetos de um código fonte
    *
    * @since 1.0
    * @param source o código fonte
    * @return int - o tamanho dos objetos
    */
   public int getObjectSize(Source source)
   {
      int objectSize = 0;

      try
      {
         objectSize = source.length();
      }
      catch (Exception e)
      {
         System.err.println(e);
         objectSize = 0;
      }
      
      return objectSize;
   }

   /**
    * Pega os HTML bytes da árvore contendo os links de páginas
    *
    * @since 1.0
    * @param locationTree uma árvore contendo links de páginas
    * @return int - o número de HTML bytes da árvore de links
    * @throws SQLException
    */
   public int getIHtmlBytes(TreeSet locationTree) throws SQLException
   {
      int totalBytes = 0;
      int elementbytes = 0;

      try
      {
         for (Iterator iter = locationTree.iterator(); iter.hasNext();)
         {
            String linked = (String) iter.next();
            elementbytes = getObjectSize(getSource(linked));
            totalBytes += elementbytes;
            System.out.print(">");
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
         elementbytes = 0;
      }
      
      return totalBytes;
   }

   /**
    * Monta uma árvore com as imagens existentes nos links da árvore de links
    *
    * @since 1.0
    * @param locationPages uma árvore com links de páginas
    * @return TreeSet - uma árvore com as imagens existentes
    * @throws SQLException
    */
   public TreeSet getImagesTree(TreeSet locationPages) throws SQLException
   {
      TreeSet imagesTree = new TreeSet();
      
      try
      {
         for (Iterator iter = locationPages.iterator(); iter.hasNext();)
         {
            String location = (String) iter.next();
            Source source = getSource(location);
            
            if (source != null)
            {
               List<net.htmlparser.jericho.Element> imgList = source.getAllElements("img");
               
               for (net.htmlparser.jericho.Element img : imgList)
               {
                  String imagen = img.getAttributeValue("src");
                  imagesTree.add(imagen);
               }
            }
         }

      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return imagesTree;
   }

   /**
    * Pega o tamanho das imagens
    *
    * @since 1.0
    * @param URL a URL a ser analisada
    * @param imagesTree uma árvore contendo todas as imagens de todas as URL's
    * @return int - o tamanho das imagens
    * @throws SQLException
    */
   public int getImagesSizeN(String URL, TreeSet imagesTree) throws SQLException
   {
      int totalSize = 0;
      int elementSize = 0;
      String src;
      String path = null;
      String tempLocation = null;
      try
      {
         for (Iterator iter = imagesTree.iterator(); iter.hasNext();)
         {
            src = iter.next().toString();
            path = this.fixLink(URL, src);
            elementSize = getObjectSize(getSource(path));
            
            if (elementSize == 0)
            {
               tempLocation = URL.substring(0, URL.lastIndexOf("/"));
               path = fixLink(tempLocation, src);
               elementSize = this.getObjectSize(getSource(path));
            }
            
            if (elementSize == 0)
            {
               tempLocation = tempLocation.substring(0, tempLocation.lastIndexOf("/"));
               path = fixLink(tempLocation, src);
               elementSize = this.getObjectSize(getSource(path));
            }
            
            if (elementSize == 0)
            {
               String tempLocation2 = tempLocation.substring(0, tempLocation.lastIndexOf("/"));
               path = fixLink(tempLocation2, src);
               elementSize = this.getObjectSize(getSource(path));
            }
            
            System.out.print(">");
            totalSize = totalSize + elementSize;
         }
      }
      catch (SQLException e)
      {
         System.err.println(e);
         elementSize = 0;
      }
      
      return totalSize;
   }

   /**
    * Conta o número de scripts existentes em uma página
    *
    * @since 1.0
    * @param source o código fonte da página
    * @return int - o número de scripts existentes na página
    */
   public int getScriptNumber(Source source)
   {
      int nScripts = 0;

      try
      {
         List scriptList = source.getAllElements("script");
         nScripts = scriptList.size();
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return nScripts;
   }

   /**
    * Conta o número de scripts existentes em uma árvore com links de páginas
    *
    * @since 1.0
    * @param pagesTree uma árvore com links de páginas
    * @return int - o número de scripts existentes em todos os links da árvore
    * @throws SQLException
    */
   public int getScriptNumber(TreeSet pagesTree) throws SQLException
   {
      int totalScriptsNumber = 0;
      int elementScriptNumber = 0;

      try
      {
         for (Iterator iter = pagesTree.iterator(); iter.hasNext();)
         {
            String linked = (String) iter.next();
            elementScriptNumber = getScriptNumber(getSource(linked));
            totalScriptsNumber += elementScriptNumber;
            System.out.print(">");
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return totalScriptsNumber;
   }

   /**
    * Conta o número de applets existentes em uma página
    *
    * @since 1.0
    * @param source o código fonte da página
    * @return int - o número de applets
    */
   public int getAppletNumber(Source source)
   {
      int nApplets = 0;

      try
      {
         List appletList = source.getAllElements("applet");
         nApplets = appletList.size();
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return nApplets;
   }

   /**
    * Conta o número de applets existentes em uma árvore com links de páginas
    *
    * @since 1.0
    * @param pagesTree uma árvore com links de páginas
    * @return int - o número de applets existentes em todos os links da árvore
    * @throws SQLException
    */
   public int getAppletNumber(TreeSet pagesTree) throws SQLException
   {
      int totalAppletsNumber = 0;
      int elementAppletsNumber = 0;

      try
      {
         for (Iterator iter = pagesTree.iterator(); iter.hasNext();)
         {
            String linked = (String) iter.next();
            elementAppletsNumber = getAppletNumber(getSource(linked));
            totalAppletsNumber += elementAppletsNumber;
            System.out.print(">");
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return totalAppletsNumber;
   }

   /**
    * Conta o número de palavras mostradas em uma página
    *
    * @since 1.0
    * @param source o código fonte da página
    * @return int - o número de palavras mostradas da página
    */
   public int displayedWordCount(Source source)
   {
      int nWords = 0;

      try
      {
         TextExtractor text = source.getTextExtractor();
         StringTokenizer words = new StringTokenizer(text.toString());
         nWords = words.countTokens();
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return nWords;
   }

   /**
    * Conta o número de palavras mostradas em uma árvore com links de páginas
    *
    * @since 1.0
    * @param locationTree uma árvore com links de páginas
    * @return int - o número de palavras mostradas em todos os links da árvore
    * @throws SQLException
    */
   public int getDisplayedWords(TreeSet locationTree) throws SQLException
   {
      int totalWords = 0;
      int elementWords = 0;

      try
      {
         for (Iterator iter = locationTree.iterator(); iter.hasNext();)
         {
            String linked = (String) iter.next();
            elementWords = displayedWordCount(getSource(linked));
            totalWords += elementWords;
            System.out.print(">");
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return totalWords;
   }

   /**
    * Conta o número de palavras nos links existentes em uma página
    *
    * @since 1.0
    * @param source o código fonte da página a ser analisada
    * @return int - o número de palavras nos links existentes na página
    */
   public int linkWordsCount(Source source)
   {
      int nWords = 0;
      int totalWords = 0;

      try
      {
         List linkList = source.getAllElements("a");
         for (Iterator i = linkList.iterator(); i.hasNext();)
         {
            Element linkElement = (Element) i.next();
            String href = linkElement.getTextExtractor().toString();
            StringTokenizer words = new StringTokenizer(href);
            nWords = words.countTokens();
            totalWords += nWords;
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return totalWords;
   }

   /**
    * Conta o número de palavras nos links existentes em uma árvore com links
    *
    * @since 1.0
    * @param locationTree uma árvore com links de páginas
    * @return int - número de palavras em todos os links
    * @throws SQLException
    */
   public int getLinkWords(TreeSet locationTree) throws SQLException
   {
      int totalWords = 0;
      int elementWords = 0;
      
      try
      {
         for (Iterator iter = locationTree.iterator(); iter.hasNext();)
         {
            String linked = (String) iter.next();
            elementWords = this.linkWordsCount(getSource(linked));
            totalWords += elementWords;
            System.out.print(">");
         }

      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return totalWords;
   }

   /**
    * Conta o número de arquivos para download
    *
    * @since 1.0
    * @param uniqueLinksList uma lista contendo todos os links únicos
    * @return int - o número de arquivos existentes para download
    * @throws SQLException
    */
   public int getFilesForDownload(TreeSet uniqueLinksList) throws SQLException
   {
      int totalFiles = 0;
      
      try
      {
         for (Iterator iter = uniqueLinksList.iterator(); iter.hasNext();)
         {
            String linked = (String) iter.next();
            if (linked.endsWith(".pdf") || linked.endsWith(".exe") || linked.endsWith(".doc") || linked.endsWith(".xls") || linked.endsWith(".odt") || linked.endsWith(".zip") || linked.endsWith(".rar") || linked.endsWith(".ppt") || linked.endsWith(".tar") || linked.endsWith(".tar.gz") || linked.endsWith(".tar.bz2") || linked.endsWith(".txt") || linked.endsWith(".java") || linked.endsWith(".pl"))
            {
               totalFiles++;
            }
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return totalFiles;
   }

   /**
    * Conta o número de arquivos de áudio
    *
    * @since 1.0
    * @param uniqueLinksList uma lista contendo todos os links únicos
    * @return int - o número de arquivos de áudio
    * @throws SQLException
    */
   public int getAudioFiles(TreeSet uniqueLinksList) throws SQLException
   {
      int totalFiles = 0;
      
      try
      {
         for (Iterator iter = uniqueLinksList.iterator(); iter.hasNext();)
         {
            String linked = (String) iter.next();
            if (linked.endsWith(".mp3") || linked.endsWith(".wav") || linked.endsWith(".midi") || linked.endsWith(".ra"))
            {
               totalFiles++;
            }
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return totalFiles;
   }

   /**
    * Conta o número de arquivos de vídeo
    *
    * @since 1.0
    * @param uniqueLinksList uma lista contendo todos os links únicos
    * @return int - o número de arquivos de vídeo
    * @throws SQLException
    */
   public int getVideoFiles(TreeSet uniqueLinksList) throws SQLException
   {
      int totalFiles = 0;
      
      try
      {
         for (Iterator iter = uniqueLinksList.iterator(); iter.hasNext();)
         {
            String linked = (String) iter.next();
            if (linked.endsWith(".mp4") || linked.endsWith(".avi") || linked.endsWith(".mov") || linked.endsWith(".swf") || linked.endsWith(".flv") || linked.endsWith(".wmv") || linked.endsWith(".mpg") || linked.endsWith(".mpeg") || linked.endsWith(".movie"))
            {
               totalFiles++;
            }
         }
      }
      catch (Exception e)
      {
         System.err.println(e);
      }
      
      return totalFiles;
   }

   /**
    * Reduz o número de casas depois da vírgula de um float
    *
    * @param entrada o número a ter as casas decimais reduzidas
    * @return float - o número com uma casa depois da vírgula
    */
   public float reducirDecimales(float entrada)
   {

      float tmpd = entrada * 100;

      int tmpi = (int) tmpd;

      tmpd = tmpd - tmpi;

      if (tmpd >= 0.5)
      {
         tmpi++;
      }

      tmpd = (float) tmpi;

      tmpd = tmpd / 100;
      return tmpd;
   }
}
