/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package crawler;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author vinicius
 */
public class ObjectData
{
   private final int ID;
   private final String location;

   public ObjectData(int ID, String location)
   {
      this.ID = ID;
      this.location = location;
   }

   public int getID()
   {
      return ID;
   }

   public String getLocation()
   {
      return location;
   }
   
   /**
    * Salva um novo registro no banco de dados do objeto que vai ser analisado pelo crawler
    * 
    * @since 1.0
    * @param connection a conexão com o banco de dados
    */
   public void insertObjectdata(Connection connection)
   {
      Statement statement = null;
      
      try
      {
         statement = connection.createStatement();
         statement.executeUpdate("INSERT INTO `Crawler`.`lodata` (ID_LO, Location) VALUES ('" 
                 + this.getID() + "', '" + this.getLocation() + "')");
      }
      catch (SQLException ex)
      {
         System.err.println(ex);
      }
   }
}
