/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package crawler;

/**
 *
 * @author vinicius
 */
public class ObjectMetrics
{
   private final int linksNumber;
   private final int linksUniqueNumber;
   private final int internalLinks;
   private final int uniqueInternalLinks;
   private final int externalLinks;
   private final int uniqueExternalLinks;
   private final int imageNumber;
   private final int htmlBytes;
   private final int imagesSize;
   private final int scriptsNumber;
   private final int appletsNumber;
   private final int displayWordCount;
   private final int linkWordCount;
   private final int numberOfPages;
   private final int downloadFiles;
   private final int audioFiles;
   private final int videoFiles;
   private final int multimediaFiles;
   private final float averageNumberUniqueInternalLinks;
   private final float averageNumberInternalLinks;
   private final float averageNumberUniqueExternalLinks;
   private final float averageNumberExternalLinks;
   private final float averageNumberUniqueLinks;
   private final float averageNumberLinks;
   private final float averageNumberWords;
   private final float averageFilesDownload;
   private final float averageAudioFiles;
   private final float averageVideoFiles;
   private final float averageMultimediaFiles;
   private final float averageAppletsNumber;
   private final float averageImageNumber;
   private final float averageHtmlBytes;
   private final float averageImagesSize;
   private final float averageScriptsNumber;
   private int pageError;

   public ObjectMetrics(int linksNumber, int linksUniqueNumber, int internalLinks, int uniqueInternalLinks, int externalLinks, 
                        int uniqueExternalLinks, int imageNumber, int htmlBytes, int imagesSize, int scriptsNumber, 
                        int appletsNumber, int displayWordCount, int linkWordCount, int numberOfPages, int downloadFiles, 
                        int audioFiles, int videoFiles, int multimediaFiles, float averageNumberUniqueInternalLinks, 
                        float averageNumberInternalLinks, float averageNumberUniqueExternalLinks, 
                        float averageNumberExternalLinks, float averageNumberUniqueLinks, float averageNumberLinks, 
                        float averageNumberWords, float averageFilesDownload, float averageAudioFiles, float averageVideoFiles, 
                        float averageMultimediaFiles, float averageAppletsNumber, float averageImageNumber, 
                        float averageHtmlBytes, float averageImagesSize, float averageScriptsNumber)
   {
      this.linksNumber = linksNumber;
      this.linksUniqueNumber = linksUniqueNumber;
      this.internalLinks = internalLinks;
      this.uniqueInternalLinks = uniqueInternalLinks;
      this.externalLinks = externalLinks;
      this.uniqueExternalLinks = uniqueExternalLinks;
      this.imageNumber = imageNumber;
      this.htmlBytes = htmlBytes;
      this.imagesSize = imagesSize;
      this.scriptsNumber = scriptsNumber;
      this.appletsNumber = appletsNumber;
      this.displayWordCount = displayWordCount;
      this.linkWordCount = linkWordCount;
      this.numberOfPages = numberOfPages;
      this.downloadFiles = downloadFiles;
      this.audioFiles = audioFiles;
      this.videoFiles = videoFiles;
      this.multimediaFiles = multimediaFiles;
      this.averageNumberUniqueInternalLinks = averageNumberUniqueInternalLinks;
      this.averageNumberInternalLinks = averageNumberInternalLinks;
      this.averageNumberUniqueExternalLinks = averageNumberUniqueExternalLinks;
      this.averageNumberExternalLinks = averageNumberExternalLinks;
      this.averageNumberUniqueLinks = averageNumberUniqueLinks;
      this.averageNumberLinks = averageNumberLinks;
      this.averageNumberWords = averageNumberWords;
      this.averageFilesDownload = averageFilesDownload;
      this.averageAudioFiles = averageAudioFiles;
      this.averageVideoFiles = averageVideoFiles;
      this.averageMultimediaFiles = averageMultimediaFiles;
      this.averageAppletsNumber = averageAppletsNumber;
      this.averageImageNumber = averageImageNumber;
      this.averageHtmlBytes = averageHtmlBytes;
      this.averageImagesSize = averageImagesSize;
      this.averageScriptsNumber = averageScriptsNumber;
   }

   public int getLinksNumber()
   {
      return linksNumber;
   }

   public int getLinksUniqueNumber()
   {
      return linksUniqueNumber;
   }

   public int getInternalLinks()
   {
      return internalLinks;
   }

   public int getUniqueInternalLinks()
   {
      return uniqueInternalLinks;
   }

   public int getExternalLinks()
   {
      return externalLinks;
   }

   public int getUniqueExternalLinks()
   {
      return uniqueExternalLinks;
   }

   public int getImageNumber()
   {
      return imageNumber;
   }

   public int getHtmlBytes()
   {
      return htmlBytes;
   }

   public int getImagesSize()
   {
      return imagesSize;
   }

   public int getScriptsNumber()
   {
      return scriptsNumber;
   }

   public int getAppletsNumber()
   {
      return appletsNumber;
   }

   public int getDisplayWordCount()
   {
      return displayWordCount;
   }

   public int getLinkWordCount()
   {
      return linkWordCount;
   }

   public int getNumberOfPages()
   {
      return numberOfPages;
   }

   public int getDownloadFiles()
   {
      return downloadFiles;
   }

   public int getAudioFiles()
   {
      return audioFiles;
   }

   public int getVideoFiles()
   {
      return videoFiles;
   }

   public int getMultimediaFiles()
   {
      return multimediaFiles;
   }

   public float getAverageNumberUniqueInternalLinks()
   {
      return averageNumberUniqueInternalLinks;
   }

   public float getAverageNumberInternalLinks()
   {
      return averageNumberInternalLinks;
   }

   public float getAverageNumberUniqueExternalLinks()
   {
      return averageNumberUniqueExternalLinks;
   }

   public float getAverageNumberExternalLinks()
   {
      return averageNumberExternalLinks;
   }

   public float getAverageNumberUniqueLinks()
   {
      return averageNumberUniqueLinks;
   }

   public float getAverageNumberLinks()
   {
      return averageNumberLinks;
   }

   public float getAverageNumberWords()
   {
      return averageNumberWords;
   }

   public float getAverageFilesDownload()
   {
      return averageFilesDownload;
   }

   public float getAverageAudioFiles()
   {
      return averageAudioFiles;
   }

   public float getAverageVideoFiles()
   {
      return averageVideoFiles;
   }

   public float getAverageMultimediaFiles()
   {
      return averageMultimediaFiles;
   }

   public float getAverageAppletsNumber()
   {
      return averageAppletsNumber;
   }

   public float getAverageImageNumber()
   {
      return averageImageNumber;
   }

   public float getAverageHtmlBytes()
   {
      return averageHtmlBytes;
   }

   public float getAverageImagesSize()
   {
      return averageImagesSize;
   }

   public float getAverageScriptsNumber()
   {
      return averageScriptsNumber;
   }

   public int getPageError()
   {
      return pageError;
   }

   public void setPageError(int pageError)
   {
      this.pageError = pageError;
   }
}
