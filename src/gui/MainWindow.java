package gui;

import classes.*;
import crawler.DataBaseCrawler;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.BayesianLogisticRegression;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.trees.J48;
import weka.core.Instances;

/**
 * @author Vinicius Santos
 *
 * @version 1.0
 *
 * @since 1.0
 */
public class MainWindow extends javax.swing.JFrame
{
   /**
    * Cria um novo form MainWindow
    *
    * @since 1.0
    */
   public MainWindow()
   {
      initComponents();
      
      Image i;
      
      /* seta o ícone do programa */
      try
      {
         i = ImageIO.read(getClass().getResource("/gui/icon.png"));
         this.setIconImage(i);
      }
      catch (IOException ex)
      {
         System.err.println(ex);
      }
      
      progressBar.setMinimum(0);
      progressBar.setMaximum(100);
      classifyDataButton.setEnabled(false);
      getMetricsButton.setEnabled(false);
      /* seta a fonte do cabeçalho da tabela (a fonte do título das colunas) para Arial tamanho 13*/
      metricsTable.getTableHeader().setFont(new Font("Arial", Font.PLAIN, 13));
      crawledMetricsTable.getTableHeader().setFont(new Font("Arial", Font.PLAIN, 13));
      model = (DefaultTableModel) metricsTable.getModel();
      modelCrawledMetrics = (DefaultTableModel) crawledMetricsTable.getModel();
      /* seta a cor de fundo do quadro onde aparecem os tipos de materiais */
      scrollPaneMaterialType.getViewport().setBackground(Color.white);
      /* seta a cor de fundo do quadro onde aparecem as categorias */
      scrollPaneCategorias.getViewport().setBackground(Color.white);
      scrollPaneCategorias.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
      scrollPaneMaterialType.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
      okButton.setEnabled(false);
      qualityPerspectiveButtonGroup.add(usersRadio);
      qualityPerspectiveButtonGroup.add(expertsRadio);
      qualityPerspectiveButtonGroup.add(personalCollectionsRadio);
      usersRadio.setFocusPainted(false);
      expertsRadio.setFocusPainted(false);
      personalCollectionsRadio.setFocusPainted(false);
      List<String> configuracao = dataBaseMerlot.leXML("Configuration.xml");
      dataBaseMerlot = new DataBaseMerlot(configuracao.get(0), configuracao.get(1), configuracao.get(2));
      conexionMerlot = dataBaseMerlot.conectaNoBancoDeDados();
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      okButtonGenerateModelJ48.setEnabled(false);
      okButtonGenerateModelPerceptron.setEnabled(false);
      okButtonGenerateModelBayes.setEnabled(false);

      components.loadInicialData(categoryModel, materialTypeModel, userRatedNumber, peerReviewRatedNumber, personalCollectionsNumber);
      
      metricsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
      {
         /* quando é mudado o valor selecionado na tabela de metricas */
         @Override  
         public void valueChanged(ListSelectionEvent evt)
         {
            /* um vetor de double que irá receber metricas dos objetos bons e não bons */
            goodAndNotGoodMetricVector = null;
            
            /* 
             * se as métricas mostradas na tabela de métricas NÃO são as métricas carregadas
             * de um modelo então calcula as estatísticas da métrica selecionada, caso o
             * contrário não calcula as estatísticas
             */
            if(!readedFromModel)
            {
               if(metricsTable.getSelectedRow() != -1 && metricsTable.getRowCount() > 0)
               {
                  if (evt.getValueIsAdjusting())
                     return;  
                  int selected = metricsTable.getSelectedRow();
                  /* 
                   * pega o valor da metrica, selecionada na tabela das metricas, de todos os objetos 
                   * bons e não bons
                   */
                  goodAndNotGoodMetricVector = utils.getGoodAndNotGoodMetricVector(selected, grupos);
                  double mean = statistic.calculateMean(goodAndNotGoodMetricVector);
                  double median = statistic.calculateMedian(goodAndNotGoodMetricVector);
                  components.showBasicStatistics(mean, median, goodAndNotGoodMetricVector, minimumValue, maximumValue, meanValue, medianValue);
                  components.showKolmogorovResult(dValue, pValueKolmogorov, kolmogorovValues.get(0).get(selected).toString(), 
                                                  kolmogorovValues.get(1).get(selected).toString());
                  components.showMannWhitneyResult(wValue, pValuemannWhitney, mannWhitneyValues.get(0).get(selected).toString(), 
                                                   mannWhitneyValues.get(1).get(selected).toString());
               }
               else
               {
                  kolmogorovValues.get(0).clear();
                  kolmogorovValues.get(1).clear();
                  mannWhitneyValues.get(0).clear();
                  mannWhitneyValues.get(1).clear();
               }
            }
         }
      });
      
      saveModel.setEnabled(false);
      selectAllMetricsMenu.setEnabled(false);
      deselectAllMetricsMenu.setEnabled(false);
      selectRelevantMetricsMenu.setEnabled(false);
   }

   /**
    * Este método é chamado de dentro do construtor para inicializar o formulário. 
    * ATENÇÃO: Não modifique o código. O conteúdo deste método é sempre reescrito pelo editor 
    * de formulários.
    */
   @SuppressWarnings("unchecked")
   // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
   private void initComponents()
   {

      qualityPerspectiveButtonGroup = new javax.swing.ButtonGroup();
      tabbedPane = new javax.swing.JTabbedPane();
      panelGeneralInformation = new javax.swing.JPanel();
      userRatedNumber = new javax.swing.JLabel();
      userRatedLabel = new javax.swing.JLabel();
      peerReviewRatedLabel = new javax.swing.JLabel();
      peerReviewRatedNumber = new javax.swing.JLabel();
      personalCollectionsLabel = new javax.swing.JLabel();
      personalCollectionsNumber = new javax.swing.JLabel();
      panelThresholds = new javax.swing.JPanel();
      goodLabel = new javax.swing.JLabel();
      notGoodLabel = new javax.swing.JLabel();
      goodThreshold = new javax.swing.JLabel();
      notGoodThreshold = new javax.swing.JLabel();
      graphicJPanel = new javax.swing.JPanel();
      graphicJLabel = new javax.swing.JLabel();
      panelExplore = new javax.swing.JPanel();
      scrollPaneMetricsTable = new javax.swing.JScrollPane();
      metricsTable = new javax.swing.JTable();
      qualityIndicatorsLabel = new javax.swing.JLabel();
      panelBasicStatistics = new javax.swing.JPanel();
      minimumValueLabel = new javax.swing.JLabel();
      maximumValueLabel = new javax.swing.JLabel();
      meanValueLabel = new javax.swing.JLabel();
      medianValueLabel = new javax.swing.JLabel();
      minimumValue = new javax.swing.JLabel();
      maximumValue = new javax.swing.JLabel();
      meanValue = new javax.swing.JLabel();
      medianValue = new javax.swing.JLabel();
      tabbedPaneStatisticMethods = new javax.swing.JTabbedPane();
      panelMannWhitney = new javax.swing.JPanel();
      wValueLabel = new javax.swing.JLabel();
      pValueMannWhitneyLabel = new javax.swing.JLabel();
      wValue = new javax.swing.JLabel();
      pValuemannWhitney = new javax.swing.JLabel();
      panelKolmogorv = new javax.swing.JPanel();
      dValueLabel = new javax.swing.JLabel();
      dValue = new javax.swing.JLabel();
      pValueKolmogorovLabel = new javax.swing.JLabel();
      pValueKolmogorov = new javax.swing.JLabel();
      panelGenerateModel = new javax.swing.JPanel();
      modelsTabs = new javax.swing.JTabbedPane();
      jPanel1 = new javax.swing.JPanel();
      jTabbedPane2 = new javax.swing.JTabbedPane();
      jPanel2 = new javax.swing.JPanel();
      scrollPaneOutput = new javax.swing.JScrollPane();
      j48outputTree = new javax.swing.JTextArea();
      jPanel3 = new javax.swing.JPanel();
      jScrollPane1 = new javax.swing.JScrollPane();
      j48outputEvaluation = new javax.swing.JTextArea();
      trainingSetRadioJ48 = new javax.swing.JRadioButton();
      crossValidationRadioJ48 = new javax.swing.JRadioButton();
      percentageSplitRadioJ48 = new javax.swing.JRadioButton();
      okButtonGenerateModelJ48 = new javax.swing.JButton();
      numberOfFoldsFieldJ48 = new javax.swing.JTextField();
      foldsLabel = new javax.swing.JLabel();
      percentageSplitFieldJ48 = new javax.swing.JTextField();
      percentLabel = new javax.swing.JLabel();
      jPanel5 = new javax.swing.JPanel();
      jTabbedPane3 = new javax.swing.JTabbedPane();
      jPanel6 = new javax.swing.JPanel();
      jScrollPane3 = new javax.swing.JScrollPane();
      multiLayerOutputTree = new javax.swing.JTextArea();
      jPanel7 = new javax.swing.JPanel();
      jScrollPane4 = new javax.swing.JScrollPane();
      multiLayerOutputEvaluation = new javax.swing.JTextArea();
      okButtonGenerateModelPerceptron = new javax.swing.JButton();
      trainingSetRadioPerceptron = new javax.swing.JRadioButton();
      crossValidationRadioPerceptron = new javax.swing.JRadioButton();
      percentageSplitRadioPerceptron = new javax.swing.JRadioButton();
      numberOfFoldsFieldPerceptron = new javax.swing.JTextField();
      percentageSplitFieldPerceptron = new javax.swing.JTextField();
      jLabel1 = new javax.swing.JLabel();
      jLabel2 = new javax.swing.JLabel();
      jPanel8 = new javax.swing.JPanel();
      jTabbedPane4 = new javax.swing.JTabbedPane();
      jPanel9 = new javax.swing.JPanel();
      jScrollPane5 = new javax.swing.JScrollPane();
      bayesianLogisticRegressionOutputTree = new javax.swing.JTextArea();
      jPanel10 = new javax.swing.JPanel();
      jScrollPane6 = new javax.swing.JScrollPane();
      bayesianLogisticRegressionOutputEvaluation = new javax.swing.JTextArea();
      trainingSetRadioBayes = new javax.swing.JRadioButton();
      crossValidationRadioBayes = new javax.swing.JRadioButton();
      percentageSplitRadioBayes = new javax.swing.JRadioButton();
      numberOfFoldsFieldBayes = new javax.swing.JTextField();
      percentageSplitFieldBayes = new javax.swing.JTextField();
      jLabel3 = new javax.swing.JLabel();
      jLabel4 = new javax.swing.JLabel();
      okButtonGenerateModelBayes = new javax.swing.JButton();
      jPanel4 = new javax.swing.JPanel();
      chooseModelButton = new javax.swing.JButton();
      choosenModelPath = new javax.swing.JTextField();
      URLPath = new javax.swing.JTextField();
      URLLabel = new javax.swing.JLabel();
      classifyDataButton = new javax.swing.JButton();
      getMetricsButton = new javax.swing.JButton();
      progressBar = new javax.swing.JProgressBar();
      jScrollPane2 = new javax.swing.JScrollPane();
      crawledMetricsTable = new javax.swing.JTable();
      classyfiedObjectLabel = new javax.swing.JLabel();
      panelQualityPerspective = new javax.swing.JPanel();
      personalCollectionsRadio = new javax.swing.JRadioButton();
      expertsRadio = new javax.swing.JRadioButton();
      usersRadio = new javax.swing.JRadioButton();
      panelCategory = new javax.swing.JPanel();
      scrollPaneCategorias = new javax.swing.JScrollPane();
      categoriesList = new javax.swing.JList(categoryModel);
      panelMaterialType = new javax.swing.JPanel();
      scrollPaneMaterialType = new javax.swing.JScrollPane();
      materialTypeList = new javax.swing.JList(materialTypeModel);
      cancelButton = new javax.swing.JButton();
      okButton = new javax.swing.JButton();
      menuBar = new javax.swing.JMenuBar();
      fileMenu = new javax.swing.JMenu();
      saveModel = new javax.swing.JMenuItem();
      editMenu = new javax.swing.JMenu();
      selectAllMetricsMenu = new javax.swing.JMenuItem();
      selectRelevantMetricsMenu = new javax.swing.JMenuItem();
      deselectAllMetricsMenu = new javax.swing.JMenuItem();

      setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
      setTitle("Merlot");
      setIconImages(null);
      setMinimumSize(new java.awt.Dimension(1024, 740));

      userRatedNumber.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      userRatedNumber.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      userRatedLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      userRatedLabel.setText("User Rated");

      peerReviewRatedLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      peerReviewRatedLabel.setText("Peer Review Rated");

      peerReviewRatedNumber.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      peerReviewRatedNumber.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      personalCollectionsLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      personalCollectionsLabel.setText("Personal Collections");

      personalCollectionsNumber.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      personalCollectionsNumber.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      panelThresholds.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Thresholds", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 13))); // NOI18N

      goodLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      goodLabel.setText("Good");

      notGoodLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      notGoodLabel.setText("Not Good");

      goodThreshold.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      goodThreshold.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      notGoodThreshold.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      notGoodThreshold.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      javax.swing.GroupLayout panelThresholdsLayout = new javax.swing.GroupLayout(panelThresholds);
      panelThresholds.setLayout(panelThresholdsLayout);
      panelThresholdsLayout.setHorizontalGroup(
         panelThresholdsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelThresholdsLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(panelThresholdsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(panelThresholdsLayout.createSequentialGroup()
                  .addComponent(notGoodLabel)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(notGoodThreshold, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
               .addGroup(panelThresholdsLayout.createSequentialGroup()
                  .addComponent(goodLabel)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                  .addComponent(goodThreshold, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGap(75, 75, 75))
      );
      panelThresholdsLayout.setVerticalGroup(
         panelThresholdsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelThresholdsLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(panelThresholdsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(goodLabel)
               .addComponent(goodThreshold, javax.swing.GroupLayout.DEFAULT_SIZE, 17, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(panelThresholdsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(notGoodLabel)
               .addComponent(notGoodThreshold, javax.swing.GroupLayout.DEFAULT_SIZE, 17, Short.MAX_VALUE))
            .addContainerGap(27, Short.MAX_VALUE))
      );

      graphicJPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Density Traces", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 13))); // NOI18N

      javax.swing.GroupLayout graphicJPanelLayout = new javax.swing.GroupLayout(graphicJPanel);
      graphicJPanel.setLayout(graphicJPanelLayout);
      graphicJPanelLayout.setHorizontalGroup(
         graphicJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(graphicJLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 642, Short.MAX_VALUE)
      );
      graphicJPanelLayout.setVerticalGroup(
         graphicJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(graphicJLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 386, javax.swing.GroupLayout.PREFERRED_SIZE)
      );

      javax.swing.GroupLayout panelGeneralInformationLayout = new javax.swing.GroupLayout(panelGeneralInformation);
      panelGeneralInformation.setLayout(panelGeneralInformationLayout);
      panelGeneralInformationLayout.setHorizontalGroup(
         panelGeneralInformationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelGeneralInformationLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(panelGeneralInformationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(panelGeneralInformationLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panelGeneralInformationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addComponent(peerReviewRatedLabel)
                     .addComponent(personalCollectionsLabel)
                     .addComponent(userRatedLabel))
                  .addGap(18, 18, 18)
                  .addGroup(panelGeneralInformationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                     .addComponent(userRatedNumber, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                     .addComponent(personalCollectionsNumber, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                     .addComponent(peerReviewRatedNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
               .addComponent(graphicJPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(panelThresholds, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(96, Short.MAX_VALUE))
      );
      panelGeneralInformationLayout.setVerticalGroup(
         panelGeneralInformationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelGeneralInformationLayout.createSequentialGroup()
            .addGroup(panelGeneralInformationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(panelGeneralInformationLayout.createSequentialGroup()
                  .addGap(26, 26, 26)
                  .addComponent(userRatedNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20))
               .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelGeneralInformationLayout.createSequentialGroup()
                  .addContainerGap()
                  .addComponent(userRatedLabel)
                  .addGap(18, 18, 18)))
            .addGroup(panelGeneralInformationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
               .addComponent(peerReviewRatedLabel)
               .addComponent(peerReviewRatedNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(18, 18, 18)
            .addGroup(panelGeneralInformationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(personalCollectionsNumber, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(personalCollectionsLabel))
            .addGap(18, 18, 18)
            .addComponent(panelThresholds, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(graphicJPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(70, Short.MAX_VALUE))
      );

      tabbedPane.addTab("General Information", panelGeneralInformation);

      metricsTable.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 0));
      metricsTable.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      metricsTable.setModel(new javax.swing.table.DefaultTableModel(
         new Object [][]
         {

         },
         new String []
         {
            "Metrics", "Indicator", "Influence", ""
         }
      )
      {
         Class[] types = new Class []
         {
            java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class
         };
         boolean[] canEdit = new boolean []
         {
            false, false, false, true
         };

         public Class getColumnClass(int columnIndex)
         {
            return types [columnIndex];
         }

         public boolean isCellEditable(int rowIndex, int columnIndex)
         {
            return canEdit [columnIndex];
         }
      });
      metricsTable.getTableHeader().setReorderingAllowed(false);
      scrollPaneMetricsTable.setViewportView(metricsTable);
      if (metricsTable.getColumnModel().getColumnCount() > 0)
      {
         metricsTable.getColumnModel().getColumn(0).setMinWidth(235);
         metricsTable.getColumnModel().getColumn(0).setPreferredWidth(235);
         metricsTable.getColumnModel().getColumn(0).setMaxWidth(235);
         metricsTable.getColumnModel().getColumn(1).setMinWidth(60);
         metricsTable.getColumnModel().getColumn(1).setPreferredWidth(60);
         metricsTable.getColumnModel().getColumn(1).setMaxWidth(60);
         metricsTable.getColumnModel().getColumn(2).setMinWidth(60);
         metricsTable.getColumnModel().getColumn(2).setPreferredWidth(60);
         metricsTable.getColumnModel().getColumn(2).setMaxWidth(60);
         metricsTable.getColumnModel().getColumn(3).setResizable(false);
      }

      qualityIndicatorsLabel.setFont(new java.awt.Font("Comic Sans MS", 0, 13)); // NOI18N
      qualityIndicatorsLabel.setText("Quality Indicators");

      panelBasicStatistics.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Basic Statistics", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 13))); // NOI18N

      minimumValueLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      minimumValueLabel.setText("Minimum");

      maximumValueLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      maximumValueLabel.setText("Maximum");

      meanValueLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      meanValueLabel.setText("Mean");

      medianValueLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      medianValueLabel.setText("Median");

      minimumValue.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      minimumValue.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      maximumValue.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      maximumValue.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      meanValue.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      meanValue.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      medianValue.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      medianValue.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      javax.swing.GroupLayout panelBasicStatisticsLayout = new javax.swing.GroupLayout(panelBasicStatistics);
      panelBasicStatistics.setLayout(panelBasicStatisticsLayout);
      panelBasicStatisticsLayout.setHorizontalGroup(
         panelBasicStatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelBasicStatisticsLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(panelBasicStatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(minimumValueLabel)
               .addComponent(maximumValueLabel)
               .addComponent(meanValueLabel)
               .addComponent(medianValueLabel))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(panelBasicStatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
               .addComponent(meanValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
               .addComponent(maximumValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
               .addComponent(minimumValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
               .addComponent(medianValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(41, 41, 41))
      );
      panelBasicStatisticsLayout.setVerticalGroup(
         panelBasicStatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelBasicStatisticsLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(panelBasicStatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
               .addGroup(panelBasicStatisticsLayout.createSequentialGroup()
                  .addComponent(minimumValueLabel)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                  .addComponent(maximumValueLabel)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                  .addComponent(meanValueLabel))
               .addGroup(panelBasicStatisticsLayout.createSequentialGroup()
                  .addComponent(minimumValue, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                  .addComponent(maximumValue, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(meanValue, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(panelBasicStatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
               .addComponent(medianValueLabel)
               .addComponent(medianValue, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
      );

      wValueLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      wValueLabel.setText("W");

      pValueMannWhitneyLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      pValueMannWhitneyLabel.setText("p-value");

      wValue.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      wValue.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      pValuemannWhitney.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      pValuemannWhitney.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      javax.swing.GroupLayout panelMannWhitneyLayout = new javax.swing.GroupLayout(panelMannWhitney);
      panelMannWhitney.setLayout(panelMannWhitneyLayout);
      panelMannWhitneyLayout.setHorizontalGroup(
         panelMannWhitneyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelMannWhitneyLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(panelMannWhitneyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(pValueMannWhitneyLabel)
               .addComponent(wValueLabel))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(panelMannWhitneyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
               .addComponent(pValuemannWhitney, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)
               .addComponent(wValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addContainerGap(109, Short.MAX_VALUE))
      );
      panelMannWhitneyLayout.setVerticalGroup(
         panelMannWhitneyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelMannWhitneyLayout.createSequentialGroup()
            .addGap(22, 22, 22)
            .addGroup(panelMannWhitneyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(wValueLabel)
               .addComponent(wValue, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(panelMannWhitneyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
               .addComponent(pValueMannWhitneyLabel)
               .addComponent(pValuemannWhitney, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(25, Short.MAX_VALUE))
      );

      tabbedPaneStatisticMethods.addTab("MannWhitney", panelMannWhitney);

      dValueLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      dValueLabel.setText("D");

      dValue.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      dValue.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      pValueKolmogorovLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      pValueKolmogorovLabel.setText("p-value");

      pValueKolmogorov.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      pValueKolmogorov.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      javax.swing.GroupLayout panelKolmogorvLayout = new javax.swing.GroupLayout(panelKolmogorv);
      panelKolmogorv.setLayout(panelKolmogorvLayout);
      panelKolmogorvLayout.setHorizontalGroup(
         panelKolmogorvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelKolmogorvLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(panelKolmogorvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(pValueKolmogorovLabel)
               .addComponent(dValueLabel))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(panelKolmogorvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
               .addComponent(pValueKolmogorov, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)
               .addComponent(dValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addContainerGap(109, Short.MAX_VALUE))
      );
      panelKolmogorvLayout.setVerticalGroup(
         panelKolmogorvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelKolmogorvLayout.createSequentialGroup()
            .addGap(22, 22, 22)
            .addGroup(panelKolmogorvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(dValueLabel)
               .addComponent(dValue, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(panelKolmogorvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
               .addComponent(pValueKolmogorovLabel)
               .addComponent(pValueKolmogorov, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(25, Short.MAX_VALUE))
      );

      tabbedPaneStatisticMethods.addTab("Kolmogorov", panelKolmogorv);

      javax.swing.GroupLayout panelExploreLayout = new javax.swing.GroupLayout(panelExplore);
      panelExplore.setLayout(panelExploreLayout);
      panelExploreLayout.setHorizontalGroup(
         panelExploreLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelExploreLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(panelExploreLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(qualityIndicatorsLabel)
               .addGroup(panelExploreLayout.createSequentialGroup()
                  .addComponent(scrollPaneMetricsTable, javax.swing.GroupLayout.PREFERRED_SIZE, 416, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(34, 34, 34)
                  .addGroup(panelExploreLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addComponent(panelBasicStatistics, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                     .addComponent(tabbedPaneStatisticMethods, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addContainerGap(32, Short.MAX_VALUE))
      );
      panelExploreLayout.setVerticalGroup(
         panelExploreLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelExploreLayout.createSequentialGroup()
            .addGroup(panelExploreLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(panelExploreLayout.createSequentialGroup()
                  .addGap(27, 27, 27)
                  .addComponent(panelBasicStatistics, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(18, 18, 18)
                  .addComponent(tabbedPaneStatisticMethods, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
               .addGroup(panelExploreLayout.createSequentialGroup()
                  .addGap(6, 6, 6)
                  .addComponent(qualityIndicatorsLabel)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(scrollPaneMetricsTable, javax.swing.GroupLayout.PREFERRED_SIZE, 609, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap(88, Short.MAX_VALUE))
      );

      tabbedPane.addTab("Explore", panelExplore);

      j48outputTree.setEditable(false);
      j48outputTree.setBackground(new java.awt.Color(1, 1, 1));
      j48outputTree.setColumns(20);
      j48outputTree.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      j48outputTree.setForeground(new java.awt.Color(254, 254, 254));
      j48outputTree.setRows(5);
      scrollPaneOutput.setViewportView(j48outputTree);

      javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
      jPanel2.setLayout(jPanel2Layout);
      jPanel2Layout.setHorizontalGroup(
         jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(scrollPaneOutput, javax.swing.GroupLayout.DEFAULT_SIZE, 746, Short.MAX_VALUE)
      );
      jPanel2Layout.setVerticalGroup(
         jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(scrollPaneOutput, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
      );

      jTabbedPane2.addTab("Model", jPanel2);

      j48outputEvaluation.setBackground(new java.awt.Color(1, 1, 1));
      j48outputEvaluation.setColumns(20);
      j48outputEvaluation.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      j48outputEvaluation.setForeground(new java.awt.Color(254, 254, 254));
      j48outputEvaluation.setRows(5);
      jScrollPane1.setViewportView(j48outputEvaluation);

      javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
      jPanel3.setLayout(jPanel3Layout);
      jPanel3Layout.setHorizontalGroup(
         jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 746, Short.MAX_VALUE)
      );
      jPanel3Layout.setVerticalGroup(
         jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
      );

      jTabbedPane2.addTab("Evaluation", jPanel3);

      trainingSetRadioJ48.setText("Using Training Set");
      trainingSetRadioJ48.addMouseListener(new java.awt.event.MouseAdapter()
      {
         public void mouseClicked(java.awt.event.MouseEvent evt)
         {
            trainingSetRadioJ48MouseClicked(evt);
         }
      });
      trainingSetRadioJ48.addItemListener(new java.awt.event.ItemListener()
      {
         public void itemStateChanged(java.awt.event.ItemEvent evt)
         {
            trainingSetRadioJ48ItemStateChanged(evt);
         }
      });

      crossValidationRadioJ48.setText("Cross-Validation");
      crossValidationRadioJ48.addMouseListener(new java.awt.event.MouseAdapter()
      {
         public void mouseClicked(java.awt.event.MouseEvent evt)
         {
            crossValidationRadioJ48MouseClicked(evt);
         }
      });
      crossValidationRadioJ48.addItemListener(new java.awt.event.ItemListener()
      {
         public void itemStateChanged(java.awt.event.ItemEvent evt)
         {
            crossValidationRadioJ48ItemStateChanged(evt);
         }
      });

      percentageSplitRadioJ48.setText("Percentage Split");
      percentageSplitRadioJ48.addMouseListener(new java.awt.event.MouseAdapter()
      {
         public void mouseClicked(java.awt.event.MouseEvent evt)
         {
            percentageSplitRadioJ48MouseClicked(evt);
         }
      });
      percentageSplitRadioJ48.addItemListener(new java.awt.event.ItemListener()
      {
         public void itemStateChanged(java.awt.event.ItemEvent evt)
         {
            percentageSplitRadioJ48ItemStateChanged(evt);
         }
      });

      okButtonGenerateModelJ48.setText("Ok");
      okButtonGenerateModelJ48.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            okButtonGenerateModelJ48ActionPerformed(evt);
         }
      });

      numberOfFoldsFieldJ48.setEnabled(false);
      numberOfFoldsFieldJ48.addKeyListener(new java.awt.event.KeyAdapter()
      {
         public void keyPressed(java.awt.event.KeyEvent evt)
         {
            numberOfFoldsFieldJ48KeyPressed(evt);
         }
         public void keyTyped(java.awt.event.KeyEvent evt)
         {
            numberOfFoldsFieldJ48KeyTyped(evt);
         }
      });

      foldsLabel.setText("Folds");

      percentageSplitFieldJ48.setEnabled(false);
      percentageSplitFieldJ48.addKeyListener(new java.awt.event.KeyAdapter()
      {
         public void keyPressed(java.awt.event.KeyEvent evt)
         {
            percentageSplitFieldJ48KeyPressed(evt);
         }
         public void keyTyped(java.awt.event.KeyEvent evt)
         {
            percentageSplitFieldJ48KeyTyped(evt);
         }
      });

      percentLabel.setText("%");

      javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
      jPanel1.setLayout(jPanel1Layout);
      jPanel1Layout.setHorizontalGroup(
         jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jTabbedPane2)
         .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(trainingSetRadioJ48)
               .addGroup(jPanel1Layout.createSequentialGroup()
                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(percentageSplitRadioJ48)
                        .addGap(18, 18, 18))
                     .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(crossValidationRadioJ48)
                        .addGap(20, 20, 20)))
                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                     .addComponent(percentageSplitFieldJ48, javax.swing.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
                     .addComponent(numberOfFoldsFieldJ48))
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addComponent(percentLabel)
                     .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(foldsLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(okButtonGenerateModelJ48, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)))))
            .addGap(79, 79, 79))
      );
      jPanel1Layout.setVerticalGroup(
         jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel1Layout.createSequentialGroup()
            .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(trainingSetRadioJ48)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(crossValidationRadioJ48)
               .addComponent(okButtonGenerateModelJ48)
               .addComponent(numberOfFoldsFieldJ48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(foldsLabel))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(percentageSplitRadioJ48)
               .addComponent(percentageSplitFieldJ48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(percentLabel))
            .addGap(0, 0, Short.MAX_VALUE))
      );

      modelsTabs.addTab("J48", jPanel1);

      jTabbedPane3.setPreferredSize(new java.awt.Dimension(754, 451));

      multiLayerOutputTree.setEditable(false);
      multiLayerOutputTree.setBackground(new java.awt.Color(1, 1, 1));
      multiLayerOutputTree.setColumns(20);
      multiLayerOutputTree.setForeground(new java.awt.Color(254, 254, 254));
      multiLayerOutputTree.setRows(5);
      jScrollPane3.setViewportView(multiLayerOutputTree);

      javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
      jPanel6.setLayout(jPanel6Layout);
      jPanel6Layout.setHorizontalGroup(
         jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 746, Short.MAX_VALUE)
      );
      jPanel6Layout.setVerticalGroup(
         jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
      );

      jTabbedPane3.addTab("Model", jPanel6);

      multiLayerOutputEvaluation.setEditable(false);
      multiLayerOutputEvaluation.setBackground(new java.awt.Color(1, 1, 1));
      multiLayerOutputEvaluation.setColumns(20);
      multiLayerOutputEvaluation.setForeground(new java.awt.Color(254, 254, 254));
      multiLayerOutputEvaluation.setRows(5);
      jScrollPane4.setViewportView(multiLayerOutputEvaluation);

      javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
      jPanel7.setLayout(jPanel7Layout);
      jPanel7Layout.setHorizontalGroup(
         jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 746, Short.MAX_VALUE)
      );
      jPanel7Layout.setVerticalGroup(
         jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
      );

      jTabbedPane3.addTab("Evaluation", jPanel7);

      okButtonGenerateModelPerceptron.setText("Ok");
      okButtonGenerateModelPerceptron.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            okButtonGenerateModelPerceptronActionPerformed(evt);
         }
      });

      trainingSetRadioPerceptron.setText("Using Training Set");
      trainingSetRadioPerceptron.addMouseListener(new java.awt.event.MouseAdapter()
      {
         public void mouseClicked(java.awt.event.MouseEvent evt)
         {
            trainingSetRadioPerceptronMouseClicked(evt);
         }
      });
      trainingSetRadioPerceptron.addItemListener(new java.awt.event.ItemListener()
      {
         public void itemStateChanged(java.awt.event.ItemEvent evt)
         {
            trainingSetRadioPerceptronItemStateChanged(evt);
         }
      });

      crossValidationRadioPerceptron.setText("Cross-Validation");
      crossValidationRadioPerceptron.addMouseListener(new java.awt.event.MouseAdapter()
      {
         public void mouseClicked(java.awt.event.MouseEvent evt)
         {
            crossValidationRadioPerceptronMouseClicked(evt);
         }
      });
      crossValidationRadioPerceptron.addItemListener(new java.awt.event.ItemListener()
      {
         public void itemStateChanged(java.awt.event.ItemEvent evt)
         {
            crossValidationRadioPerceptronItemStateChanged(evt);
         }
      });

      percentageSplitRadioPerceptron.setText("Percentage Split");
      percentageSplitRadioPerceptron.addMouseListener(new java.awt.event.MouseAdapter()
      {
         public void mouseClicked(java.awt.event.MouseEvent evt)
         {
            percentageSplitRadioPerceptronMouseClicked(evt);
         }
      });
      percentageSplitRadioPerceptron.addItemListener(new java.awt.event.ItemListener()
      {
         public void itemStateChanged(java.awt.event.ItemEvent evt)
         {
            percentageSplitRadioPerceptronItemStateChanged(evt);
         }
      });

      numberOfFoldsFieldPerceptron.setEnabled(false);
      numberOfFoldsFieldPerceptron.addKeyListener(new java.awt.event.KeyAdapter()
      {
         public void keyPressed(java.awt.event.KeyEvent evt)
         {
            numberOfFoldsFieldPerceptronKeyPressed(evt);
         }
         public void keyTyped(java.awt.event.KeyEvent evt)
         {
            numberOfFoldsFieldPerceptronKeyTyped(evt);
         }
      });

      percentageSplitFieldPerceptron.setEnabled(false);
      percentageSplitFieldPerceptron.addKeyListener(new java.awt.event.KeyAdapter()
      {
         public void keyPressed(java.awt.event.KeyEvent evt)
         {
            percentageSplitFieldPerceptronKeyPressed(evt);
         }
         public void keyTyped(java.awt.event.KeyEvent evt)
         {
            percentageSplitFieldPerceptronKeyTyped(evt);
         }
      });

      jLabel1.setText("Folds");

      jLabel2.setText("%");

      javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
      jPanel5.setLayout(jPanel5Layout);
      jPanel5Layout.setHorizontalGroup(
         jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jTabbedPane3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
         .addGroup(jPanel5Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(jPanel5Layout.createSequentialGroup()
                  .addComponent(trainingSetRadioPerceptron)
                  .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
               .addGroup(jPanel5Layout.createSequentialGroup()
                  .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addComponent(crossValidationRadioPerceptron)
                     .addComponent(percentageSplitRadioPerceptron))
                  .addGap(18, 18, 18)
                  .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(percentageSplitFieldPerceptron, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                     .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(numberOfFoldsFieldPerceptron, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(okButtonGenerateModelPerceptron, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(79, 79, 79))))))
      );
      jPanel5Layout.setVerticalGroup(
         jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel5Layout.createSequentialGroup()
            .addComponent(jTabbedPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(trainingSetRadioPerceptron)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(okButtonGenerateModelPerceptron)
               .addComponent(crossValidationRadioPerceptron)
               .addComponent(numberOfFoldsFieldPerceptron, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(jLabel1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(percentageSplitRadioPerceptron)
               .addComponent(percentageSplitFieldPerceptron, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(jLabel2))
            .addGap(3, 3, 3))
      );

      modelsTabs.addTab("Multi Layer Perceptron", jPanel5);

      jTabbedPane4.setPreferredSize(new java.awt.Dimension(754, 451));

      bayesianLogisticRegressionOutputTree.setEditable(false);
      bayesianLogisticRegressionOutputTree.setBackground(new java.awt.Color(1, 1, 1));
      bayesianLogisticRegressionOutputTree.setColumns(20);
      bayesianLogisticRegressionOutputTree.setForeground(new java.awt.Color(254, 254, 254));
      bayesianLogisticRegressionOutputTree.setRows(5);
      jScrollPane5.setViewportView(bayesianLogisticRegressionOutputTree);

      javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
      jPanel9.setLayout(jPanel9Layout);
      jPanel9Layout.setHorizontalGroup(
         jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 746, Short.MAX_VALUE)
      );
      jPanel9Layout.setVerticalGroup(
         jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
      );

      jTabbedPane4.addTab("Model", jPanel9);

      bayesianLogisticRegressionOutputEvaluation.setEditable(false);
      bayesianLogisticRegressionOutputEvaluation.setBackground(new java.awt.Color(1, 1, 1));
      bayesianLogisticRegressionOutputEvaluation.setColumns(20);
      bayesianLogisticRegressionOutputEvaluation.setForeground(new java.awt.Color(255, 255, 255));
      bayesianLogisticRegressionOutputEvaluation.setRows(5);
      jScrollPane6.setViewportView(bayesianLogisticRegressionOutputEvaluation);

      javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
      jPanel10.setLayout(jPanel10Layout);
      jPanel10Layout.setHorizontalGroup(
         jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 746, Short.MAX_VALUE)
      );
      jPanel10Layout.setVerticalGroup(
         jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
      );

      jTabbedPane4.addTab("Evaluation", jPanel10);

      trainingSetRadioBayes.setText("Using Training Set");
      trainingSetRadioBayes.addMouseListener(new java.awt.event.MouseAdapter()
      {
         public void mouseClicked(java.awt.event.MouseEvent evt)
         {
            trainingSetRadioBayesMouseClicked(evt);
         }
      });
      trainingSetRadioBayes.addItemListener(new java.awt.event.ItemListener()
      {
         public void itemStateChanged(java.awt.event.ItemEvent evt)
         {
            trainingSetRadioBayesItemStateChanged(evt);
         }
      });

      crossValidationRadioBayes.setText("Cross-Validation");
      crossValidationRadioBayes.addMouseListener(new java.awt.event.MouseAdapter()
      {
         public void mouseClicked(java.awt.event.MouseEvent evt)
         {
            crossValidationRadioBayesMouseClicked(evt);
         }
      });
      crossValidationRadioBayes.addItemListener(new java.awt.event.ItemListener()
      {
         public void itemStateChanged(java.awt.event.ItemEvent evt)
         {
            crossValidationRadioBayesItemStateChanged(evt);
         }
      });

      percentageSplitRadioBayes.setText("Percentage Split");
      percentageSplitRadioBayes.addMouseListener(new java.awt.event.MouseAdapter()
      {
         public void mouseClicked(java.awt.event.MouseEvent evt)
         {
            percentageSplitRadioBayesMouseClicked(evt);
         }
      });
      percentageSplitRadioBayes.addItemListener(new java.awt.event.ItemListener()
      {
         public void itemStateChanged(java.awt.event.ItemEvent evt)
         {
            percentageSplitRadioBayesItemStateChanged(evt);
         }
      });

      numberOfFoldsFieldBayes.setEnabled(false);
      numberOfFoldsFieldBayes.addKeyListener(new java.awt.event.KeyAdapter()
      {
         public void keyPressed(java.awt.event.KeyEvent evt)
         {
            numberOfFoldsFieldBayesKeyPressed(evt);
         }
         public void keyTyped(java.awt.event.KeyEvent evt)
         {
            numberOfFoldsFieldBayesKeyTyped(evt);
         }
      });

      percentageSplitFieldBayes.setEnabled(false);
      percentageSplitFieldBayes.addKeyListener(new java.awt.event.KeyAdapter()
      {
         public void keyPressed(java.awt.event.KeyEvent evt)
         {
            percentageSplitFieldBayesKeyPressed(evt);
         }
         public void keyTyped(java.awt.event.KeyEvent evt)
         {
            percentageSplitFieldBayesKeyTyped(evt);
         }
      });

      jLabel3.setText("Folds");

      jLabel4.setText("%");

      okButtonGenerateModelBayes.setText("Ok");
      okButtonGenerateModelBayes.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            okButtonGenerateModelBayesActionPerformed(evt);
         }
      });

      javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
      jPanel8.setLayout(jPanel8Layout);
      jPanel8Layout.setHorizontalGroup(
         jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jTabbedPane4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
         .addGroup(jPanel8Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(jPanel8Layout.createSequentialGroup()
                  .addComponent(trainingSetRadioBayes)
                  .addGap(0, 0, Short.MAX_VALUE))
               .addGroup(jPanel8Layout.createSequentialGroup()
                  .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addComponent(percentageSplitRadioBayes)
                     .addComponent(crossValidationRadioBayes))
                  .addGap(18, 18, 18)
                  .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                     .addComponent(numberOfFoldsFieldBayes, javax.swing.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
                     .addComponent(percentageSplitFieldBayes))
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(okButtonGenerateModelBayes, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(79, 79, 79))
                     .addComponent(jLabel4)))))
      );
      jPanel8Layout.setVerticalGroup(
         jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel8Layout.createSequentialGroup()
            .addComponent(jTabbedPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(trainingSetRadioBayes)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(crossValidationRadioBayes)
               .addComponent(numberOfFoldsFieldBayes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(jLabel3)
               .addComponent(okButtonGenerateModelBayes))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(percentageSplitRadioBayes)
               .addComponent(percentageSplitFieldBayes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(jLabel4))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
      );

      modelsTabs.addTab("Bayesian Logistic Regression", jPanel8);

      javax.swing.GroupLayout panelGenerateModelLayout = new javax.swing.GroupLayout(panelGenerateModel);
      panelGenerateModel.setLayout(panelGenerateModelLayout);
      panelGenerateModelLayout.setHorizontalGroup(
         panelGenerateModelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(modelsTabs)
      );
      panelGenerateModelLayout.setVerticalGroup(
         panelGenerateModelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelGenerateModelLayout.createSequentialGroup()
            .addComponent(modelsTabs, javax.swing.GroupLayout.PREFERRED_SIZE, 589, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(139, Short.MAX_VALUE))
      );

      tabbedPane.addTab("Generate Model", panelGenerateModel);

      chooseModelButton.setText("Choose model ...");
      chooseModelButton.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            chooseModelButtonActionPerformed(evt);
         }
      });

      choosenModelPath.setEditable(false);

      URLLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      URLLabel.setText("URL");

      classifyDataButton.setText("Classify Data");
      classifyDataButton.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            classifyDataButtonActionPerformed(evt);
         }
      });

      getMetricsButton.setText("Get Metrics");
      getMetricsButton.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            getMetricsButtonActionPerformed(evt);
         }
      });

      crawledMetricsTable.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      crawledMetricsTable.setModel(new javax.swing.table.DefaultTableModel(
         new Object [][]
         {

         },
         new String []
         {
            "Metrics", "Values"
         }
      )
      {
         Class[] types = new Class []
         {
            java.lang.String.class, java.lang.Double.class
         };
         boolean[] canEdit = new boolean []
         {
            false, false
         };

         public Class getColumnClass(int columnIndex)
         {
            return types [columnIndex];
         }

         public boolean isCellEditable(int rowIndex, int columnIndex)
         {
            return canEdit [columnIndex];
         }
      });
      jScrollPane2.setViewportView(crawledMetricsTable);
      if (crawledMetricsTable.getColumnModel().getColumnCount() > 0)
      {
         crawledMetricsTable.getColumnModel().getColumn(0).setMinWidth(235);
         crawledMetricsTable.getColumnModel().getColumn(0).setPreferredWidth(235);
         crawledMetricsTable.getColumnModel().getColumn(0).setMaxWidth(235);
         crawledMetricsTable.getColumnModel().getColumn(1).setResizable(false);
      }

      classyfiedObjectLabel.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      classyfiedObjectLabel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
      jPanel4.setLayout(jPanel4Layout);
      jPanel4Layout.setHorizontalGroup(
         jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
            .addGap(24, 24, 24)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
               .addGroup(jPanel4Layout.createSequentialGroup()
                  .addGap(0, 0, Short.MAX_VALUE)
                  .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                     .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(classyfiedObjectLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                     .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(getMetricsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(classifyDataButton))))
               .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                  .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                     .addComponent(chooseModelButton)
                     .addComponent(URLLabel))
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                  .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE)
                     .addComponent(URLPath)
                     .addComponent(choosenModelPath))))
            .addGap(169, 169, 169))
      );
      jPanel4Layout.setVerticalGroup(
         jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel4Layout.createSequentialGroup()
            .addGap(22, 22, 22)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(chooseModelButton)
               .addComponent(choosenModelPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(27, 27, 27)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(URLPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(URLLabel))
            .addGap(26, 26, 26)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(classifyDataButton)
               .addComponent(getMetricsButton))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
               .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
               .addComponent(classyfiedObjectLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(103, Short.MAX_VALUE))
      );

      tabbedPane.addTab("Use Model", jPanel4);

      panelQualityPerspective.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Quality Perspective", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 13), new java.awt.Color(1, 1, 1))); // NOI18N
      panelQualityPerspective.addMouseListener(new java.awt.event.MouseAdapter()
      {
         public void mouseClicked(java.awt.event.MouseEvent evt)
         {
            panelQualityPerspectiveMouseClicked(evt);
         }
      });

      personalCollectionsRadio.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      personalCollectionsRadio.setText("Personal Collections");
      personalCollectionsRadio.addMouseListener(new java.awt.event.MouseAdapter()
      {
         public void mouseClicked(java.awt.event.MouseEvent evt)
         {
            personalCollectionsRadioMouseClicked(evt);
         }
      });
      personalCollectionsRadio.addItemListener(new java.awt.event.ItemListener()
      {
         public void itemStateChanged(java.awt.event.ItemEvent evt)
         {
            personalCollectionsRadioItemStateChanged(evt);
         }
      });

      expertsRadio.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      expertsRadio.setText("Experts");
      expertsRadio.addMouseListener(new java.awt.event.MouseAdapter()
      {
         public void mouseClicked(java.awt.event.MouseEvent evt)
         {
            expertsRadioMouseClicked(evt);
         }
      });
      expertsRadio.addItemListener(new java.awt.event.ItemListener()
      {
         public void itemStateChanged(java.awt.event.ItemEvent evt)
         {
            expertsRadioItemStateChanged(evt);
         }
      });

      usersRadio.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      usersRadio.setText("Users");
      usersRadio.addMouseListener(new java.awt.event.MouseAdapter()
      {
         public void mouseClicked(java.awt.event.MouseEvent evt)
         {
            usersRadioMouseClicked(evt);
         }
      });
      usersRadio.addItemListener(new java.awt.event.ItemListener()
      {
         public void itemStateChanged(java.awt.event.ItemEvent evt)
         {
            usersRadioItemStateChanged(evt);
         }
      });

      javax.swing.GroupLayout panelQualityPerspectiveLayout = new javax.swing.GroupLayout(panelQualityPerspective);
      panelQualityPerspective.setLayout(panelQualityPerspectiveLayout);
      panelQualityPerspectiveLayout.setHorizontalGroup(
         panelQualityPerspectiveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelQualityPerspectiveLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(panelQualityPerspectiveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(expertsRadio)
               .addComponent(personalCollectionsRadio)
               .addComponent(usersRadio))
            .addContainerGap(142, Short.MAX_VALUE))
      );
      panelQualityPerspectiveLayout.setVerticalGroup(
         panelQualityPerspectiveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelQualityPerspectiveLayout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(usersRadio)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(expertsRadio)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(personalCollectionsRadio)
            .addContainerGap())
      );

      panelCategory.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Category", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 13))); // NOI18N
      panelCategory.setMinimumSize(new java.awt.Dimension(256, 175));
      panelCategory.setName(""); // NOI18N
      panelCategory.setPreferredSize(new java.awt.Dimension(256, 175));
      panelCategory.setLayout(new javax.swing.BoxLayout(panelCategory, javax.swing.BoxLayout.LINE_AXIS));

      scrollPaneCategorias.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

      categoriesList.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      categoriesList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
      categoriesList.addListSelectionListener(new javax.swing.event.ListSelectionListener()
      {
         public void valueChanged(javax.swing.event.ListSelectionEvent evt)
         {
            categoriesListValueChanged(evt);
         }
      });
      scrollPaneCategorias.setViewportView(categoriesList);

      panelCategory.add(scrollPaneCategorias);

      panelMaterialType.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Material Type", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 13))); // NOI18N
      panelMaterialType.setMinimumSize(new java.awt.Dimension(256, 175));
      panelMaterialType.setPreferredSize(new java.awt.Dimension(256, 175));
      panelMaterialType.setLayout(new javax.swing.BoxLayout(panelMaterialType, javax.swing.BoxLayout.LINE_AXIS));

      scrollPaneMaterialType.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

      materialTypeList.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
      materialTypeList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
      materialTypeList.addListSelectionListener(new javax.swing.event.ListSelectionListener()
      {
         public void valueChanged(javax.swing.event.ListSelectionEvent evt)
         {
            materialTypeListValueChanged(evt);
         }
      });
      scrollPaneMaterialType.setViewportView(materialTypeList);

      panelMaterialType.add(scrollPaneMaterialType);

      cancelButton.setText("Cancel");
      cancelButton.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            cancelButtonActionPerformed(evt);
         }
      });

      okButton.setText("Ok");
      okButton.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            okButtonActionPerformed(evt);
         }
      });

      fileMenu.setText("File");

      saveModel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gui/save.png"))); // NOI18N
      saveModel.setText("Save Model");
      saveModel.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            saveModelActionPerformed(evt);
         }
      });
      fileMenu.add(saveModel);

      menuBar.add(fileMenu);

      editMenu.setText("Edit");

      selectAllMetricsMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
      selectAllMetricsMenu.setText("Select All Metrics");
      selectAllMetricsMenu.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            selectAllMetricsMenuActionPerformed(evt);
         }
      });
      editMenu.add(selectAllMetricsMenu);

      selectRelevantMetricsMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
      selectRelevantMetricsMenu.setText("Select Relevant Metrics");
      selectRelevantMetricsMenu.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            selectRelevantMetricsMenuActionPerformed(evt);
         }
      });
      editMenu.add(selectRelevantMetricsMenu);

      deselectAllMetricsMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
      deselectAllMetricsMenu.setText("Deselect All Metrics");
      deselectAllMetricsMenu.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            deselectAllMetricsMenuActionPerformed(evt);
         }
      });
      editMenu.add(deselectAllMetricsMenu);

      menuBar.add(editMenu);

      setJMenuBar(menuBar);

      javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
      getContentPane().setLayout(layout);
      layout.setHorizontalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                  .addComponent(panelQualityPerspective, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panelCategory, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
               .addGroup(layout.createSequentialGroup()
                  .addGap(61, 61, 61)
                  .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(18, 18, 18)
                  .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
               .addComponent(panelMaterialType, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(20, 20, 20)
            .addComponent(tabbedPane))
      );
      layout.setVerticalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(tabbedPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addGroup(layout.createSequentialGroup()
                  .addComponent(panelQualityPerspective, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                  .addComponent(panelCategory, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                  .addComponent(panelMaterialType, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(28, 28, 28)
                  .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                     .addComponent(okButton)
                     .addComponent(cancelButton))))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
      );

      pack();
   }// </editor-fold>//GEN-END:initComponents

   private void okButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_okButtonActionPerformed
   {//GEN-HEADEREND:event_okButtonActionPerformed
      /* recebe as notas dadas por especialistas ou usuários ou o número de coleções pessoais */
      double[] notesOrPersonalCollectionsNumber;
      /* 
       * recebe os tercis calculados a partir das notas dos especialistas ou usuários ou o número
       * de coleções pessoais
       */
      double[] tertile;
      /* recebe o resultado do calculo da medina do grupo dos objetos bons para uma determinada metrica */
      double medianGood;
      /* recebe o resultado do calculo da medina do grupo dos objetos não bons para uma determinada metrica */
      double medianNotGood;
      double[] goodVector;
      double[] notGoodVector;
      boolean[] influence;
      
      grupos = null;
      indices.clear();
      readedFromModel = false;
      components.clearMetricsFromTable(model);
      j48outputTree.setText(null);
      j48outputEvaluation.setText(null);
      mannWhitneyW.clear();
      mannWhitneyP.clear();
      kolmogorovD.clear();
      kolmogorovP.clear();
      metricsNames.clear();

      notesOrPersonalCollectionsNumber = utils.extractDataForTheTertileCalculus(categoriesList, materialTypeList,
                                                                                usersRadio, expertsRadio, personalCollectionsRadio);
      
      tertile = statistic.calculateTertile(notesOrPersonalCollectionsNumber);

      /* 
       * se o método showTertileValues retornar false significa que não existem objetos para serem avaliados e então
       * é mostrada a seguinte mensagem: "Não existem objetos para serem avaliados"
       */
      if(components.showTertileValues(tertile, goodThreshold, notGoodThreshold))
      {
         if(usersRadio.isSelected())
         {
            statistic.createAndSaveRGraphic(notesOrPersonalCollectionsNumber, tertile, notesOrPersonalCollectionsNumber[0], 
                                            notesOrPersonalCollectionsNumber[notesOrPersonalCollectionsNumber.length - 1], 
                                            usersRadio.getText());
         }
         else
         {
            if(expertsRadio.isSelected())
            {
               statistic.createAndSaveRGraphic(notesOrPersonalCollectionsNumber, tertile, notesOrPersonalCollectionsNumber[0], 
                                               notesOrPersonalCollectionsNumber[notesOrPersonalCollectionsNumber.length - 1], 
                                               expertsRadio.getText());
            }
            else
            {
               statistic.createAndSaveRGraphic(notesOrPersonalCollectionsNumber, tertile, notesOrPersonalCollectionsNumber[0], 
                                               notesOrPersonalCollectionsNumber[notesOrPersonalCollectionsNumber.length - 1], 
                                               personalCollectionsRadio.getText());
            }
         }

         grupos = utils.separateObjectsIntoGroups(tertile, categoriesList, materialTypeList, usersRadio, 
                                                  expertsRadio, personalCollectionsRadio/*, expertsNotesComboBox*/);
         
         /*
          * se não existir pelo menos um objeto em cada grupo (bons e não bons) é mostrada uma mensagem que não há objetos 
          * suficientes para uma análise do subgrupo selecionado
          */
         if(grupos.get(0).size() > 0 && grupos.get(1).size() > 0)
         {
            /* 
             * recebe o valor das métricas do grupo dos objetos bons (uma métrica de cada vez de todos os objetos bons
             * ex: recebe o valor da metrica links_number de todos os objetos bons, depois recebe o valor da metrica
             * internal_links de todos os objetos bons e assim por diante)
             */
            goodVector = new double[grupos.get(0).size()];
            /* 
             * recebe o valor das métricas do grupo dos objetos não bons (uma métrica de cada vez de todos os objetos não bons
             * ex: recebe o valor da metrica links_number de todos os objetos não bons, depois recebe o valor da metrica
             * internal_links de todos os objetos não bons e assim por diante)
             */
            notGoodVector = new double[grupos.get(1).size()];
            /* 
             * influence recebe o número de metricas menos um pois o ultimo elemento é o page error que não
             * é uma metrica
             */
            influence = new boolean[grupos.get(0).get(0).getNameOfMetricsSize() - 1];

            /* 
             * este for vai até o numero de metricas menos um pois o ultimo elemento é o page error que não
             * é uma metrica
             */
            for(int x = 0; x < grupos.get(0).get(0).getNameOfMetricsSize() - 1; x++)
            {
               /* adiciona o nome da metrica para depois recuperar na hora de adicionar na tabela de metricas */
               metricsNames.add(grupos.get(0).get(0).getNameOfMetric(x));

               for(int i = 0; i < grupos.get(0).size(); i++)
               {
                  goodVector[i] = grupos.get(0).get(i).getMetrics(x);
               }
               
               for(int i = 0; i < grupos.get(1).size(); i++)
               {
                  notGoodVector[i] = grupos.get(1).get(i).getMetrics(x);
               }
               
               /* faz o calculo da estatistica de mannWhitney do grupo dos objetos bons e não bons */
               mannWhitneyResult = statistic.mannWhitney(goodVector, notGoodVector);
               /* adiciona na lista o valor do W calculado pelo metodo de mannWhitney para a metrica */
               mannWhitneyW.add(mannWhitneyResult[0]);
               /* adiciona na lista o valor do P calculado pelo metodo de mannWhitney para a metrica */
               mannWhitneyP.add(mannWhitneyResult[1]);
               /* faz o calculo da estatistica de kolmogorovSmirnov do grupo dos objetos bons e não bons */
               kolmogorovResult = statistic.kolmogorovSmirnov(goodVector, notGoodVector);
               /* adiciona na lista o valor do D calculado pelo metodo de kolmogorovSmirnov para a metrica */
               kolmogorovD.add(kolmogorovResult[0]);
               /* adiciona na lista o valor do P calculado pelo metodo de kolmogorovSmirnov para a metrica */
               kolmogorovP.add(kolmogorovResult[1]);

               medianGood = statistic.calculateMedian(goodVector);
               medianNotGood = statistic.calculateMedian(notGoodVector);
               influence[x] = utils.calculateInfluence(medianGood, medianNotGood);
            }

            kolmogorovValues.add(0, kolmogorovD); //D
            kolmogorovValues.add(1, kolmogorovP); //p
            mannWhitneyValues.add(0, mannWhitneyW); //W
            mannWhitneyValues.add(1, mannWhitneyP); //p

            components.plotGraphic(graphicJLabel);

            components.addNameOfTheMetricsInTheTable(metricsNames, model);

            indicators = new int[mannWhitneyValues.get(1).size()];

            for(int i = 0; i < mannWhitneyValues.get(1).size(); i++)
            {
               /* 
                * o vetor indicators recebe os valores definidos no metodo calculateIndicator da classe Utils que indicam 
                * se a metrica é um indicador de qualidade, indicador de qualidade parcial ou não é um indicador de qualidade
                */
               indicators[i] = utils.calculateIndicator(mannWhitneyValues.get(1).get(i), kolmogorovValues.get(1).get(i));
            }

            components.addIndicatorInTheMetricsTable(model, indicators, indices);
            components.addInfluenceInTheMetricsTable(model, influence);
            
            modelTable = metricsTable.getColumnModel();
            
            modelTable.getColumn(0).setCellRenderer(new MetricsNamesTableRenderer());
            modelTable.getColumn(1).setCellRenderer(new IndicatorAndInfluenceTableRenderer());
            modelTable.getColumn(2).setCellRenderer(new IndicatorAndInfluenceTableRenderer());
            JCheckBoxTableRenderer checkBox = new JCheckBoxTableRenderer();
            modelTable.getColumn(3).setCellRenderer(checkBox);
            
            //okButtonGenerateModel.setEnabled(true);
            selectAllMetricsMenu.setEnabled(true);
            deselectAllMetricsMenu.setEnabled(true);
            selectRelevantMetricsMenu.setEnabled(true);
         }
         else
         {
            JOptionPane.showMessageDialog(null, "There are not enough objects to an analysis of the selected subgroup", 
                                          "Warning", JOptionPane.WARNING_MESSAGE);
         }
         okButton.setFocusPainted(false);
      }
      else
      {
         JOptionPane.showMessageDialog(null, "There are no objects to be evaluated", "Error", JOptionPane.ERROR_MESSAGE);
         okButton.setFocusPainted(false);
      }
   }//GEN-LAST:event_okButtonActionPerformed

   private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cancelButtonActionPerformed
   {//GEN-HEADEREND:event_cancelButtonActionPerformed
      components.cleanDefaultListModel(categoryModel);
      components.cleanDefaultListModel(materialTypeModel);
      qualityPerspectiveButtonGroup.clearSelection();
      okButton.setEnabled(false);
      components.loadInicialData(categoryModel, materialTypeModel, userRatedNumber, peerReviewRatedNumber, personalCollectionsNumber);
      graphicJLabel.setIcon(null);
      components.clearJlabels(goodThreshold, notGoodThreshold, minimumValue, maximumValue, meanValue, 
                              medianValue, wValue, pValuemannWhitney, dValue, pValueKolmogorov);
      cancelButton.setFocusPainted(false);
      userRatedNumber.setBackground(new Color(242,241,240));
      peerReviewRatedNumber.setBackground(new Color(242,241,240));
      personalCollectionsNumber.setBackground(new Color(242,241,240));
      readedFromModel = false;
      components.clearMetricsFromTable(model);
      indices.clear();
      j48outputTree.setText(null);
      bayesianLogisticRegressionOutputTree.setText(null);
      multiLayerOutputTree.setText(null);
      j48outputEvaluation.setText(null);
      bayesianLogisticRegressionOutputEvaluation.setText(null);
      multiLayerOutputEvaluation.setText(null);
      okButtonGenerateModelJ48.setEnabled(false);
      okButtonGenerateModelBayes.setEnabled(false);
      okButtonGenerateModelPerceptron.setEnabled(false);
      saveModel.setEnabled(false);
      selectAllMetricsMenu.setEnabled(false);
      deselectAllMetricsMenu.setEnabled(false);
      selectRelevantMetricsMenu.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
   }//GEN-LAST:event_cancelButtonActionPerformed

   private void materialTypeListValueChanged(javax.swing.event.ListSelectionEvent evt)//GEN-FIRST:event_materialTypeListValueChanged
   {//GEN-HEADEREND:event_materialTypeListValueChanged
      components.updateNumberOfObjects(categoriesList, materialTypeList, usersRadio, 
                                       expertsRadio, personalCollectionsRadio/*, expertsNotesComboBox*/);
      graphicJLabel.setIcon(null);
      components.clearJlabels(goodThreshold, notGoodThreshold, minimumValue, maximumValue, meanValue, 
                              medianValue, wValue, pValuemannWhitney, dValue, pValueKolmogorov);
      readedFromModel = false;
      components.clearMetricsFromTable(model);
      indices.clear();
      j48outputTree.setText(null);
      j48outputEvaluation.setText(null);
      okButtonGenerateModelJ48.setEnabled(false);
      saveModel.setEnabled(false);
      selectAllMetricsMenu.setEnabled(false);
      deselectAllMetricsMenu.setEnabled(false);
      selectRelevantMetricsMenu.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
   }//GEN-LAST:event_materialTypeListValueChanged

   private void categoriesListValueChanged(javax.swing.event.ListSelectionEvent evt)//GEN-FIRST:event_categoriesListValueChanged
   {//GEN-HEADEREND:event_categoriesListValueChanged
      components.updateDefaultListModelOfMaterialTypes(materialTypeModel, categoriesList, materialTypeList, usersRadio, 
                                                       expertsRadio, personalCollectionsRadio/*, expertsNotesComboBox*/);
      graphicJLabel.setIcon(null);
      components.clearJlabels(goodThreshold, notGoodThreshold, minimumValue, maximumValue, meanValue, 
                              medianValue, wValue, pValuemannWhitney, dValue, pValueKolmogorov);
      readedFromModel = false;
      components.clearMetricsFromTable(model);
      indices.clear();
      j48outputTree.setText(null);
      j48outputEvaluation.setText(null);
      okButtonGenerateModelJ48.setEnabled(false);
      saveModel.setEnabled(false);
      selectAllMetricsMenu.setEnabled(false);
      deselectAllMetricsMenu.setEnabled(false);
      selectRelevantMetricsMenu.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
   }//GEN-LAST:event_categoriesListValueChanged

   private void panelQualityPerspectiveMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_panelQualityPerspectiveMouseClicked
   {//GEN-HEADEREND:event_panelQualityPerspectiveMouseClicked
      qualityPerspectiveButtonGroup.clearSelection();
      usersRadio.setFocusPainted(false);
      expertsRadio.setFocusPainted(false);
      personalCollectionsRadio.setFocusPainted(false);
      okButton.setEnabled(false);
      components.updateNumberOfObjects(categoriesList, materialTypeList, usersRadio, expertsRadio, 
                                       personalCollectionsRadio/*, expertsNotesComboBox*/);
      graphicJLabel.setIcon(null);
      components.clearJlabels(goodThreshold, notGoodThreshold, minimumValue, maximumValue, meanValue, 
                              medianValue, wValue, pValuemannWhitney, dValue, pValueKolmogorov);
      userRatedNumber.setBackground(new Color(242,241,240));
      peerReviewRatedNumber.setBackground(new Color(242,241,240));
      personalCollectionsNumber.setBackground(new Color(242,241,240));
      readedFromModel = false;
      components.clearMetricsFromTable(model);
      indices.clear();
      j48outputTree.setText(null);
      j48outputEvaluation.setText(null);
      okButtonGenerateModelJ48.setEnabled(false);
      saveModel.setEnabled(false);
      selectAllMetricsMenu.setEnabled(false);
      deselectAllMetricsMenu.setEnabled(false);
      selectRelevantMetricsMenu.setEnabled(false);
   }//GEN-LAST:event_panelQualityPerspectiveMouseClicked

   private void usersRadioItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_usersRadioItemStateChanged
   {//GEN-HEADEREND:event_usersRadioItemStateChanged
      graphicJLabel.setIcon(null);
      components.clearJlabels(goodThreshold, notGoodThreshold, minimumValue, maximumValue, meanValue, 
                              medianValue, wValue, pValuemannWhitney, dValue, pValueKolmogorov);
      readedFromModel = false;
      components.clearMetricsFromTable(model);
      indices.clear();
      j48outputTree.setText(null);
      j48outputEvaluation.setText(null);
      okButtonGenerateModelJ48.setEnabled(false);
      saveModel.setEnabled(false);
      selectAllMetricsMenu.setEnabled(false);
      deselectAllMetricsMenu.setEnabled(false);
      selectRelevantMetricsMenu.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
      if(usersRadio.isSelected())
      {
         userRatedNumber.setBackground(new Color(185,211,238));
         peerReviewRatedNumber.setBackground(new Color(242,241,240));
         personalCollectionsNumber.setBackground(new Color(242,241,240));
      }
      else
      {
         userRatedNumber.setBackground(new Color(242,241,240));
         peerReviewRatedNumber.setBackground(new Color(242,241,240));
         personalCollectionsNumber.setBackground(new Color(242,241,240));
      }
   }//GEN-LAST:event_usersRadioItemStateChanged

   private void usersRadioMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_usersRadioMouseClicked
   {//GEN-HEADEREND:event_usersRadioMouseClicked
      components.updateNumberOfObjects(categoriesList, materialTypeList, usersRadio, expertsRadio, 
                                       personalCollectionsRadio/*, expertsNotesComboBox*/);
      okButton.setEnabled(true);
      userRatedNumber.setOpaque(true);
   }//GEN-LAST:event_usersRadioMouseClicked

   private void expertsRadioItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_expertsRadioItemStateChanged
   {//GEN-HEADEREND:event_expertsRadioItemStateChanged
      graphicJLabel.setIcon(null);
      components.clearJlabels(goodThreshold, notGoodThreshold, minimumValue, maximumValue, meanValue, 
                              medianValue, wValue, pValuemannWhitney, dValue, pValueKolmogorov);
      readedFromModel = false;
      components.clearMetricsFromTable(model);
      indices.clear();
      j48outputTree.setText(null);
      j48outputEvaluation.setText(null);
      okButtonGenerateModelJ48.setEnabled(false);
      saveModel.setEnabled(false);
      selectAllMetricsMenu.setEnabled(false);
      deselectAllMetricsMenu.setEnabled(false);
      selectRelevantMetricsMenu.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
      if(expertsRadio.isSelected())
      {
         peerReviewRatedNumber.setBackground(new Color(185,211,238));
         userRatedNumber.setBackground(new Color(242,241,240));
         personalCollectionsNumber.setBackground(new Color(242,241,240));
      }
      else
      {
         userRatedNumber.setBackground(new Color(242,241,240));
         peerReviewRatedNumber.setBackground(new Color(242,241,240));
         personalCollectionsNumber.setBackground(new Color(242,241,240));
      }
   }//GEN-LAST:event_expertsRadioItemStateChanged

   private void expertsRadioMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_expertsRadioMouseClicked
   {//GEN-HEADEREND:event_expertsRadioMouseClicked
      components.updateNumberOfObjects(categoriesList, materialTypeList, usersRadio, expertsRadio, 
                                       personalCollectionsRadio/*, expertsNotesComboBox*/);
      okButton.setEnabled(true);
      peerReviewRatedNumber.setOpaque(true);
   }//GEN-LAST:event_expertsRadioMouseClicked

   private void personalCollectionsRadioItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_personalCollectionsRadioItemStateChanged
   {//GEN-HEADEREND:event_personalCollectionsRadioItemStateChanged
      graphicJLabel.setIcon(null);
      components.clearJlabels(goodThreshold, notGoodThreshold, minimumValue, maximumValue, meanValue, 
                              medianValue, wValue, pValuemannWhitney, dValue, pValueKolmogorov);
      readedFromModel = false;
      components.clearMetricsFromTable(model);
      indices.clear();
      j48outputTree.setText(null);
      j48outputEvaluation.setText(null);
      okButtonGenerateModelJ48.setEnabled(false);
      saveModel.setEnabled(false);
      selectAllMetricsMenu.setEnabled(false);
      deselectAllMetricsMenu.setEnabled(false);
      selectRelevantMetricsMenu.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
      if(personalCollectionsRadio.isSelected())
      {
         personalCollectionsNumber.setBackground(new Color(185,211,238));
         userRatedNumber.setBackground(new Color(242,241,240));
         peerReviewRatedNumber.setBackground(new Color(242,241,240));
      }
      else
      {
         userRatedNumber.setBackground(new Color(242,241,240));
         peerReviewRatedNumber.setBackground(new Color(242,241,240));
         personalCollectionsNumber.setBackground(new Color(242,241,240));
      }
   }//GEN-LAST:event_personalCollectionsRadioItemStateChanged

   private void personalCollectionsRadioMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_personalCollectionsRadioMouseClicked
   {//GEN-HEADEREND:event_personalCollectionsRadioMouseClicked
      components.updateNumberOfObjects(categoriesList, materialTypeList, usersRadio, expertsRadio, 
                                       personalCollectionsRadio/*, expertsNotesComboBox*/);
      okButton.setEnabled(true);
      personalCollectionsNumber.setOpaque(true);
   }//GEN-LAST:event_personalCollectionsRadioMouseClicked

   private void okButtonGenerateModelJ48ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_okButtonGenerateModelJ48ActionPerformed
   {//GEN-HEADEREND:event_okButtonGenerateModelJ48ActionPerformed
      /* caminho do arquivo arff criado pelo software */
      String archivePath;
      //Evaluation evaluation;
      Instances archiveData;
      
      j48outputTree.setText(null);
      j48outputEvaluation.setText(null);
      usedMetricsOnModel.clear();
      
      for(int i = 0; i < model.getRowCount(); i++)
      {
         if((Boolean)model.getValueAt(i, 3))
         {
            usedMetricsOnModel.add(model.getValueAt(i, 0).toString());
         }
      }
      
      archivePath = utils.createArffFile(grupos, model/*, categorie, materialType, qualityPerspective*/);

      /* 
       * se o caminho do arquivo devolvido pelo metodo createArffFile for null significa que não existem métricas
       * relevantes para definir qualidade no subgrupo selecionado
       */
      if(archivePath == null)
      {
         JOptionPane.showMessageDialog(null, "There are no relevant metrics to define quality", "Warning", 
                                       JOptionPane.WARNING_MESSAGE);
      }
      else
      {
         archiveData = utils.readDataFromArchive(archivePath);
         
         if(percentageSplitRadioJ48.isSelected())
         {
            int percentageSplit = Integer.parseInt(percentageSplitFieldJ48.getText());
            j48 = utils.j48BuildClassifier(archiveData, true, percentageSplit);
            evaluation = utils.j48Evaluation(j48, archiveData, 3, 0);
         }
         else
         {
            if(crossValidationRadioJ48.isSelected())
            {
               j48 = utils.j48BuildClassifier(archiveData, false, 0);
               evaluation = utils.j48Evaluation(j48, archiveData, 2, Integer.parseInt(numberOfFoldsFieldJ48.getText()));
            }
            else
            {
               j48 = utils.j48BuildClassifier(archiveData, false, 0);
               evaluation = utils.j48Evaluation(j48, archiveData, 1, 0);
            }
         }

         j48outputTree.append(j48.toString() + "\n");
         j48outputEvaluation.append("Confidence Factor: " + j48.getConfidenceFactor() + "\n");
         j48outputEvaluation.append("Correct Instances: " + evaluation.correct() + "    " + evaluation.pctCorrect() + "%\n");
         j48outputEvaluation.append("Incorrect Instances: " + evaluation.incorrect() + "    " + evaluation.pctIncorrect() + "%\n");
         j48outputEvaluation.append("Kappa: " + evaluation.kappa() + "\n");
         j48outputEvaluation.append("Mean Absolute Error: " + evaluation.meanAbsoluteError() + "\n");
         j48outputEvaluation.append("F Measure Good: " + evaluation.fMeasure(0) + "\n");
         j48outputEvaluation.append("F Measure Not Good: " + evaluation.fMeasure(1) + "\n");
         j48outputEvaluation.append("Precision Good: " + evaluation.precision(0) + "\n");
         j48outputEvaluation.append("Precision Not Good: " + evaluation.precision(1) + "\n");
         j48outputEvaluation.append("TruePositive Good: " + evaluation.truePositiveRate(0) + "\n");
         j48outputEvaluation.append("TruePositive Not Good: " + evaluation.truePositiveRate(1) + "\n");
         System.out.println(evaluation.numInstances());
         j48outputEvaluation.append("Number of Rules: " + j48.measureNumRules() + "\n\n");

         saveModel.setEnabled(true);

         try
         {
            j48outputEvaluation.append(evaluation.toMatrixString("===== Evaluation Numbers =====\n"));
         }
         catch (Exception ex)
         {
            System.err.println(ex);
         }
      }
      
      okButtonGenerateModelJ48.setFocusPainted(false);
      
      modelAttributes = usedMetricsOnModel;
      URLLabel.setEnabled(true);
      URLPath.setEnabled(true);
      getMetricsButton.setEnabled(true);
   }//GEN-LAST:event_okButtonGenerateModelJ48ActionPerformed

   private void saveModelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_saveModelActionPerformed
   {//GEN-HEADEREND:event_saveModelActionPerformed
      String qualityPerspective = null;
      String category = null;
      String materialType = null;
      String modelType = null;
      
      if(usersRadio.isSelected())
      {
         qualityPerspective = usersRadio.getText();
      }
      else
      {
         if(expertsRadio.isSelected())
         {
            qualityPerspective = expertsRadio.getText();
         }
         else
         {
            qualityPerspective = personalCollectionsRadio.getText();
         }
      }
      
      if(!categoriesList.isSelectionEmpty())
      {
         category = categoriesList.getSelectedValue().toString();
      }
      
      if(!materialTypeList.isSelectionEmpty())
      {
         materialType = materialTypeList.getSelectedValue().toString();
      }
      
      List<String> metricsNamesLocal = new ArrayList<>();
      List<String> metricsIndicators = new ArrayList<>();
      List<String> metricsInfluences = new ArrayList<>();
      
      for(int i = 0; i < model.getRowCount(); i++)
      {
         metricsNamesLocal.add(model.getValueAt(i, 0).toString());
      }
      
      for(int i = 0; i < model.getRowCount(); i++)
      {
         metricsIndicators.add(model.getValueAt(i, 1).toString());
      }
      
      for(int i = 0; i < model.getRowCount(); i++)
      {
         if(model.getValueAt(i, 2) != null)
         {
            char unicode = model.getValueAt(i, 2).toString().charAt(0);
            int unicodeValue = unicode;
            
            if(unicodeValue == 8593)
            {
               metricsInfluences.add("+");
            }
            else
            {
               metricsInfluences.add("-");
            }
         }
         else
         {
            metricsInfluences.add("false");
         }
      }
      
      int selected = modelsTabs.getSelectedIndex();
      
      if(selected == 0)
      {
         modelType = "j48";
         modelPath = utils.saveModel(j48, null, null, evaluation, usedMetricsOnModel, qualityPerspective, category, materialType,
                                    metricsNamesLocal, metricsIndicators, metricsInfluences, modelType);
      }
      else
      {
         if(selected == 1)
         {
            modelType = "perceptron";
            modelPath = utils.saveModel(null, perceptron, null, evaluation, usedMetricsOnModel, qualityPerspective, category, materialType,
                                    metricsNamesLocal, metricsIndicators, metricsInfluences, modelType);
         }
         else
         {
            if(selected == 2)
            {
               modelType = "bayes";
               modelPath = utils.saveModel(null, null, bayes, evaluation, usedMetricsOnModel, qualityPerspective, category, materialType,
                                    metricsNamesLocal, metricsIndicators, metricsInfluences, modelType);
            }
         }
      }
      
      if(modelPath == null)
      {
         JOptionPane.showMessageDialog(null, "Model not saved", "Error", JOptionPane.ERROR_MESSAGE);
      }
   }//GEN-LAST:event_saveModelActionPerformed

   private void chooseModelButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_chooseModelButtonActionPerformed
   {//GEN-HEADEREND:event_chooseModelButtonActionPerformed
      modelFilePath = utils.getModelPath();
      List<String> userSelection = null;
      List<String> metricsName = null;
      List<String> evaluationResults = null;
      int[] metricsIndicators = null;
      boolean[] metricsInfluences = null;
      String confidenceFactor = null;
      String correctInstances = null;
      String correctInstancesPercent = null;
      String incorrectInstances = null;
      String incorrectInstancesPercent = null;
      String kappa = null;
      String meanAbsoluteError = null;
      String fMeasureGood = null;
      String fMeasureNotGood = null;
      String precisionGood = null;
      String precisionNotGood = null;
      String truePositiveGood = null;
      String truePositiveNotGood = null;
      String numberOfRules = null;
      String confusionMatrixTitle = null;
      String confusionMatrixLinha1 = null;
      String confusionMatrixLinha2 = null;
      String confusionMatrixLinha3 = null;
      String confusionMatrixLinha4 = null;
      String confusionMatrixLinha5 = null;
      String modelType = null;
      String rootMeanSquaredError = null;
      String relativeAbsoluteError = null;
      String rootRelativeSquaredError = null;
      
      if(modelFilePath == null)
      {
         System.err.println("Nenhum modelo escolhido");
         URLLabel.setEnabled(false);
         URLPath.setEnabled(false);
         getMetricsButton.setEnabled(false);
         choosenModelPath.setText(null);
         classifyDataButton.setEnabled(false);
         progressBar.setValue(0);
         progressBar.setStringPainted(false);
         classyfiedObjectLabel.setText(null);
         modelCrawledMetrics.setRowCount(0);
      }
      else
      {
         components.clearMetricsFromTable(model);
         components.clearJlabels(goodThreshold, notGoodThreshold, minimumValue, maximumValue, meanValue, 
                              medianValue, wValue, pValuemannWhitney, dValue, pValueKolmogorov);
         indices.clear();
         graphicJLabel.setIcon(null);
         j48outputTree.setText(null);
         j48outputEvaluation.setText(null);
         multiLayerOutputTree.setText(null);
         multiLayerOutputEvaluation.setText(null);
         bayesianLogisticRegressionOutputTree.setText(null);
         bayesianLogisticRegressionOutputEvaluation.setText(null);
         
         modelType = utils.readModelType(modelFilePath);
         
         if(modelType.equals("j48"))
         {
            j48 = utils.readJ48Model(modelFilePath);
            modelAttributes = utils.readModelAttributes(modelFilePath);
            userSelection = utils.readUserSelection(modelFilePath);
            metricsName = utils.readMetricsNames(modelFilePath);
            metricsIndicators = utils.readMetricsIndicators(modelFilePath);
            metricsInfluences = utils.readMetricsInFluence(modelFilePath);
            evaluationResults = utils.readEvaluationResults(modelFilePath);

            confidenceFactor = evaluationResults.get(0);
            correctInstances = evaluationResults.get(1);
            correctInstancesPercent = evaluationResults.get(2);
            incorrectInstances = evaluationResults.get(3);
            incorrectInstancesPercent = evaluationResults.get(4);
            kappa = evaluationResults.get(5);
            meanAbsoluteError = evaluationResults.get(6);
            fMeasureGood = evaluationResults.get(7);
            fMeasureNotGood = evaluationResults.get(8);
            precisionGood = evaluationResults.get(9);
            precisionNotGood = evaluationResults.get(10);
            truePositiveGood = evaluationResults.get(11);
            truePositiveNotGood = evaluationResults.get(12);
            numberOfRules = evaluationResults.get(13);
            confusionMatrixTitle = evaluationResults.get(14);
            confusionMatrixLinha1 = evaluationResults.get(15);
            confusionMatrixLinha2 = evaluationResults.get(16);
            confusionMatrixLinha3 = evaluationResults.get(17);
            confusionMatrixLinha4 = evaluationResults.get(18);
            confusionMatrixLinha5 = evaluationResults.get(19);
            
            components.selectQualityPerspective(userSelection.get(0), usersRadio, expertsRadio, personalCollectionsRadio);
            components.selectCategory(userSelection.get(1), categoriesList);
            components.selectMaterialType(userSelection.get(2), materialTypeList);

            components.addNameOfTheMetricsInTheTable(metricsName, model);
            components.addIndicatorInTheMetricsTable(model, metricsIndicators, indices);
            components.addInfluenceInTheMetricsTableReadedFromModel(model, metricsInfluences, modelAttributes);

            readedFromModel = true;

            modelTable = metricsTable.getColumnModel();

            modelTable.getColumn(0).setCellRenderer(new MetricsNamesTableRenderer());
            modelTable.getColumn(1).setCellRenderer(new IndicatorAndInfluenceTableRenderer());
            modelTable.getColumn(2).setCellRenderer(new IndicatorAndInfluenceTableRenderer());
            JCheckBoxTableRenderer checkBox = new JCheckBoxTableRenderer();
            modelTable.getColumn(3).setCellRenderer(checkBox);
            components.showModelPath(choosenModelPath, modelFilePath.getAbsolutePath());
            URLLabel.setEnabled(true);
            URLPath.setEnabled(true);
            getMetricsButton.setEnabled(true);
            
            j48outputTree.append(j48.toString() + "\n");
            j48outputEvaluation.append("Confidence Factor: " + confidenceFactor + "\n");
            j48outputEvaluation.append("Correct Instances: " + correctInstances + "    " + correctInstancesPercent + "%\n");
            j48outputEvaluation.append("Incorrect Instances: " + incorrectInstances + "    " + incorrectInstancesPercent + "%\n");
            j48outputEvaluation.append("Kappa: " + kappa + "\n");
            j48outputEvaluation.append("Mean Absolute Error: " + meanAbsoluteError + "\n");
            j48outputEvaluation.append("F Measure Good: " + fMeasureGood + "\n");
            j48outputEvaluation.append("F Measure Not Good: " + fMeasureNotGood + "\n");
            j48outputEvaluation.append("Precision Good: " + precisionGood + "\n");
            j48outputEvaluation.append("Precision Not Good: " + precisionNotGood + "\n");
            j48outputEvaluation.append("TruePositive Good: " + truePositiveGood + "\n");
            j48outputEvaluation.append("TruePositive Not Good: " + truePositiveNotGood + "\n");
            j48outputEvaluation.append("Number of Rules: " + numberOfRules + "\n\n");
            j48outputEvaluation.append(confusionMatrixTitle + "\n");
            j48outputEvaluation.append(confusionMatrixLinha1 + "\n");
            j48outputEvaluation.append(confusionMatrixLinha2 + "\n");
            j48outputEvaluation.append(confusionMatrixLinha3 + "\n");
            j48outputEvaluation.append(confusionMatrixLinha4 + "\n");
            j48outputEvaluation.append(confusionMatrixLinha5 + "\n");
         }
         else
         {
            if(modelType.equals("perceptron"))
            {
               perceptron = utils.readPerceptronModel(modelFilePath);
               
               modelAttributes = utils.readModelAttributes(modelFilePath);
               userSelection = utils.readUserSelection(modelFilePath);
               metricsName = utils.readMetricsNames(modelFilePath);
               metricsIndicators = utils.readMetricsIndicators(modelFilePath);
               metricsInfluences = utils.readMetricsInFluence(modelFilePath);
               evaluationResults = utils.readEvaluationResults(modelFilePath);
               
               correctInstances = evaluationResults.get(0);
               correctInstancesPercent = evaluationResults.get(1);
               incorrectInstances = evaluationResults.get(2);
               incorrectInstancesPercent = evaluationResults.get(3);
               kappa = evaluationResults.get(4);
               meanAbsoluteError = evaluationResults.get(5);
               rootMeanSquaredError = evaluationResults.get(6);
               relativeAbsoluteError = evaluationResults.get(7);
               rootRelativeSquaredError = evaluationResults.get(8);
               
               confusionMatrixTitle = evaluationResults.get(9);
               confusionMatrixLinha1 = evaluationResults.get(10);
               confusionMatrixLinha2 = evaluationResults.get(11);
               confusionMatrixLinha3 = evaluationResults.get(12);
               confusionMatrixLinha4 = evaluationResults.get(13);
               confusionMatrixLinha5 = evaluationResults.get(14);
               
               components.selectQualityPerspective(userSelection.get(0), usersRadio, expertsRadio, personalCollectionsRadio);
               components.selectCategory(userSelection.get(1), categoriesList);
               components.selectMaterialType(userSelection.get(2), materialTypeList);

               components.addNameOfTheMetricsInTheTable(metricsName, model);
               components.addIndicatorInTheMetricsTable(model, metricsIndicators, indices);
               components.addInfluenceInTheMetricsTableReadedFromModel(model, metricsInfluences, modelAttributes);

               readedFromModel = true;

               modelTable = metricsTable.getColumnModel();

               modelTable.getColumn(0).setCellRenderer(new MetricsNamesTableRenderer());
               modelTable.getColumn(1).setCellRenderer(new IndicatorAndInfluenceTableRenderer());
               modelTable.getColumn(2).setCellRenderer(new IndicatorAndInfluenceTableRenderer());
               JCheckBoxTableRenderer checkBox = new JCheckBoxTableRenderer();
               modelTable.getColumn(3).setCellRenderer(checkBox);
               components.showModelPath(choosenModelPath, modelFilePath.getAbsolutePath());
               URLLabel.setEnabled(true);
               URLPath.setEnabled(true);
               getMetricsButton.setEnabled(true);
               
               multiLayerOutputTree.append(perceptron.toString() + "\n");
               multiLayerOutputEvaluation.append("Correct Instances: " + correctInstances + "    " + correctInstancesPercent + "%\n");
               multiLayerOutputEvaluation.append("Incorrect Instances: " + incorrectInstances + "    " + incorrectInstancesPercent + "%\n");
               multiLayerOutputEvaluation.append("Kappa: " + kappa + "\n");
               multiLayerOutputEvaluation.append("Mean Absolute Error: " + meanAbsoluteError + "\n");
               multiLayerOutputEvaluation.append("Root Mean Squared Error: " + rootMeanSquaredError + "\n");
               multiLayerOutputEvaluation.append("Relative absolute error: " + relativeAbsoluteError + "\n");
               multiLayerOutputEvaluation.append("Root relative squared error: " + rootRelativeSquaredError + "\n");
               multiLayerOutputEvaluation.append(confusionMatrixTitle + "\n");
               multiLayerOutputEvaluation.append(confusionMatrixLinha1 + "\n");
               multiLayerOutputEvaluation.append(confusionMatrixLinha2 + "\n");
               multiLayerOutputEvaluation.append(confusionMatrixLinha3 + "\n");
               multiLayerOutputEvaluation.append(confusionMatrixLinha4 + "\n");
               multiLayerOutputEvaluation.append(confusionMatrixLinha5 + "\n");
            }
            else
            {
               if(modelType.equals("bayes"))
               {
                  bayes = utils.readBayesModel(modelFilePath);
               
                  modelAttributes = utils.readModelAttributes(modelFilePath);
                  userSelection = utils.readUserSelection(modelFilePath);
                  metricsName = utils.readMetricsNames(modelFilePath);
                  metricsIndicators = utils.readMetricsIndicators(modelFilePath);
                  metricsInfluences = utils.readMetricsInFluence(modelFilePath);
                  evaluationResults = utils.readEvaluationResults(modelFilePath);

                  correctInstances = evaluationResults.get(0);
                  correctInstancesPercent = evaluationResults.get(1);
                  incorrectInstances = evaluationResults.get(2);
                  incorrectInstancesPercent = evaluationResults.get(3);
                  kappa = evaluationResults.get(4);
                  meanAbsoluteError = evaluationResults.get(5);
                  rootMeanSquaredError = evaluationResults.get(6);
                  relativeAbsoluteError = evaluationResults.get(7);
                  rootRelativeSquaredError = evaluationResults.get(8);

                  confusionMatrixTitle = evaluationResults.get(9);
                  confusionMatrixLinha1 = evaluationResults.get(10);
                  confusionMatrixLinha2 = evaluationResults.get(11);
                  confusionMatrixLinha3 = evaluationResults.get(12);
                  confusionMatrixLinha4 = evaluationResults.get(13);
                  confusionMatrixLinha5 = evaluationResults.get(14);

                  components.selectQualityPerspective(userSelection.get(0), usersRadio, expertsRadio, personalCollectionsRadio);
                  components.selectCategory(userSelection.get(1), categoriesList);
                  components.selectMaterialType(userSelection.get(2), materialTypeList);

                  components.addNameOfTheMetricsInTheTable(metricsName, model);
                  components.addIndicatorInTheMetricsTable(model, metricsIndicators, indices);
                  components.addInfluenceInTheMetricsTableReadedFromModel(model, metricsInfluences, modelAttributes);

                  readedFromModel = true;

                  modelTable = metricsTable.getColumnModel();

                  modelTable.getColumn(0).setCellRenderer(new MetricsNamesTableRenderer());
                  modelTable.getColumn(1).setCellRenderer(new IndicatorAndInfluenceTableRenderer());
                  modelTable.getColumn(2).setCellRenderer(new IndicatorAndInfluenceTableRenderer());
                  JCheckBoxTableRenderer checkBox = new JCheckBoxTableRenderer();
                  modelTable.getColumn(3).setCellRenderer(checkBox);
                  components.showModelPath(choosenModelPath, modelFilePath.getAbsolutePath());
                  URLLabel.setEnabled(true);
                  URLPath.setEnabled(true);
                  getMetricsButton.setEnabled(true);

                  bayesianLogisticRegressionOutputTree.append(bayes.toString() + "\n");
                  bayesianLogisticRegressionOutputEvaluation.append("Correct Instances: " + correctInstances + "    " + correctInstancesPercent + "%\n");
                  bayesianLogisticRegressionOutputEvaluation.append("Incorrect Instances: " + incorrectInstances + "    " + incorrectInstancesPercent + "%\n");
                  bayesianLogisticRegressionOutputEvaluation.append("Kappa: " + kappa + "\n");
                  bayesianLogisticRegressionOutputEvaluation.append("Mean Absolute Error: " + meanAbsoluteError + "\n");
                  bayesianLogisticRegressionOutputEvaluation.append("Root Mean Squared Error: " + rootMeanSquaredError + "\n");
                  bayesianLogisticRegressionOutputEvaluation.append("Relative absolute error: " + relativeAbsoluteError + "\n");
                  bayesianLogisticRegressionOutputEvaluation.append("Root relative squared error: " + rootRelativeSquaredError + "\n");
                  bayesianLogisticRegressionOutputEvaluation.append(confusionMatrixTitle + "\n");
                  bayesianLogisticRegressionOutputEvaluation.append(confusionMatrixLinha1 + "\n");
                  bayesianLogisticRegressionOutputEvaluation.append(confusionMatrixLinha2 + "\n");
                  bayesianLogisticRegressionOutputEvaluation.append(confusionMatrixLinha3 + "\n");
                  bayesianLogisticRegressionOutputEvaluation.append(confusionMatrixLinha4 + "\n");
                  bayesianLogisticRegressionOutputEvaluation.append(confusionMatrixLinha5 + "\n");
               }
            }
         }
      }
      
      chooseModelButton.setFocusable(false);
   }//GEN-LAST:event_chooseModelButtonActionPerformed

   private void classifyDataButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_classifyDataButtonActionPerformed
   {//GEN-HEADEREND:event_classifyDataButtonActionPerformed
      String objectEvaluation;
      
      progressBar.setEnabled(true);
      classyfiedObjectLabel.setEnabled(true);
      
      String modelType = utils.readModelType(modelFilePath);
      
      if(!URLAnalized)
      {
         if(!attributesValues.isEmpty())
         {
            if(modelType.equals("j48"))
            {
               objectEvaluation = utils.classifyData(j48, null, null, modelAttributes, attributesValues, null, false);
               classyfiedObjectLabel.setText(" " + objectEvaluation);
            }
            else
            {
               if(modelType.equals("perceptron"))
               {
                  objectEvaluation = utils.classifyData(null, perceptron, null, modelAttributes, attributesValues, null, false);
                  classyfiedObjectLabel.setText(" " + objectEvaluation);
               }
               else
               {
                  if(modelType.equals("bayes"))
                  {
                     objectEvaluation = utils.classifyData(null, null, bayes, modelAttributes, attributesValues, null, false);
                     classyfiedObjectLabel.setText(" " + objectEvaluation);
                  }
               }
            }
         }
      }
      else
      {
         if(modelType.equals("j48"))
         {
            objectEvaluation = utils.classifyData(j48, null, null, modelAttributes, null, URLPath.getText(), true);
            classyfiedObjectLabel.setText(" " + objectEvaluation);
         }
         else
         {
            if(modelType.equals("perceptron"))
            {
               objectEvaluation = utils.classifyData(null, perceptron, null, modelAttributes, null, URLPath.getText(), true);
               classyfiedObjectLabel.setText(" " + objectEvaluation);
            }
            else
            {
               if(modelType.equals("bayes"))
               {
                  objectEvaluation = utils.classifyData(null, null, bayes, modelAttributes, null, URLPath.getText(), true);
                  classyfiedObjectLabel.setText(" " + objectEvaluation);
               }
            }
         }
      }
      
      classifyDataButton.setFocusable(false);
   }//GEN-LAST:event_classifyDataButtonActionPerformed

   private void getMetricsButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_getMetricsButtonActionPerformed
   {//GEN-HEADEREND:event_getMetricsButtonActionPerformed
      attributesValues.clear();
      components.clearMetricsFromTable(modelCrawledMetrics);
      URLAnalized = false;
      classyfiedObjectLabel.setText(null);
      
      progressBar.setEnabled(true);
      classyfiedObjectLabel.setEnabled(true);
      
      progressBar.setMinimum(0);
      progressBar.setMaximum(100);
      progressBar.setIndeterminate(true);
      progressBar.setStringPainted(true);
      progressBar.setString("Processing...");
      
      ExtractMetrics run = new ExtractMetrics(URLPath.getText(), modelAttributes, modelCrawledMetrics, 
              classifyDataButton, progressBar);
      Thread thread = new Thread(run);
      thread.start();
      
      getMetricsButton.setFocusable(false);
      classifyDataButton.setFocusable(false);
   }//GEN-LAST:event_getMetricsButtonActionPerformed

   private void selectAllMetricsMenuActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_selectAllMetricsMenuActionPerformed
   {//GEN-HEADEREND:event_selectAllMetricsMenuActionPerformed
      components.selectAllMetrics(model);
   }//GEN-LAST:event_selectAllMetricsMenuActionPerformed

   private void deselectAllMetricsMenuActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_deselectAllMetricsMenuActionPerformed
   {//GEN-HEADEREND:event_deselectAllMetricsMenuActionPerformed
      components.deSelectAllMetrics(model);
   }//GEN-LAST:event_deselectAllMetricsMenuActionPerformed

   private void selectRelevantMetricsMenuActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_selectRelevantMetricsMenuActionPerformed
   {//GEN-HEADEREND:event_selectRelevantMetricsMenuActionPerformed
      components.selectRelevantMetrics(model);
   }//GEN-LAST:event_selectRelevantMetricsMenuActionPerformed

   private void trainingSetRadioJ48MouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_trainingSetRadioJ48MouseClicked
   {//GEN-HEADEREND:event_trainingSetRadioJ48MouseClicked
      okButtonGenerateModelJ48.setEnabled(true);
      trainingSetRadioJ48.setSelected(true);
   }//GEN-LAST:event_trainingSetRadioJ48MouseClicked

   private void trainingSetRadioJ48ItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_trainingSetRadioJ48ItemStateChanged
   {//GEN-HEADEREND:event_trainingSetRadioJ48ItemStateChanged
      j48outputTree.setText(null);
      j48outputEvaluation.setText(null);
      saveModel.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
      crossValidationRadioJ48.setSelected(false);
      numberOfFoldsFieldJ48.setEnabled(false);
      percentageSplitRadioJ48.setSelected(false);
      percentageSplitFieldJ48.setEnabled(false);
   }//GEN-LAST:event_trainingSetRadioJ48ItemStateChanged

   private void crossValidationRadioJ48MouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_crossValidationRadioJ48MouseClicked
   {//GEN-HEADEREND:event_crossValidationRadioJ48MouseClicked
      numberOfFoldsFieldJ48.setEnabled(true);
      crossValidationRadioJ48.setSelected(true);
      
      if(numberOfFoldsFieldJ48.getText().length() >= 1)
      {
         okButtonGenerateModelJ48.setEnabled(true);
      }
   }//GEN-LAST:event_crossValidationRadioJ48MouseClicked

   private void numberOfFoldsFieldJ48KeyTyped(java.awt.event.KeyEvent evt)//GEN-FIRST:event_numberOfFoldsFieldJ48KeyTyped
   {//GEN-HEADEREND:event_numberOfFoldsFieldJ48KeyTyped
      if(!Character.isDigit(evt.getKeyChar()))
      {
         evt.consume();
      }
      else
      {
         String textField = numberOfFoldsFieldJ48.getText();
         
         if(textField.length() < 0)
               okButtonGenerateModelJ48.setEnabled(false);
         else
         {
            okButtonGenerateModelJ48.setEnabled(true);
            
            if(textField.length() > 1)
                evt.consume();
         }
      }
   }//GEN-LAST:event_numberOfFoldsFieldJ48KeyTyped

   private void numberOfFoldsFieldJ48KeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_numberOfFoldsFieldJ48KeyPressed
   {//GEN-HEADEREND:event_numberOfFoldsFieldJ48KeyPressed
      if(evt.getKeyCode() == KeyEvent.VK_BACK_SPACE)
      {
         j48outputTree.setText(null);
         j48outputEvaluation.setText(null);
         saveModel.setEnabled(false);
         
         String textField = numberOfFoldsFieldJ48.getText();
         
         if(textField.length() == 1)
               okButtonGenerateModelJ48.setEnabled(false);
      }
   }//GEN-LAST:event_numberOfFoldsFieldJ48KeyPressed

   private void crossValidationRadioJ48ItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_crossValidationRadioJ48ItemStateChanged
   {//GEN-HEADEREND:event_crossValidationRadioJ48ItemStateChanged
      j48outputTree.setText(null);
      j48outputEvaluation.setText(null);
      okButtonGenerateModelJ48.setEnabled(false);
      saveModel.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
      trainingSetRadioJ48.setSelected(false);
      percentageSplitRadioJ48.setSelected(false);
      percentageSplitFieldJ48.setEnabled(false);
   }//GEN-LAST:event_crossValidationRadioJ48ItemStateChanged

   private void percentageSplitRadioJ48MouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_percentageSplitRadioJ48MouseClicked
   {//GEN-HEADEREND:event_percentageSplitRadioJ48MouseClicked
      percentageSplitFieldJ48.setEnabled(true);
      percentageSplitRadioJ48.setSelected(true);
      
      if(percentageSplitFieldJ48.getText().length() >= 1)
      {
         okButtonGenerateModelJ48.setEnabled(true);
      }
   }//GEN-LAST:event_percentageSplitRadioJ48MouseClicked

   private void percentageSplitFieldJ48KeyTyped(java.awt.event.KeyEvent evt)//GEN-FIRST:event_percentageSplitFieldJ48KeyTyped
   {//GEN-HEADEREND:event_percentageSplitFieldJ48KeyTyped
      if(!Character.isDigit(evt.getKeyChar()))
      {
         evt.consume();
      }
      else
      {
         String textField = percentageSplitFieldJ48.getText();
         
         if(textField.length() < 0)
               okButtonGenerateModelJ48.setEnabled(false);
         else
         {
            okButtonGenerateModelJ48.setEnabled(true);
            
            if(textField.length() > 1)
                evt.consume();
         }
      }
   }//GEN-LAST:event_percentageSplitFieldJ48KeyTyped

   private void percentageSplitFieldJ48KeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_percentageSplitFieldJ48KeyPressed
   {//GEN-HEADEREND:event_percentageSplitFieldJ48KeyPressed
      if(evt.getKeyCode() == KeyEvent.VK_BACK_SPACE)
      {
         j48outputTree.setText(null);
         j48outputEvaluation.setText(null);
         saveModel.setEnabled(false);
         
         String textField = percentageSplitFieldJ48.getText();
         
         if(textField.length() == 1)
               okButtonGenerateModelJ48.setEnabled(false);
      }
   }//GEN-LAST:event_percentageSplitFieldJ48KeyPressed

   private void percentageSplitRadioJ48ItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_percentageSplitRadioJ48ItemStateChanged
   {//GEN-HEADEREND:event_percentageSplitRadioJ48ItemStateChanged
      j48outputTree.setText(null);
      j48outputEvaluation.setText(null);
      okButtonGenerateModelJ48.setEnabled(false);
      saveModel.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
      trainingSetRadioJ48.setSelected(false);
      crossValidationRadioJ48.setSelected(false);
      numberOfFoldsFieldJ48.setEnabled(false);
   }//GEN-LAST:event_percentageSplitRadioJ48ItemStateChanged

   private void okButtonGenerateModelPerceptronActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_okButtonGenerateModelPerceptronActionPerformed
   {//GEN-HEADEREND:event_okButtonGenerateModelPerceptronActionPerformed
      /* caminho do arquivo arff criado pelo software */
      String archivePath;
      //Evaluation evaluation;
      Instances archiveData;
      
      multiLayerOutputTree.setText(null);
      multiLayerOutputEvaluation.setText(null);
      usedMetricsOnModel.clear();
      
      for(int i = 0; i < model.getRowCount(); i++)
      {
         if((Boolean)model.getValueAt(i, 3))
         {
            usedMetricsOnModel.add(model.getValueAt(i, 0).toString());
         }
      }
      
      archivePath = utils.createArffFile(grupos, model);

      /* 
       * se o caminho do arquivo devolvido pelo metodo createArffFile for null significa que não existem métricas
       * relevantes para definir qualidade no subgrupo selecionado
       */
      if(archivePath == null)
      {
         JOptionPane.showMessageDialog(null, "There are no relevant metrics to define quality", "Warning", 
                                       JOptionPane.WARNING_MESSAGE);
      }
      else
      {
         archiveData = utils.readDataFromArchive(archivePath);
         
         if(percentageSplitRadioPerceptron.isSelected())
         {
            int percentageSplit = Integer.parseInt(percentageSplitFieldPerceptron.getText());
            perceptron = utils.multiLayerPerceptronBuildClassifier(archiveData, true, percentageSplit);
            evaluation = utils.multiLayerPerceptronEvaluation(perceptron, archiveData, 3, 0);
         }
         else
         {
            if(crossValidationRadioPerceptron.isSelected())
            {
               perceptron = utils.multiLayerPerceptronBuildClassifier(archiveData, false, 0);
               evaluation = utils.multiLayerPerceptronEvaluation(perceptron, archiveData, 2, Integer.valueOf(numberOfFoldsFieldPerceptron.getText()));
            }
            else
            {
               perceptron = utils.multiLayerPerceptronBuildClassifier(archiveData, false, 0);
               evaluation = utils.multiLayerPerceptronEvaluation(perceptron, archiveData, 1, 0);
            }
         }

         multiLayerOutputTree.append(perceptron.toString() + "\n");
         multiLayerOutputEvaluation.append("Correct Instances: " + evaluation.correct() + "    " + evaluation.pctCorrect() + "%\n");
         multiLayerOutputEvaluation.append("Incorrect Instances: " + evaluation.incorrect() + "    " + evaluation.pctIncorrect() + "%\n");
         multiLayerOutputEvaluation.append("Kappa: " + evaluation.kappa() + "\n");
         multiLayerOutputEvaluation.append("Mean Absolute Error: " + evaluation.meanAbsoluteError() + "\n");
         multiLayerOutputEvaluation.append("Root Mean Squared Error: " + evaluation.rootMeanSquaredError() + "\n");
         
         try
         {
            multiLayerOutputEvaluation.append("Relative absolute error: " + evaluation.relativeAbsoluteError() + "\n");
         }
         catch (Exception ex)
         {
            System.err.println(ex);
         }
         
         multiLayerOutputEvaluation.append("Root relative squared error: " + evaluation.rootRelativeSquaredError() + "\n");

         saveModel.setEnabled(true);

         try
         {
            multiLayerOutputEvaluation.append(evaluation.toMatrixString("===== Evaluation Numbers =====\n"));
         }
         catch (Exception ex)
         {
            System.err.println(ex);
         }
      }
      
      okButtonGenerateModelPerceptron.setFocusPainted(false);
      
      modelAttributes = usedMetricsOnModel;
      URLLabel.setEnabled(true);
      URLPath.setEnabled(true);
      getMetricsButton.setEnabled(true);
   }//GEN-LAST:event_okButtonGenerateModelPerceptronActionPerformed

   private void trainingSetRadioPerceptronItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_trainingSetRadioPerceptronItemStateChanged
   {//GEN-HEADEREND:event_trainingSetRadioPerceptronItemStateChanged
      multiLayerOutputTree.setText(null);
      multiLayerOutputEvaluation.setText(null);
      saveModel.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
      crossValidationRadioPerceptron.setSelected(false);
      numberOfFoldsFieldPerceptron.setEnabled(false);
      percentageSplitRadioPerceptron.setSelected(false);
      percentageSplitFieldPerceptron.setEnabled(false);
   }//GEN-LAST:event_trainingSetRadioPerceptronItemStateChanged

   private void trainingSetRadioPerceptronMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_trainingSetRadioPerceptronMouseClicked
   {//GEN-HEADEREND:event_trainingSetRadioPerceptronMouseClicked
      okButtonGenerateModelPerceptron.setEnabled(true);
      trainingSetRadioPerceptron.setSelected(true);
   }//GEN-LAST:event_trainingSetRadioPerceptronMouseClicked

   private void crossValidationRadioPerceptronItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_crossValidationRadioPerceptronItemStateChanged
   {//GEN-HEADEREND:event_crossValidationRadioPerceptronItemStateChanged
      multiLayerOutputTree.setText(null);
      multiLayerOutputEvaluation.setText(null);
      okButtonGenerateModelPerceptron.setEnabled(false);
      saveModel.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
      trainingSetRadioPerceptron.setSelected(false);
      percentageSplitRadioPerceptron.setSelected(false);
      percentageSplitFieldPerceptron.setEnabled(false);
   }//GEN-LAST:event_crossValidationRadioPerceptronItemStateChanged

   private void crossValidationRadioPerceptronMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_crossValidationRadioPerceptronMouseClicked
   {//GEN-HEADEREND:event_crossValidationRadioPerceptronMouseClicked
      numberOfFoldsFieldPerceptron.setEnabled(true);
      crossValidationRadioPerceptron.setSelected(true);
      
      if(numberOfFoldsFieldPerceptron.getText().length() >= 1)
      {
         okButtonGenerateModelPerceptron.setEnabled(true);
      }
   }//GEN-LAST:event_crossValidationRadioPerceptronMouseClicked

   private void numberOfFoldsFieldPerceptronKeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_numberOfFoldsFieldPerceptronKeyPressed
   {//GEN-HEADEREND:event_numberOfFoldsFieldPerceptronKeyPressed
      if(evt.getKeyCode() == KeyEvent.VK_BACK_SPACE)
      {
         multiLayerOutputTree.setText(null);
         multiLayerOutputEvaluation.setText(null);
         saveModel.setEnabled(false);
         
         String textField = numberOfFoldsFieldPerceptron.getText();
         
         if(textField.length() == 1)
            okButtonGenerateModelPerceptron.setEnabled(false);
      }
   }//GEN-LAST:event_numberOfFoldsFieldPerceptronKeyPressed

   private void numberOfFoldsFieldPerceptronKeyTyped(java.awt.event.KeyEvent evt)//GEN-FIRST:event_numberOfFoldsFieldPerceptronKeyTyped
   {//GEN-HEADEREND:event_numberOfFoldsFieldPerceptronKeyTyped
      if(!Character.isDigit(evt.getKeyChar()))
      {
         evt.consume();
      }
      else
      {
         String textField = numberOfFoldsFieldPerceptron.getText();
         
         if(textField.length() < 0)
            okButtonGenerateModelPerceptron.setEnabled(false);
         else
         {
            okButtonGenerateModelPerceptron.setEnabled(true);
            
            if(textField.length() > 1)
               evt.consume();
         }
      }
   }//GEN-LAST:event_numberOfFoldsFieldPerceptronKeyTyped

   private void percentageSplitRadioPerceptronItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_percentageSplitRadioPerceptronItemStateChanged
   {//GEN-HEADEREND:event_percentageSplitRadioPerceptronItemStateChanged
      multiLayerOutputTree.setText(null);
      multiLayerOutputEvaluation.setText(null);
      okButtonGenerateModelPerceptron.setEnabled(false);
      saveModel.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
      trainingSetRadioPerceptron.setSelected(false);
      crossValidationRadioPerceptron.setSelected(false);
      numberOfFoldsFieldPerceptron.setEnabled(false);
   }//GEN-LAST:event_percentageSplitRadioPerceptronItemStateChanged

   private void percentageSplitRadioPerceptronMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_percentageSplitRadioPerceptronMouseClicked
   {//GEN-HEADEREND:event_percentageSplitRadioPerceptronMouseClicked
      percentageSplitFieldPerceptron.setEnabled(true);
      percentageSplitRadioPerceptron.setSelected(true);
      
      if(percentageSplitFieldPerceptron.getText().length() >= 1)
      {
         okButtonGenerateModelPerceptron.setEnabled(true);
      }
   }//GEN-LAST:event_percentageSplitRadioPerceptronMouseClicked

   private void percentageSplitFieldPerceptronKeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_percentageSplitFieldPerceptronKeyPressed
   {//GEN-HEADEREND:event_percentageSplitFieldPerceptronKeyPressed
      if(evt.getKeyCode() == KeyEvent.VK_BACK_SPACE)
      {
         multiLayerOutputTree.setText(null);
         multiLayerOutputEvaluation.setText(null);
         saveModel.setEnabled(false);
         
         String textField = percentageSplitFieldPerceptron.getText();
         
         if(textField.length() == 1)
            okButtonGenerateModelPerceptron.setEnabled(false);
      }
   }//GEN-LAST:event_percentageSplitFieldPerceptronKeyPressed

   private void percentageSplitFieldPerceptronKeyTyped(java.awt.event.KeyEvent evt)//GEN-FIRST:event_percentageSplitFieldPerceptronKeyTyped
   {//GEN-HEADEREND:event_percentageSplitFieldPerceptronKeyTyped
      if(!Character.isDigit(evt.getKeyChar()))
      {
         evt.consume();
      }
      else
      {
         String textField = percentageSplitFieldPerceptron.getText();
         
         if(textField.length() < 0)
               okButtonGenerateModelPerceptron.setEnabled(false);
         else
         {
            okButtonGenerateModelPerceptron.setEnabled(true);
            
            if(textField.length() > 1)
                evt.consume();
         }
      }
   }//GEN-LAST:event_percentageSplitFieldPerceptronKeyTyped

   private void trainingSetRadioBayesItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_trainingSetRadioBayesItemStateChanged
   {//GEN-HEADEREND:event_trainingSetRadioBayesItemStateChanged
      bayesianLogisticRegressionOutputTree.setText(null);
      bayesianLogisticRegressionOutputEvaluation.setText(null);
      saveModel.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
      crossValidationRadioBayes.setSelected(false);
      numberOfFoldsFieldBayes.setEnabled(false);
      percentageSplitRadioBayes.setSelected(false);
      percentageSplitFieldBayes.setEnabled(false);
   }//GEN-LAST:event_trainingSetRadioBayesItemStateChanged

   private void trainingSetRadioBayesMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_trainingSetRadioBayesMouseClicked
   {//GEN-HEADEREND:event_trainingSetRadioBayesMouseClicked
      okButtonGenerateModelBayes.setEnabled(true);
      trainingSetRadioBayes.setSelected(true);
   }//GEN-LAST:event_trainingSetRadioBayesMouseClicked

   private void crossValidationRadioBayesItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_crossValidationRadioBayesItemStateChanged
   {//GEN-HEADEREND:event_crossValidationRadioBayesItemStateChanged
      bayesianLogisticRegressionOutputTree.setText(null);
      bayesianLogisticRegressionOutputEvaluation.setText(null);
      okButtonGenerateModelBayes.setEnabled(false);
      saveModel.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
      trainingSetRadioBayes.setSelected(false);
      percentageSplitRadioBayes.setSelected(false);
      percentageSplitFieldBayes.setEnabled(false);
   }//GEN-LAST:event_crossValidationRadioBayesItemStateChanged

   private void crossValidationRadioBayesMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_crossValidationRadioBayesMouseClicked
   {//GEN-HEADEREND:event_crossValidationRadioBayesMouseClicked
      numberOfFoldsFieldBayes.setEnabled(true);
      crossValidationRadioBayes.setSelected(true);
      
      if(numberOfFoldsFieldBayes.getText().length() >= 1)
      {
         okButtonGenerateModelBayes.setEnabled(true);
      }
   }//GEN-LAST:event_crossValidationRadioBayesMouseClicked

   private void percentageSplitRadioBayesItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_percentageSplitRadioBayesItemStateChanged
   {//GEN-HEADEREND:event_percentageSplitRadioBayesItemStateChanged
      bayesianLogisticRegressionOutputTree.setText(null);
      bayesianLogisticRegressionOutputEvaluation.setText(null);
      okButtonGenerateModelBayes.setEnabled(false);
      saveModel.setEnabled(false);
      URLLabel.setEnabled(false);
      URLPath.setEnabled(false);
      getMetricsButton.setEnabled(false);
      choosenModelPath.setText(null);
      progressBar.setEnabled(false);              
      classyfiedObjectLabel.setEnabled(false);
      components.clearMetricsFromTable(modelCrawledMetrics);
      progressBar.setValue(0);
      progressBar.setStringPainted(false);
      classifyDataButton.setEnabled(false);
      classyfiedObjectLabel.setText(null);
      trainingSetRadioBayes.setSelected(false);
      crossValidationRadioBayes.setSelected(false);
      numberOfFoldsFieldBayes.setEnabled(false);
   }//GEN-LAST:event_percentageSplitRadioBayesItemStateChanged

   private void percentageSplitRadioBayesMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_percentageSplitRadioBayesMouseClicked
   {//GEN-HEADEREND:event_percentageSplitRadioBayesMouseClicked
      percentageSplitFieldBayes.setEnabled(true);
      percentageSplitRadioBayes.setSelected(true);
      
      if(percentageSplitFieldBayes.getText().length() >= 1)
      {
         okButtonGenerateModelBayes.setEnabled(true);
      }
   }//GEN-LAST:event_percentageSplitRadioBayesMouseClicked

   private void numberOfFoldsFieldBayesKeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_numberOfFoldsFieldBayesKeyPressed
   {//GEN-HEADEREND:event_numberOfFoldsFieldBayesKeyPressed
      if(evt.getKeyCode() == KeyEvent.VK_BACK_SPACE)
      {
         bayesianLogisticRegressionOutputTree.setText(null);
         bayesianLogisticRegressionOutputEvaluation.setText(null);
         saveModel.setEnabled(false);
         
         String textField = numberOfFoldsFieldBayes.getText();
         
         if(textField.length() == 1)
            okButtonGenerateModelBayes.setEnabled(false);
      }
   }//GEN-LAST:event_numberOfFoldsFieldBayesKeyPressed

   private void numberOfFoldsFieldBayesKeyTyped(java.awt.event.KeyEvent evt)//GEN-FIRST:event_numberOfFoldsFieldBayesKeyTyped
   {//GEN-HEADEREND:event_numberOfFoldsFieldBayesKeyTyped
      if(!Character.isDigit(evt.getKeyChar()))
      {
         evt.consume();
      }
      else
      {
         String textField = numberOfFoldsFieldBayes.getText();
         
         if(textField.length() < 0)
            okButtonGenerateModelBayes.setEnabled(false);
         else
         {
            okButtonGenerateModelBayes.setEnabled(true);
            
            if(textField.length() > 1)
               evt.consume();
         }
      }
   }//GEN-LAST:event_numberOfFoldsFieldBayesKeyTyped

   private void percentageSplitFieldBayesKeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_percentageSplitFieldBayesKeyPressed
   {//GEN-HEADEREND:event_percentageSplitFieldBayesKeyPressed
      if(evt.getKeyCode() == KeyEvent.VK_BACK_SPACE)
      {
         bayesianLogisticRegressionOutputTree.setText(null);
         bayesianLogisticRegressionOutputEvaluation.setText(null);
         saveModel.setEnabled(false);
         
         String textField = percentageSplitFieldBayes.getText();
         
         if(textField.length() == 1)
            okButtonGenerateModelBayes.setEnabled(false);
      }
   }//GEN-LAST:event_percentageSplitFieldBayesKeyPressed

   private void percentageSplitFieldBayesKeyTyped(java.awt.event.KeyEvent evt)//GEN-FIRST:event_percentageSplitFieldBayesKeyTyped
   {//GEN-HEADEREND:event_percentageSplitFieldBayesKeyTyped
      if(!Character.isDigit(evt.getKeyChar()))
      {
         evt.consume();
      }
      else
      {
         String textField = percentageSplitFieldBayes.getText();
         
         if(textField.length() < 0)
               okButtonGenerateModelBayes.setEnabled(false);
         else
         {
            okButtonGenerateModelBayes.setEnabled(true);
            
            if(textField.length() > 1)
                evt.consume();
         }
      }
   }//GEN-LAST:event_percentageSplitFieldBayesKeyTyped

   private void okButtonGenerateModelBayesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_okButtonGenerateModelBayesActionPerformed
   {//GEN-HEADEREND:event_okButtonGenerateModelBayesActionPerformed
      /* caminho do arquivo arff criado pelo software */
      String archivePath;
      //Evaluation evaluation;
      Instances archiveData;
      
      bayesianLogisticRegressionOutputTree.setText(null);
      bayesianLogisticRegressionOutputEvaluation.setText(null);
      usedMetricsOnModel.clear();
      
      for(int i = 0; i < model.getRowCount(); i++)
      {
         if((Boolean)model.getValueAt(i, 3))
         {
            usedMetricsOnModel.add(model.getValueAt(i, 0).toString());
         }
      }
      
      archivePath = utils.createArffFile(grupos, model);

      /* 
       * se o caminho do arquivo devolvido pelo metodo createArffFile for null significa que não existem métricas
       * relevantes para definir qualidade no subgrupo selecionado
       */
      if(archivePath == null)
      {
         JOptionPane.showMessageDialog(null, "There are no relevant metrics to define quality", "Warning", 
                                       JOptionPane.WARNING_MESSAGE);
      }
      else
      {
         archiveData = utils.readDataFromArchive(archivePath);
         
         if(percentageSplitRadioBayes.isSelected())
         {
            int percentageSplit = Integer.parseInt(percentageSplitFieldBayes.getText());
            bayes = utils.bayesNetworkBuildClassifier(archiveData, true, percentageSplit);
            evaluation = utils.bayesNetworkEvaluation(bayes, archiveData, 3, 0);
         }
         else
         {
            if(crossValidationRadioBayes.isSelected())
            {
               bayes = utils.bayesNetworkBuildClassifier(archiveData, false, 0);
               evaluation = utils.bayesNetworkEvaluation(bayes, archiveData, 2, Integer.valueOf(numberOfFoldsFieldBayes.getText()));
            }
            else
            {
               bayes = utils.bayesNetworkBuildClassifier(archiveData, false, 0);
               evaluation = utils.bayesNetworkEvaluation(bayes, archiveData, 1, 0);
            }
         }

         bayesianLogisticRegressionOutputTree.append(bayes.toString() + "\n");
         bayesianLogisticRegressionOutputEvaluation.append("Correct Instances: " + evaluation.correct() + "    " + evaluation.pctCorrect() + "%\n");
         bayesianLogisticRegressionOutputEvaluation.append("Incorrect Instances: " + evaluation.incorrect() + "    " + evaluation.pctIncorrect() + "%\n");
         bayesianLogisticRegressionOutputEvaluation.append("Kappa: " + evaluation.kappa() + "\n");
         bayesianLogisticRegressionOutputEvaluation.append("Mean Absolute Error: " + evaluation.meanAbsoluteError() + "\n");
         bayesianLogisticRegressionOutputEvaluation.append("Root Mean Squared Error: " + evaluation.rootMeanSquaredError() + "\n");
         
         try
         {
            bayesianLogisticRegressionOutputEvaluation.append("Relative absolute error: " + evaluation.relativeAbsoluteError() + "\n");
         }
         catch (Exception ex)
         {
            System.err.println(ex);
         }
         
         bayesianLogisticRegressionOutputEvaluation.append("Root relative squared error: " + evaluation.rootRelativeSquaredError() + "\n");

         saveModel.setEnabled(true);

         try
         {
            bayesianLogisticRegressionOutputEvaluation.append(evaluation.toMatrixString("===== Evaluation Numbers =====\n"));
         }
         catch (Exception ex)
         {
            System.err.println(ex);
         }
      }
      
      okButtonGenerateModelBayes.setFocusPainted(false);
      
      modelAttributes = usedMetricsOnModel;
      URLLabel.setEnabled(true);
      URLPath.setEnabled(true);
      getMetricsButton.setEnabled(true);
   }//GEN-LAST:event_okButtonGenerateModelBayesActionPerformed

   /**
    * @param args os argumentos da linha de comando
    */
   public static void main(String args[])
   {
      /*
       * Seta o visual GTK+
       */
      //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
       * Se GTK+ não estiver disponível, permanece o visual padrão. Para mais detalhes veja
       * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
       */
      try
      {
         for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
         {
            if ("GTK+".equals(info.getName()))
            {
               javax.swing.UIManager.setLookAndFeel(info.getClassName());
               break;
            }
         }
      }
      catch (ClassNotFoundException ex)
      {
         java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
      }
      catch (InstantiationException ex)
      {
         java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
      }
      catch (IllegalAccessException ex)
      {
         java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
      }
      catch (javax.swing.UnsupportedLookAndFeelException ex)
      {
         java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
      }
      //</editor-fold>

      /*
       * Cria e mostra o form
       */
      java.awt.EventQueue.invokeLater(new Runnable()
      {
         @Override
         public void run()
         {
            new MainWindow().setVisible(true);
         }
      });
   }
   // Variables declaration - do not modify//GEN-BEGIN:variables
   private javax.swing.JLabel URLLabel;
   private javax.swing.JTextField URLPath;
   private javax.swing.JTextArea bayesianLogisticRegressionOutputEvaluation;
   private javax.swing.JTextArea bayesianLogisticRegressionOutputTree;
   private javax.swing.JButton cancelButton;
   private javax.swing.JList categoriesList;
   private javax.swing.JButton chooseModelButton;
   private javax.swing.JTextField choosenModelPath;
   private javax.swing.JButton classifyDataButton;
   private javax.swing.JLabel classyfiedObjectLabel;
   private javax.swing.JTable crawledMetricsTable;
   private javax.swing.JRadioButton crossValidationRadioBayes;
   private javax.swing.JRadioButton crossValidationRadioJ48;
   private javax.swing.JRadioButton crossValidationRadioPerceptron;
   private javax.swing.JLabel dValue;
   private javax.swing.JLabel dValueLabel;
   private javax.swing.JMenuItem deselectAllMetricsMenu;
   private javax.swing.JMenu editMenu;
   private javax.swing.JRadioButton expertsRadio;
   private javax.swing.JMenu fileMenu;
   private javax.swing.JLabel foldsLabel;
   private javax.swing.JButton getMetricsButton;
   private javax.swing.JLabel goodLabel;
   private javax.swing.JLabel goodThreshold;
   private javax.swing.JLabel graphicJLabel;
   private javax.swing.JPanel graphicJPanel;
   private javax.swing.JTextArea j48outputEvaluation;
   private javax.swing.JTextArea j48outputTree;
   private javax.swing.JLabel jLabel1;
   private javax.swing.JLabel jLabel2;
   private javax.swing.JLabel jLabel3;
   private javax.swing.JLabel jLabel4;
   private javax.swing.JPanel jPanel1;
   private javax.swing.JPanel jPanel10;
   private javax.swing.JPanel jPanel2;
   private javax.swing.JPanel jPanel3;
   private javax.swing.JPanel jPanel4;
   private javax.swing.JPanel jPanel5;
   private javax.swing.JPanel jPanel6;
   private javax.swing.JPanel jPanel7;
   private javax.swing.JPanel jPanel8;
   private javax.swing.JPanel jPanel9;
   private javax.swing.JScrollPane jScrollPane1;
   private javax.swing.JScrollPane jScrollPane2;
   private javax.swing.JScrollPane jScrollPane3;
   private javax.swing.JScrollPane jScrollPane4;
   private javax.swing.JScrollPane jScrollPane5;
   private javax.swing.JScrollPane jScrollPane6;
   private javax.swing.JTabbedPane jTabbedPane2;
   private javax.swing.JTabbedPane jTabbedPane3;
   private javax.swing.JTabbedPane jTabbedPane4;
   private javax.swing.JList materialTypeList;
   private javax.swing.JLabel maximumValue;
   private javax.swing.JLabel maximumValueLabel;
   private javax.swing.JLabel meanValue;
   private javax.swing.JLabel meanValueLabel;
   private javax.swing.JLabel medianValue;
   private javax.swing.JLabel medianValueLabel;
   private javax.swing.JMenuBar menuBar;
   private javax.swing.JTable metricsTable;
   private javax.swing.JLabel minimumValue;
   private javax.swing.JLabel minimumValueLabel;
   private javax.swing.JTabbedPane modelsTabs;
   private javax.swing.JTextArea multiLayerOutputEvaluation;
   private javax.swing.JTextArea multiLayerOutputTree;
   private javax.swing.JLabel notGoodLabel;
   public static javax.swing.JLabel notGoodThreshold;
   private javax.swing.JTextField numberOfFoldsFieldBayes;
   private javax.swing.JTextField numberOfFoldsFieldJ48;
   private javax.swing.JTextField numberOfFoldsFieldPerceptron;
   private javax.swing.JButton okButton;
   private javax.swing.JButton okButtonGenerateModelBayes;
   private javax.swing.JButton okButtonGenerateModelJ48;
   private javax.swing.JButton okButtonGenerateModelPerceptron;
   public static javax.swing.JLabel pValueKolmogorov;
   private javax.swing.JLabel pValueKolmogorovLabel;
   private javax.swing.JLabel pValueMannWhitneyLabel;
   public static javax.swing.JLabel pValuemannWhitney;
   private javax.swing.JPanel panelBasicStatistics;
   private javax.swing.JPanel panelCategory;
   private javax.swing.JPanel panelExplore;
   private javax.swing.JPanel panelGeneralInformation;
   private javax.swing.JPanel panelGenerateModel;
   private javax.swing.JPanel panelKolmogorv;
   private javax.swing.JPanel panelMannWhitney;
   private javax.swing.JPanel panelMaterialType;
   public static javax.swing.JPanel panelQualityPerspective;
   private javax.swing.JPanel panelThresholds;
   private javax.swing.JLabel peerReviewRatedLabel;
   public static javax.swing.JLabel peerReviewRatedNumber;
   private javax.swing.JLabel percentLabel;
   private javax.swing.JTextField percentageSplitFieldBayes;
   private javax.swing.JTextField percentageSplitFieldJ48;
   private javax.swing.JTextField percentageSplitFieldPerceptron;
   private javax.swing.JRadioButton percentageSplitRadioBayes;
   private javax.swing.JRadioButton percentageSplitRadioJ48;
   private javax.swing.JRadioButton percentageSplitRadioPerceptron;
   private javax.swing.JLabel personalCollectionsLabel;
   public static javax.swing.JLabel personalCollectionsNumber;
   private javax.swing.JRadioButton personalCollectionsRadio;
   public static javax.swing.JProgressBar progressBar;
   private javax.swing.JLabel qualityIndicatorsLabel;
   private javax.swing.ButtonGroup qualityPerspectiveButtonGroup;
   private javax.swing.JMenuItem saveModel;
   private javax.swing.JScrollPane scrollPaneCategorias;
   private javax.swing.JScrollPane scrollPaneMaterialType;
   private javax.swing.JScrollPane scrollPaneMetricsTable;
   private javax.swing.JScrollPane scrollPaneOutput;
   private javax.swing.JMenuItem selectAllMetricsMenu;
   private javax.swing.JMenuItem selectRelevantMetricsMenu;
   private javax.swing.JTabbedPane tabbedPane;
   private javax.swing.JTabbedPane tabbedPaneStatisticMethods;
   private javax.swing.JRadioButton trainingSetRadioBayes;
   private javax.swing.JRadioButton trainingSetRadioJ48;
   private javax.swing.JRadioButton trainingSetRadioPerceptron;
   private javax.swing.JLabel userRatedLabel;
   public static javax.swing.JLabel userRatedNumber;
   private javax.swing.JRadioButton usersRadio;
   public static javax.swing.JLabel wValue;
   private javax.swing.JLabel wValueLabel;
   // End of variables declaration//GEN-END:variables
   private DefaultListModel categoryModel = new DefaultListModel();
   private DefaultListModel materialTypeModel = new DefaultListModel();
   private DefaultTableModel model;
   private DefaultTableModel modelCrawledMetrics;
   private List<String> metricsNames = new ArrayList<>();
   private final Statistic statistic = new Statistic();
   // irá receber os grupos bons e não bons sendo a lista 0 os bons e a lista 1 os não bons
   private List<List<LearningObject>> grupos;
   // recebe a os parametros D no grupo 0 e p-value no grupo 1 resultantes do calculo do kolmogorov
   private List<List<Double>> kolmogorovValues = new ArrayList<>();
   // recebe a os parametros D no grupo 0 e p-value no grupo 1 resultantes do calculo do mannWhitney
   private List<List<Double>> mannWhitneyValues = new ArrayList<>();
   private List<Double> mannWhitneyW = new ArrayList<>();
   private List<Double> mannWhitneyP = new ArrayList<>();
   private List<Double> kolmogorovD = new ArrayList<>();
   private List<Double> kolmogorovP = new ArrayList<>();
   private double[] mannWhitneyResult;
   private double[] kolmogorovResult;
   private double[] goodAndNotGoodMetricVector;
   private int[] indicators;
   public static Components components = new Components();
   public static Utils utils = new Utils();
   public static DataBaseMerlot dataBaseMerlot = new DataBaseMerlot();
   public static DataBaseCrawler dataBaseCrawler = new DataBaseCrawler();
   public static Connection conexionMerlot;
   public static Connection conexionCrawler;
   public static int testSize;
   public static int trainSize;
   public static Instances randData;
   public static Instances train;
   /* receberá os índices da tabela que contém metricas indicadoras qualidade*/
   public static List<Integer>indices = new ArrayList<>();
   public static List<Double> attributesValues = new ArrayList<>();
   public static boolean URLAnalized = false;
   private Evaluation evaluation;
   private J48 j48 = new J48();
   private MultilayerPerceptron perceptron = new MultilayerPerceptron();
   private BayesianLogisticRegression bayes = new BayesianLogisticRegression();
   private String modelPath;
   private List<String> modelAttributes = new ArrayList<>();
   private List<String> usedMetricsOnModel = new ArrayList<>();
   private File modelFilePath;
   private TableColumnModel modelTable;
   private boolean readedFromModel = true;
}
